<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Setor <?= h($areaservico->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Descrição do Setor') ?></p>
                        <p><?= $areaservico->descricao ? $areaservico->descricao : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $areaservico->dt_cadastro ? $areaservico->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $areaservico->last_update ? $areaservico->last_update : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $areaservico->status ? __('Ativo') : __('Desativado'); ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($areaservico->grupodocumentos) && $admin): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Grupo de Documentos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Grupo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($areaservico->grupodocumentos as $grupodocumentos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($grupodocumentos->id) ?>">
                                        </td>
                                        <td><?= h($grupodocumentos->descricao) ?></td>
                                        <td><?= h($grupodocumentos->dt_cadastro) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Grupodocumentos', 'action' => 'view', $grupodocumentos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Grupodocumentos', 'action' => 'edit', $grupodocumentos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Grupodocumentos', 'action' => 'delete', $grupodocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $grupodocumentos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($areaservico->gruposervicos) && $admin): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Subsetores Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descricao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($areaservico->gruposervicos as $gruposervicos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($gruposervicos->id) ?>">
                                        </td>
                                        <td><?= h($gruposervicos->nome) ?></td>
                                        <td><?= h($gruposervicos->descricao) ?></td>
                                        <td><?= h($gruposervicos->dt_cadastro) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Gruposervicos', 'action' => 'view', $gruposervicos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Gruposervicos', 'action' => 'edit', $gruposervicos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Gruposervicos', 'action' => 'delete', $gruposervicos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $gruposervicos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($areaservico->mensagens) && $admin): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Mensagens Vínculadas </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Envio') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Remetente') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Assunto') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($areaservico->mensagens as $mensagens):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($mensagens->id) ?>">
                                        </td>
                                        <td><?= h($mensagens->dt_envio) ?></td>
                                        <td><?= h($mensagens->user->nome) ?></td>
                                        <td><?= h($mensagens->assunto) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Mensagens', 'action' => 'view', $mensagens->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Mensagens', 'action' => 'edit', $mensagens->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Mensagens', 'action' => 'delete', $mensagens->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $mensagens->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($areaservico->tarefas) && $admin): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Tarefas Vínculadas </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Dt Inicio') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Final') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tipo de Tarefa') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Prioridade') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tipo de Serviço') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Titulo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Lembrete') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($areaservico->tarefas as $tarefas):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($tarefas->id) ?>">
                                        </td>
                                        <td><?= h($tarefas->dt_inicio) ?></td>
                                        <td><?= h($tarefas->dt_final) ?></td>
                                        <td><?= h($tarefas->tarefatipo->descricao) ?></td>
                                        <td><?= h($tarefas->tarefaprioridade->descricao) ?></td>
                                        <td><?= h($tarefas->tiposervico->descricao) ?></td>
                                        <td><?= h($tarefas->titulo) ?></td>
                                        <td><?= h($tarefas->dt_lembrete) ?></td>
                                        <td><?= $tarefas->dt_concluida ? 'Concluída':'Pendente' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Tarefas', 'action' => 'view', $tarefas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefas', 'action' => 'delete', $tarefas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefas->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($areaservico->usersareaservicos) && $admin): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Responsáveis por esse Setor </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Responsável') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Admin Setor') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ult. Atualização') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($areaservico->usersareaservicos as $usersareaservicos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($usersareaservicos->id) ?>">
                                        </td>
                                        <td><?= h($usersareaservicos->user->nome) ?></td>
                                        <td><?= h($usersareaservicos->admin_setor) ?></td>
                                        <td><?= h($usersareaservicos->dt_cadastro) ?></td>
                                        <td><?= h($usersareaservicos->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Usersareaservicos', 'action' => 'view', $usersareaservicos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Usersareaservicos', 'action' => 'edit', $usersareaservicos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Usersareaservicos', 'action' => 'delete', $usersareaservicos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $usersareaservicos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


