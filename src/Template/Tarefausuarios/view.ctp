<div class="page-title">
    <div class="title_left">
        <h3><?= __('Tarefausuarios') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tarefausuario->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Tarefa') ?></p>
                                    <p><?= $tarefausuario->has('tarefa') ? $this->Html->link($tarefausuario->tarefa->titulo, ['controller' => 'Tarefas', 'action' => 'view', $tarefausuario->tarefa->id]) : 'Não Informado' ?></p>

                                                                                                                    <p class="title"><?= __('User') ?></p>
                                    <p><?= $tarefausuario->has('user') ? $this->Html->link($tarefausuario->user->nome, ['controller' => 'Users', 'action' => 'view', $tarefausuario->user->id]) : 'Não Informado' ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $tarefausuario->id ? $this->Number->format($tarefausuario->id) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Concluida') ?></p>
                                <p><?= $tarefausuario->dt_concluida ? $tarefausuario->dt_concluida : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $tarefausuario->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


