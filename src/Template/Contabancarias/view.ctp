<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Conta Bancária -  <?= $contabancaria->banco->nome ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Banco') ?></p>
                        <p><?= $contabancaria->has('banco') ? $contabancaria->banco->nome : 'Não informado'; ?></p>

                        <?php if ($contabancaria->has('banco') && $contabancaria->banco->id != 1) { ?>
                            <p class="title"><?= __('CPF/CNPJ') ?></p>
                            <p><?= $contabancaria->cpf ? $contabancaria->cpf : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Titular da Conta') ?></p>
                            <p><?= $contabancaria->nome ? $contabancaria->nome : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Operacao') ?></p>
                            <p><?= $contabancaria->operacao ? $this->Number->format($contabancaria->operacao) : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Agência') ?></p>
                            <p><?= $contabancaria->agencia ? ($this->Number->format($contabancaria->agencia) . '-' . $this->Number->format($contabancaria->ag_digito)) : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Conta') ?></p>
                            <p><?= $contabancaria->conta ? $this->Number->format($contabancaria->conta) . '-' . $this->Number->format($contabancaria->co_digito) : 'Não Informado'; ?></p>
                        <?php } ?>
                        <p class="title"><?= __('Saldo em Caixa') ?></p>
                        <p><?= $contabancaria->valorcaixa ? $this->Number->format($contabancaria->valorcaixa) : 'Não Informado'; ?></p>


                    </div>
                </div>
                <?php if ($contabancaria->has('banco') && $contabancaria->banco->id != 1) { ?>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="project_detail">
                            <p class="title"><?= __('Endereco do Titular') ?></p>
                            <p><?= $contabancaria->endereco ? $contabancaria->endereco : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Numero') ?></p>
                            <p><?= $contabancaria->end_numero ? $contabancaria->end_numero : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Complemento') ?></p>
                            <p><?= $contabancaria->end_complemento ? $contabancaria->end_complemento : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Bairro') ?></p>
                            <p><?= $contabancaria->end_bairro ? $contabancaria->end_bairro : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Cep') ?></p>
                            <p><?= $contabancaria->end_cep ? $contabancaria->end_cep : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Estado') ?></p>
                            <p><?= $contabancaria->has('estado') ? $contabancaria->estado->estado_sigla : 'Não Informado' ?></p>

                            <p class="title"><?= __('Cidade') ?></p>
                            <p><?= $contabancaria->has('cidade') ? $contabancaria->cidade->cidade_nome : 'Não Informado' ?></p>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="project_detail">

                            <p class="title"><?= __('Dt Vencimento Cartão') ?></p>
                            <p><?= $contabancaria->dt_vencimento ? $contabancaria->dt_vencimento : 'Não Informado' ?></p>

                            <p class="title"><?= __('Dt Cadastro') ?></p>
                            <p><?= h($contabancaria->dt_cadastro) ?></p>


                            <p class="title"><?= __('Ultima Atualização') ?></p>
                            <p><?= $contabancaria->last_update ? $contabancaria->last_update : 'Não houve alteração' ?></p>

                        </div>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($contabancaria->movimentacaobancarias)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Movimentações de Caixa dessa Conta</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th>
                                <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th scope="col"  class="column-title"><?= __('Dt. Cadastro') ?></th>
                            <th scope="col"  class="column-title"><?= __('Tipo Movimentação') ?></th>
                            <th scope="col"  class="column-title"><?= __('Fornecedor') ?></th>
                            <th scope="col"  class="column-title"><?= __('Cliente') ?></th>
                            <th scope="col"  class="column-title"><?= __('Forma Pagamento') ?></th>
                            <th scope="col"  class="column-title"><?= __('Valor') ?></th>
                            <th scope="col"  class="column-title"><?= __('Saldo Posterior') ?></th>
                            <th scope="col"  class="column-title"><?= __('Status Pag.') ?></th>
                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($contabancaria->movimentacaobancarias as $movimentacaobancarias):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td class="a-center ">
                                    <input type="checkbox" class="flat" name="table_records" value="<?= h($movimentacaobancarias->id) ?>">
                                </td>
                                <td><?= h($movimentacaobancarias->dt_cadastro) ?></td>
                                <td><?= h($movimentacaobancarias->movimentacaotipo->descricao) ?></td>
                                <td><?= $movimentacaobancarias->has('fornecedore') ? $movimentacaobancarias->fornecedore->nome : '--' ?></td>
                                <td><?= $movimentacaobancarias->has('cliente') ? $movimentacaobancarias->cliente->nome : '--' ?></td>
                                <td><?= $movimentacaobancarias->has('formaspagamento') ? $movimentacaobancarias->formaspagamento->descricao . ( $movimentacaobancarias->avista ? ' à vista' : ' parcelado') : '' ?></td>
                                <td>R$ <?= $this->Number->format($movimentacaobancarias->valor) ?></td>
                                <td>R$ <?= $this->Number->format($movimentacaobancarias->saldo_posterior) ?></td>
                                <td><?= $movimentacaobancarias->pago ? 'PAGO' : 'PENDENTE' ?></td>                                

                                <td  class=" last">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('Visualizar'), ['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacaobancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                        <?= $this->Html->link(__('Editar'), ['controller' => 'Movimentacaobancarias', 'action' => 'edit', $movimentacaobancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Movimentacaobancarias', 'action' => 'delete', $movimentacaobancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $movimentacaobancarias->id)]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>



