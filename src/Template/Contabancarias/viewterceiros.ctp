<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Conta Bancária -  <?= $contabancaria->cliente_id ? $contabancaria->cliente->nome : $contabancaria->fornecedore->nome; ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($contabancaria->cliente_id) { ?>
                            <p class="title"><?= __('Cliente') ?></p>
                            <p><?= $contabancaria->cliente->nome; ?></p>
                        <?php } else { ?>
                            <p class="title"><?= __('Fornecedor') ?></p>
                            <p><?= $contabancaria->fornecedore->nome; ?></p>                            
                        <?php } ?>

                        <p class="title"><?= __('Banco') ?></p>
                        <p><?= $contabancaria->has('banco') ? $contabancaria->banco->nome : 'Não informado'; ?></p>

                        <p class="title"><?= __('CPF/CNPJ') ?></p>
                        <p><?= $contabancaria->cpf ? $contabancaria->cpf : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Titular da Conta') ?></p>
                        <p><?= $contabancaria->nome ? $contabancaria->nome : 'Não Informado'; ?></p>

                    </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">


                        <p class="title"><?= __('Operacao') ?></p>
                        <p><?= $contabancaria->operacao ? $this->Number->format($contabancaria->operacao) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Agência') ?></p>
                        <p><?= $contabancaria->agencia ? ($this->Number->format($contabancaria->agencia) . '-' . $this->Number->format($contabancaria->ag_digito)) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Conta') ?></p>
                        <p><?= $contabancaria->conta ? $this->Number->format($contabancaria->conta) . '-' . $this->Number->format($contabancaria->co_digito) : 'Não Informado'; ?></p>


                    </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= h($contabancaria->dt_cadastro) ?></p>


                        <p class="title"><?= __('Ultima Atualização') ?></p>
                        <p><?= $contabancaria->last_update ? $contabancaria->last_update : 'Não houve alteração' ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>