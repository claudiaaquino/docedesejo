<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Contas da Empresa</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?> </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <?php if ($contabancarias) { ?>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col" class="column-title">Banco</th>
                                    <th scope="col" class="column-title">Agência</th>
                                    <th scope="col" class="column-title">Conta</th>
                                    <th scope="col" class="column-title">Saldo Atual</th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($contabancarias as $contabancaria) {
                                    ?>                                
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $contabancaria->has('banco') ? $contabancaria->banco->nome : 'Não informado' ?></td>
                                        <td><?= $contabancaria->agencia ? $contabancaria->agencia . '-' . $contabancaria->ag_digito : 'Não informado' ?></td>
                                        <td><?= $contabancaria->conta ? $contabancaria->conta . '-' . $contabancaria->co_digito : 'Não informado' ?></td>
                                        <td>R$ <?= $this->Number->format($contabancaria->valorcaixa) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $contabancaria->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $contabancaria->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $contabancaria->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $contabancaria->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>

                            </tbody>                       
                        </table>
                    </div>
                <?php } else {
                    ?>
                    Não há nenhuma conta cadastrada
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Contas de Clientes/Fornecedores</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'addterceiros'], ['class' => "btn btn-dark fa fa-file"]) ?> </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php if (count($contabancariaterceiros) > 0) { ?>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('fornecedor_id', 'Fornecedor') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('cliente_id', 'Cliente') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('banco_id', 'Banco') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('nome', 'Títular') ?></th>                                
                                    <th scope="col" class="column-title">Agência</th>
                                    <th scope="col" class="column-title">Conta</th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($contabancariaterceiros as $contabancaria) {
                                    ?>                                
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $contabancaria->has('fornecedore') ? $this->Html->link($contabancaria->fornecedore->nome, ['controller' => 'Fornecedores', 'action' => 'view', $contabancaria->fornecedore->id]) : '--' ?></td>
                                        <td><?= $contabancaria->has('cliente') ? $this->Html->link($contabancaria->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $contabancaria->cliente->id]) : '--' ?></td>
                                        <td><?= $contabancaria->has('banco') ? $contabancaria->banco->nome : '' ?></td>
                                        <td><?= h($contabancaria->nome) ?></td>
                                        <td><?= $contabancaria->agencia ? $contabancaria->agencia . '-' . $contabancaria->ag_digito : 'Não informado' ?></td>
                                        <td><?= $contabancaria->conta ? $contabancaria->conta . '-' . $contabancaria->co_digito : 'Não informado' ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $contabancaria->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'editterceiros', $contabancaria->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $contabancaria->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $contabancaria->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <?= $this->Paginator->counter() ?>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    Não há nenhuma conta cadastrada
                <?php } ?>
            </div>
        </div>
    </div>
</div>
