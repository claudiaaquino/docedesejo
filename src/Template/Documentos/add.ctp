<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Enviar Documentos  <?= !$admin ? 'para a Tríade' : '' ?><small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($documento, ["class" => "form-horizontal form-label-left", 'enctype' => "multipart/form-data"]) ?>
                <?= $this->Flash->render() ?>
                <?= $this->Form->input('backlink', ['type' => 'hidden', 'value' => $backlink, 'label' => false]); ?>

                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="setor">Setor  
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <?= $this->Form->input('setor_id', [ 'options' => $areaservicos, "class" => "form-control col-md-7 col-xs-12", 'required' => 'required', 'label' => false, "style" => "width: 100%", 'empty' => 'selecione uma opção']); ?>
                    </div>
                </div>
                <div class="form-group grupodocumento-id">
                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="grupo">Grupo 
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <?= $this->Form->input('grupodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                    </div>
                </div>                
                <div class="form-group document-details">
                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="tipo">Documento 
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <?= $this->Form->input('tipodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                    </div>
                </div>

                <div class="form-group document-details">
                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="descricao">Observações
                    </label>
                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <?= $this->Form->input('descricao-anexo', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div>
                </div>  
                <div id="fileField" class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-2 col-sm-2 col-xs-1">
                    </div>
                    <div id="pathFile"  class="col-md-9 col-sm-9 col-xs-7">
                        <?= $this->Form->file('files[]', ['label' => false, 'key' => 0, 'class' => 'file']) ?>      
                    </div>
                </div>
                <div class="clearfix"></div>  
                <div class="document-details col-md-12 col-sm-12 col-xs-12">
                    <div class="ln_solid"></div>
                    <div class="col-md-2 col-sm-2 col-xs-4">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4 btn-addFile">
                        <button class="addFile btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Adicionar Arquivo</button>
                    </div>
                </div>
            </div>
            <div class="form-group files">
                <div id="queue">        
                    <div class="clearfix"></div>  
                    <div class="ln_solid"></div>    
                    <h2>Lista de Documentos:</h2>
                </div>
            </div>  
            <div class="clearfix"></div>  
            <div class="ln_solid"></div>  
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Salvar Todos Documentos</button>
                    <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/index.js'); ?>
<script>
    var keys_anexos = 0;
    $(document).ready(function () {
        var fields = ['empresa-id'];
        $('form').on('submit', function (e) {
            clearFieldsInfo(fields);
        });

        $('.grupodocumento-id').hide();
        $('.document-details').hide();
        $('.btn-addFile').hide();
        $('#queue').hide();

        $("select").select2({minimumResultsForSearch: 6});
        $('.addFile').click(function (e) {
            e.preventDefault();
            if (!$('#setor-id').val() || !$('#grupodocumento-id').val() || !$('#tipodocumento-id').val()) {
                alert('Os campos Setor/Grupo/Tipo de documentos devem ser preenchidos para poder adicionar o arquivo.');
            } else {
                $('#queue').show();
                addAttachment();
            }
        });
        $('select').on('select2:open', function (e) {
            $('.select2-search input').prop('focus', false);
        });
    });

    function addAttachment() {
        getPropertiesFile();
        additemListFiles();
        hideLatestAttachmentField();
        newAttachmentField();
    }
    function getPropertiesFile() {
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='tipo_documentos[" + keys_anexos + "]' value='" + $('#tipodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='grupo_documentos[" + keys_anexos + "]' value='" + $('#grupodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='area_servicos[" + keys_anexos + "]' value='" + $('#setor-id').val() + "'>");
         $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='descricoes[" + keys_anexos + "]' value='" + $('#descricao-anexo').val() + "'>");
    }
    function additemListFiles() {
        $('#queue').append('<div class="clearfix"></div><div class="itemqueue col-md-3 col-sm-3 col-xs-12 well well-sm"  style="padding: 0;" item="' + keys_anexos + '">\
                            <div id="descFile" class="col-md-9 col-sm-9 col-xs-9">' +
                $('#setor-id option:selected').text() + '/ ' + $('#grupodocumento-id option:selected').text() + '/ ' + $('#tipodocumento-id option:selected').text()
                + '</div>\
                <div class="removeFile col-md-3 col-sm-3 col-xs-3">\
                    <button class="remove btn btn-sm btn-danger" type="button" onclick="removeItemListFiles(this,' + keys_anexos + ');"><i class="fa fa-trash"></i></button>\
                </div></div>');
    }
    function hideLatestAttachmentField() {
        $('.file').hide();
    }
    function newAttachmentField() {
        keys_anexos++;
        $('#pathFile').append("<input type='file' class='file' key='" + keys_anexos + "' name='files[]'>");
    }
    function removeItemListFiles(item, key) {
        $(item).parent().parent().remove();
        $(':input[key=' + key + ']').remove();
    }
</script>