<?php
if ($tpview == 'table') {

    $cor = 'even';
    foreach ($documentos as $documento):
        ?>
        <tr class="<?= $cor ?> pointer">
            <td ><?= h($documento->dt_enviado) ?></td>
            <td ><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->descricao, ['controller' => 'Tipodocumentos', 'action' => 'view', $documento->tipodocumento->id]) : '' ?> </td>
            <td ><?= h($documento->nome) ?></td>
            <td ><?= h($documento->descricao) ?></td>
            <td  class=" last">
                <div class="btn-group">
                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $documento->id], ['class' => "btn btn-primary btn-xs"]); ?>
                    <?= $this->Html->link(__('Download'), "/docs/" . $documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                    <?= $this->Form->postLink("Deletar", ['action' => 'delete', $documento->id], ['class' => "btn btn-danger  btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse arquivo?', $documento->id)]) ?>
                </div>
            </td>
        </tr>
        <?php
        $cor = $cor == 'even' ? 'odd' : 'even';
    endforeach;
}else if ($tpview == 'list') {
    echo $retorno;
} else if ($tpview == 'add') {
    echo $retorno;
}
?>