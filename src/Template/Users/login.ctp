<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Triade Consultoria</title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/vendors/animate.css/animate.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

    </head>


    <body class="login">
        <div>
            <div class="login_wrapper">
                <div class="animate form login_form  well well-sm">
                    <section class="login_content">
                        <?= $this->Form->create() ?>
                        <div>
                            <?php echo $this->Html->image("logo_p.jpg", ["alt" => "Sistema Doce Desejo"]); ?>
                        </div>
                        <!--<h1>Doce Desejo</h1>-->
                        <p><?= $this->Flash->render() ?></p>
                        <p><?= $this->Flash->render('auth') ?></p>
                        <div>
                            <?= $this->Form->input('username', ['label' => false, "class" => "form-control", "placeholder" => "Usuário"]) ?>
                            <?= $this->Form->input('password', ['label' => false, "class" => "form-control", "placeholder" => "Senha"]) ?>
                        </div>
                        <div>
                            <?= $this->Form->button(__('Entrar'), ["class" => "btn btn-default submit"]); ?>
                        </div>

                        <div class="clearfix"></div>

                        <?= $this->Form->end() ?>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
