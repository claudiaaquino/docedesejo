<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($user->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Nome</p>
                        <p><?= h($user->nome) ?></p>
                        <?php if ($user->empresa) { ?>
                            <p class="title">Empresa</p>
                            <p><?= h($user->empresa->razao) ?></p>
                        <?php } ?>
                        <p class="title">Usuário (login)</p>
                        <p><?= h($user->username) ?></p>
                        <p class="title">Email</p>
                        <p><?= $user->email ?></p>
                        <?php if ($admin && $user->tipousuario) { ?>
                            <p class="title">Tipo de Usuário</p>
                            <p><?= $user->has('tipousuario') ? $this->Html->link($user->tipousuario->descricao, ['controller' => 'Tipousuarios', 'action' => 'view', $user->tipousuario->id]) : '' ?></p>
                        <?php } ?>

                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($admin || $admin_empresa) { ?>
                            <p class="title">Admin da empresa?</p>
                            <p><?= $user->admin_empresa ? 'Sim' : 'Não' ?></p>
                        <?php } ?>
                        <p class="title">Últ. login</p>
                        <p><?= $user->last_login ?></p>
                        <p class="title">Últ. atualização</p>
                        <p><?= $user->last_update ?></p>
                        <p class="title">Status</p>
                        <p><?= $user->status == 1 ? "Ativo" : "Inativo" ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (!empty($user->documentos) && $admin) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Documentos de <?= $user->nome ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title">ID</th>
                                    <th scope="col" class="column-title">Nome</th>
                                    <th scope="col" class="column-title">Descrição</th>
                                    <th scope="col" class="column-title">Lido</th>
                                    <th scope="col" class="column-title">Enviado</th>
                                    <th scope="col" class="column-title">Últ. Atualização</th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($user->documentos as $documento) {
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= h($documento->id) ?></td>
                                        <td><?= h($documento->nome) ?></td>
                                        <td><?= h($documento->descricao) ?></td>
                                        <td><?= !empty($documento->dt_lido) ? $documento->dt_lido : "Não lido"; ?></td>
                                        <td><?= !empty($documento->dt_enviado) ? $documento->dt_enviado : "Não enviado"; ?></td>
                                        <td><?= h($documento->last_update) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Documentos', 'action' => 'view', $documento->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Documentos', 'action' => 'edit', $documento->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documento->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo deletar o documento #{0}?', $documento->id)]) ?>
                                            </div>
                                        </td>    
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>            
                        </table>    
                    </div>    
                </div>
            </div>    
            <?php
        }
        ?>

        <?php
        if (!empty($user->usermodulos) && $admin) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Módulos ao qual tem Permissão</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col" class="column-title">Módulo</th>
                                    <th scope="col" class="column-title">Descrição</th>
                                    <th scope="col" class="column-title">Dt. Vinculo</th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($user->usermodulos as $modulo) {
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $this->Html->link($modulo->modulo->nome, ['controller' => 'Modulos', 'action' => 'view', $modulo->modulo_id]) ?></td>
                                        <td><?= h($modulo->modulo->descricao) ?></td>
                                        <td><?= h($modulo->dt_cadastro) ?></td>
                                        <td class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Módulo'), ['controller' => 'Modulos', 'action' => 'view', $modulo->modulo_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvincular do Móduo'), ['controller' => 'Usermodulos', 'action' => 'delete', $modulo->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo deletar o módulo #{0}?', $modulo->id)]) ?>
                                            </div>
                                        </td>    
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>            
                        </table>    
                    </div>    
                </div>
            </div>
            <?php
        }
        ?>
        <?php
        if (!empty($user->usersareaservicos) && $admin) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Setores do qual faz parte</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col" class="column-title">Setor</th>
                                    <th scope="col" class="column-title">Admin do Setor?</th>
                                    <th scope="col" class="column-title">Dt. Vínculo</th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($user->usersareaservicos as $userareaservicos) {
                                    //echo "<pre>";
                                    //print_r($modulo);exit;
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td> <?= $this->Html->link($userareaservicos->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $userareaservicos->areaservico_id]) ?></td>
                                        <td><?= $userareaservicos->admin_setor ? 'Sim' : 'Não' ?></td>
                                        <td><?= h($userareaservicos->dt_cadastro) ?></td>
                                        <td class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Setor'), ['controller' => 'Areaservicos', 'action' => 'view', $userareaservicos->areaservico_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvincular desse Setor'), ['controller' => 'Userareaservicos', 'action' => 'delete', $userareaservicos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo deletar o módulo #{0}?', $userareaservicos->id)]) ?>
                                            </div>
                                        </td>    
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>            
                        </table>    
                    </div>    
                </div>
            </div>
            <?php
        }
        ?>

    </div>
</div>
