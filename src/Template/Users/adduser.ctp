<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Cadastro de Usuário <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#"></a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!--<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">-->
                <?= $this->Form->create($user, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>  
                <h3 class="well well-sm">Informações Principais</h3>
                <?php if ($admin) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_id">Empresa para qual empresa <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('empresa_id', ['options' => $empresas, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>            
                <?php } ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Usuário (login) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('username', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div>
                </div>            
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Nome Completo <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('email', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Senha <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('password', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div>
                </div>
                <br>
                <br>
                <?php if ($admin) { ?>
                    <h3 class="well well-sm">Permissões no Sistema</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="modulo_ids">Módulos que deseja liberar acesso para esse usuário
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('modulo_ids', ['options' => $modulos, "class" => "form-control", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipousuario_id">Tipo de Usuário <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('tipousuario_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>
                    <br>
                    <br>
                    <?php
                }
                if ($admin || $admin_empresa) {
                    ?>
                    <h3 class="well well-sm">Atribuições na Empresa</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="setor_ids">Setor(es) do qual faz parte
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('setor_ids', ['options' => $areaservicos, "class" => "form-control col-md-7 col-xs-12", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_empresa">Será o administrador desse(s) setor(es)? <span class="required">*</span> <i class="fa fa-question-circle" data-placement="right" data-toggle="tooltip" data-original-title="Isso significa que esse usuário poderá receber todas as mensagens, tarefas e documentos relativas a esse setor, além de poder fazer os cadastros do setor."></i>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->checkbox('admin_setor', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_empresa">Será ADMIN da empresa? <span class="required">*</span> <i class="fa fa-question-circle" data-placement="right" data-toggle="tooltip" data-original-title="Isso significa que esse usuário poderá receber todas as mensagens, tarefas e documentos relativas à empresa, além de poder utilizar as funcionalidades principais do sistema e fazer os cadastros da empresa."></i>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->checkbox('admin_empresa', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<script>

    $(document).ready(function () {
        $("select").select2();
        $("#modulo-ids").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione um módulo ou digite um novo'});
        $("#setor-ids").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione um setor ou digite um novo'});
    });


</script>