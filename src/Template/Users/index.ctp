<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Usuários</h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'adduser'], ['class' => "btn btn-dark fa fa-file"]) ?> </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('nome', "Nome") ?></th>
                                <?php if ($admin) { ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Empresas.razao', 'Empresa') ?></th>                   
                                <?php } ?>
                                <?php /* <th scope="col" class="column-title"><?= $this->Paginator->sort('tipousuario_id', 'Tipo de Usuário') ?></th> */ ?>

                                <th scope="col" class="column-title"><?= $this->Paginator->sort('email', 'Email') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_login', 'Últ. login') ?></th>                               
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('admin_empresa', 'ADMIN') ?></th>                                
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $cor = 'even';
                            foreach ($users as $user) {
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $user->id ?>">
                                    </td>
                                    <td><?= h($user->nome) ?></td>
                                    <?php if ($admin) { ?>
                                        <td><?= $user->has('empresa') ? $this->Html->link($user->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $user->empresa->id]) : '' ?></td>
                                    <?php } ?>
                                    <?php /* if ($admin) { ?>
                                      <td><?= $user->has('tipousuario') ? $this->Html->link($user->tipousuario->descricao, ['controller' => 'Tipousuarios', 'action' => 'view', $user->tipousuario->id]) : '' ?></td>
                                      <?php } else { ?>
                                      <td><?= $user->tipousuario->descricao ?></td>
                                      <?php } */ ?>
                                    <td><?= h($user->email) ?></td>
                                    <td><?= h($user->last_login) ?></td>
                                    <td><?= $user->admin_empresa ? 'SIM' : 'NÃO'; ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $user->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $user->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo deletar o usuário #{0}?', $user->id)]) ?>

                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                </div>        
            </div>    
        </div>
    </div>
</div>