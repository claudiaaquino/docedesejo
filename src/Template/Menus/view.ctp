<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Menu - <?= h($menu->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $menu->descricao ? $menu->descricao : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Ícone') ?></p>
                        <p><?= $menu->iconname ? "<i class='fa fa-{$menu->iconname}'></i>" : 'Não Informado'; ?></p>

                        <?php /* if ($menu->areaservico_id) { ?>
                          <p class="title"><?= __('Setor (informar se for específico desse setor)') ?></p>
                          <p><?= $menu->areaservico_id ? $menu->areaservico->descricao : 'Não Informado'; ?></p>
                          <?php } ?>

                          <?php if ($menu->ordem) { ?>
                          <p class="title"><?= __('Ordem que aparecerá na tela para o usuário') ?></p>
                          <p><?= $menu->ordem ? $this->Number->format($menu->ordem) : 'Não Informado'; ?></p>
                          <?php } */ ?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Dt. Cadastro') ?></p>
                        <p><?= $menu->dt_cadastro ? $menu->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $menu->last_update ? $menu->last_update : 'Não modificado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $menu->status ? __('Ativo') : __('Desativado'); ?></p>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $menu->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $menu->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $menu->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($menu->menusubmenus)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Submenus dentro desse Menu </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Submenu') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Vínculada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($menu->menusubmenus as $menusubmenus):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td> <?= $this->Html->link($menusubmenus->submenu->nome, ['controller' => 'Submenus', 'action' => 'view', $menusubmenus->submenu_id]) ?></td>
                                        <td><?= h($menusubmenus->dt_cadastro) ?></td>
                                        <td><?= $menusubmenus->status ? 'Ativo' : 'Desativado' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Submenu'), ['controller' => 'Submenus', 'action' => 'view', $menusubmenus->submenu_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvincular desse Submenu'), ['controller' => 'Menusubmenus', 'action' => 'delete', $menusubmenus->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $menusubmenus->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($menu->modulosmenus)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Módulos que esse Menu está incluído </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Módulo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Vínculada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($menu->modulosmenus as $modulosmenus):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td> <?= $this->Html->link($modulosmenus->modulo->descricao, ['controller' => 'Modulos', 'action' => 'view', $modulosmenus->modulo_id]) ?></td>
                                        <td><?= h($modulosmenus->dt_cadastro) ?></td>
                                        <td><?= $modulosmenus->status ? 'Ativo' : 'Desativado' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Módulo'), ['controller' => 'Modulos', 'action' => 'view', $modulosmenus->modulo_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvincular desse Módulo'), ['controller' => 'Modulosmenus', 'action' => 'delete', $modulosmenus->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $modulosmenus->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


