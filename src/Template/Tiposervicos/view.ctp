<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tiposervico->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Subsetor</p>
                        <p><?= $tiposervico->has('gruposervico') ? $this->Html->link($tiposervico->gruposervico->descricao, ['controller' => 'Gruposervicos', 'action' => 'view', $tiposervico->gruposervico->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Nome') ?></p>
                        <p><?= $tiposervico->nome ? $tiposervico->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $tiposervico->descricao ? $tiposervico->descricao : 'Não Informado'; ?></p>
                        
                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $tiposervico->last_update ? $tiposervico->last_update : 'Não houve alteração'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $tiposervico->dt_cadastro ? $tiposervico->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $tiposervico->status ? __('Ativo') : __('Desativado'); ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($tiposervico->contratos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Contratos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor Servico') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Filecontrato') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Contrato') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiposervico->contratos as $contratos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($contratos->id) ?>">
                                        </td>
                                        <td><?= h($contratos->id) ?></td>
                                        <td><?= h($contratos->tiposervico_id) ?></td>
                                        <td><?= h($contratos->user_id) ?></td>
                                        <td><?= h($contratos->valor_servico) ?></td>
                                        <td><?= h($contratos->filecontrato) ?></td>
                                        <td><?= h($contratos->dt_contrato) ?></td>
                                        <td><?= h($contratos->status) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Contratos', 'action' => 'view', $contratos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Contratos', 'action' => 'edit', $contratos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Contratos', 'action' => 'delete', $contratos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $contratos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($tiposervico->previsaoorcamentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Previsão de Orçamentos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Apuracaoforma Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Atuacaoramo Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Min Funcionarios') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Max Funcionarios') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Min Faturamento Mensal') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Max Faturamento Mensal') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor Servico') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiposervico->previsaoorcamentos as $previsaoorcamentos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($previsaoorcamentos->id) ?>">
                                        </td>
                                        <td><?= h($previsaoorcamentos->id) ?></td>
                                        <td><?= h($previsaoorcamentos->tiposervico_id) ?></td>
                                        <td><?= h($previsaoorcamentos->apuracaoforma_id) ?></td>
                                        <td><?= h($previsaoorcamentos->atuacaoramo_id) ?></td>
                                        <td><?= h($previsaoorcamentos->min_funcionarios) ?></td>
                                        <td><?= h($previsaoorcamentos->max_funcionarios) ?></td>
                                        <td><?= h($previsaoorcamentos->min_faturamento_mensal) ?></td>
                                        <td><?= h($previsaoorcamentos->max_faturamento_mensal) ?></td>
                                        <td><?= h($previsaoorcamentos->valor_servico) ?></td>
                                        <td><?= h($previsaoorcamentos->user_id) ?></td>
                                        <td><?= h($previsaoorcamentos->dt_cadastro) ?></td>
                                        <td><?= h($previsaoorcamentos->last_update) ?></td>
                                        <td><?= h($previsaoorcamentos->status) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Previsaoorcamentos', 'action' => 'view', $previsaoorcamentos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Previsaoorcamentos', 'action' => 'edit', $previsaoorcamentos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Previsaoorcamentos', 'action' => 'delete', $previsaoorcamentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $previsaoorcamentos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($tiposervico->tarefas)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Tarefas Vínculadas </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Areaservico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tarefatipo Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tarefaprioridade Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Titulo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descricao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Inicio') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Final') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Lembrete') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Concluida') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Adiada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Adiada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Automatica') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Admin') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Permite Deletar') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Cadastrou') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiposervico->tarefas as $tarefas):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($tarefas->id) ?>">
                                        </td>
                                        <td><?= h($tarefas->id) ?></td>
                                        <td><?= h($tarefas->empresa_id) ?></td>
                                        <td><?= h($tarefas->areaservico_id) ?></td>
                                        <td><?= h($tarefas->tarefatipo_id) ?></td>
                                        <td><?= h($tarefas->tiposervico_id) ?></td>
                                        <td><?= h($tarefas->user_id) ?></td>
                                        <td><?= h($tarefas->tarefaprioridade_id) ?></td>
                                        <td><?= h($tarefas->titulo) ?></td>
                                        <td><?= h($tarefas->descricao) ?></td>
                                        <td><?= h($tarefas->dt_inicio) ?></td>
                                        <td><?= h($tarefas->dt_final) ?></td>
                                        <td><?= h($tarefas->dt_lembrete) ?></td>
                                        <td><?= h($tarefas->dt_concluida) ?></td>
                                        <td><?= h($tarefas->adiada) ?></td>
                                        <td><?= h($tarefas->dt_adiada) ?></td>
                                        <td><?= h($tarefas->automatica) ?></td>
                                        <td><?= h($tarefas->admin) ?></td>
                                        <td><?= h($tarefas->permite_deletar) ?></td>
                                        <td><?= h($tarefas->dt_cadastro) ?></td>
                                        <td><?= h($tarefas->user_cadastrou) ?></td>
                                        <td><?= h($tarefas->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Tarefas', 'action' => 'view', $tarefas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefas', 'action' => 'delete', $tarefas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefas->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($tiposervico->telaquestionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Tela de Questionarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Telaquestionario Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Assunto Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Lei Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tela Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tipoaction Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Pergunta') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Resposta') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Exigeconfirmacao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiposervico->telaquestionarios as $telaquestionarios):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($telaquestionarios->id) ?>">
                                        </td>
                                        <td><?= h($telaquestionarios->id) ?></td>
                                        <td><?= h($telaquestionarios->telaquestionario_id) ?></td>
                                        <td><?= h($telaquestionarios->assunto_id) ?></td>
                                        <td><?= h($telaquestionarios->estado_id) ?></td>
                                        <td><?= h($telaquestionarios->cidade_id) ?></td>
                                        <td><?= h($telaquestionarios->lei_id) ?></td>
                                        <td><?= h($telaquestionarios->tela_id) ?></td>
                                        <td><?= h($telaquestionarios->tipoaction_id) ?></td>
                                        <td><?= h($telaquestionarios->tiposervico_id) ?></td>
                                        <td><?= h($telaquestionarios->pergunta) ?></td>
                                        <td><?= h($telaquestionarios->resposta) ?></td>
                                        <td><?= h($telaquestionarios->exigeconfirmacao) ?></td>
                                        <td><?= h($telaquestionarios->user_id) ?></td>
                                        <td><?= h($telaquestionarios->dt_cadastro) ?></td>
                                        <td><?= h($telaquestionarios->last_update) ?></td>
                                        <td><?= h($telaquestionarios->status) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Telaquestionarios', 'action' => 'view', $telaquestionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Telaquestionarios', 'action' => 'edit', $telaquestionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Telaquestionarios', 'action' => 'delete', $telaquestionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $telaquestionarios->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


