<div class="page-title">
    <div class="title_left">
        <h3>Detalhes do Tipo de Usuário</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tipousuario->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Status</p>
                        <p><?= $tipousuario->status == 1 ? 'Ativo' : 'Inativo' ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Ultima Atualização</p>
                        <p><?= $tipousuario->last_update; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (!empty($tipousuario->users)) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i>Usuários do tipo <?=h($tipousuario->descricao)?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= __('ID') ?></th>
                                    <th scope="col" class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col" class="column-title"><?= __('Username') ?></th>
                                    <th scope="col" class="column-title"><?= __('Status') ?></th>
                                    <th scope="col" class="column-title"><?= __('Últ. Login') ?></th>
                                    <th scope="col" class="column-title"><?= __('Últ. Atualização') ?></th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($tipousuario->users as $users) {
                                    ?>

                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= h($users->id) ?></td>
                                        <td><?= h($users->nome) ?></td>
                                        <td><?= h($users->username) ?></td>
                                        <td><?= $users->status == 1 ? 'Ativo' : 'Inativo'; ?></td>
                                        <td><?= h($users->last_login) ?></td> 
                                        <td><?= h($users->last_update) ?></td>  
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Users', 'action' => 'view', $users->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Users', 'action' => 'edit', $users->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $users->id)]) ?>

                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>