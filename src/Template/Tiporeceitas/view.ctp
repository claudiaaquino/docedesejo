<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tiporeceita->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Descricao') ?></p>
                        <p><?= $tiporeceita->descricao ? $tiporeceita->descricao : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $tiporeceita->dt_cadastro ? $tiporeceita->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $tiporeceita->last_update ? $tiporeceita->last_update : 'Não foi modificado'; ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($tiporeceita->movimentacaobancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Movimentação de Caixa dessa Forma de Receita </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Dt Lançamento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cliente') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Conta Bancária') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Forma de Pagamento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor da Movimentação') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Saldo Posterior') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Pago') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiporeceita->movimentacaobancarias as $movimentacaobancarias):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($movimentacaobancarias->id) ?>">
                                        </td>
                                        <td><?= h($movimentacaobancarias->dt_cadastro) ?></td>
                                       <td><?= $movimentacaobancarias->has('cliente') ? $this->Html->link($movimentacaobancarias->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $movimentacaobancarias->cliente->id]) : '--' ?></td>
                                        <td><?= $movimentacaobancarias->has('contabancaria') ? $movimentacaobancarias->contabancaria->banco->nome . ($movimentacaobancarias->contabancaria->conta ? ' (' . $movimentacaobancarias->contabancaria->conta . '-' . $movimentacaobancarias->contabancaria->co_digito . ')' : '') : 'Não informado' ?></td>
                                        <td><?= $movimentacaobancarias->has('formaspagamento') ? $movimentacaobancarias->formaspagamento->descricao . ( $movimentacaobancarias->avista ? ' à vista' : ' parcelado') : '' ?></td>
                                        <td>R$ <?= $this->Number->format($movimentacaobancarias->valor) ?></td>
                                        <td>R$ <?= $this->Number->format($movimentacaobancarias->saldo_posterior) ?></td>
                                        <td><?= $movimentacaobancarias->pago ? 'PAGO' : 'PENDENTE' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacaobancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Movimentacaobancarias', 'action' => 'edit', $movimentacaobancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Movimentacaobancarias', 'action' => 'delete', $movimentacaobancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $movimentacaobancarias->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


