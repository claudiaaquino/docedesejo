<?php $this->start('menu'); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <?php foreach ($menus as $menu) { ?>
            <li><?= $menu->descricao ?></li>
            <?php foreach ($menu->submenus as $submenu) { ?>
                <li><?= $this->Html->link(__($menu->descricao), ['controller' => ucfirst($submenu->nome)]) ?></li>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>
<?php $this->end(); ?>