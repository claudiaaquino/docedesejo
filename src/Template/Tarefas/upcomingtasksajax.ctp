<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
    <i class="fa fa-calendar"></i>
    <?php if ($count_upcoming > 0) { ?><span class="badge bg-green"><?= $count_upcoming; ?></span><?php } ?>
</a>
<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
    <?php foreach ($tarefas as $tarefa) { ?>
        <li>
            <a href="<?= $this->request->webroot ?>tarefas/view/<?= h($tarefa->id) ?>">
                <span class="image">
                    <!--<img src="images/img.jpg" alt="Profile Image" />-->
                </span>
                <span>
                    <span><?= h($tarefa->titulo) ?></span>
                    <span class="time"><?= h($tarefa->dt_inicio) ?></span>
                </span>
                <span class="message">
                    <?= h($tarefa->descricao) ?>
                </span>
            </a>
        </li>
        <?php
    }
    if ($count_upcoming <= 0) {
        ?>
        <li>
            <div class="text-center">
                <strong>Não há tarefas próximas ou pendentes...</strong>
            </div>
        </li>
    <?php } ?>   
    <li>
        <div class="text-center">
            <a href="<?= $this->request->webroot ?>tarefas/agenda">
                <strong>Ver minha agenda</strong>
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </li>
</ul>