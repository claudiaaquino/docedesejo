<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Editar informações da Tarefa  <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($admin || $loggeduser == $tarefa->user_id) { ?>
                        <li><?= $this->Html->link(__(' Visualizar Tarefa'), ['controller' => 'Tarefas', 'action' => 'view', $tarefa->id], ['class' => "btn btn-dark fa fa-file-o"]) ?>  </li>
                        <li><?= $this->Html->link(__(' Anexar Documentos'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-paperclip"]) ?>  </li>
                    <?php } ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tarefa, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>                
                <h3>Informações Principais</h3>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">Título <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('titulo', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>              

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tarefatipo_id">Tipo de Tarefa <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tarefatipo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefatipos]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tarefaprioridade_id">Prioridade 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tarefaprioridade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefaprioridades, 'empty' => true]); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_inicio">Dt Início da tarefa <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_inicio', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_inicio">Hora da tarefa 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('hr_tarefa', ["class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99:99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_final">Dt. Final 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_final', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_lembrete">Dt. Lembrete no sistema 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_lembrete', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>      


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>      

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_concluida">Dt. da Conlusão
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_concluida', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         


              
                <div class="clearfix ln_solid">  </div>              

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    <?php if (!empty($tarefa->tarefadocumentos)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col"  class="column-title"><?= __('Documento') ?></th>
                                <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                <th scope="col"  class="column-title"><?= __('Tamanho do Arquivo') ?></th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tarefa->tarefadocumentos as $tarefadocumentos):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td><?= h($tarefadocumentos->documento->tipodocumento->descricao) ?></td>
                                    <td><?= h($tarefadocumentos->documento->descricao) ?></td>
                                    <td><?= ($tarefadocumentos->documento->filesize / 1024) < 1024 ? floor($tarefadocumentos->documento->filesize / 1024) . ' KB' : floor(($tarefadocumentos->documento->filesize / 1024) / 1024) . ' MB'; ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Download'), "/docs/" . $tarefadocumentos->documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefadocumentos', 'action' => 'delete', $tarefadocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefadocumentos->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($tarefa->tarefausuarios)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Usuários Responsáveis por essa Tarefa </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefausuarios', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col"  class="column-title">Nome do Responsável</th>
                                <th scope="col"  class="column-title">Dt Concluída</th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tarefa->tarefausuarios as $tarefausuarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td><?= h($tarefausuarios->user->nome) ?></td>
                                    <td><?= $tarefausuarios->dt_concluida ? $tarefausuarios->dt_concluida : 'NÃO CONCLUÍDA' ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefausuarios', 'action' => 'delete', $tarefausuarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefausuarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>

<script>
    $(document).ready(function () {
        $(":input").inputmask();
    });
</script>