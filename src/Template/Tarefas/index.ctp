
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Minha Agenda</h2> 
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_inicio') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Tarefatipos.descricao', 'Tipo') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Tarefaprioridades.descricao', 'Prioridade') ?></th>
                                <?php if ($admin) { ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Areaservicos.descricao', 'Setor') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Tiposervicos.descricao', 'Tipo de Serviço') ?></th>
                                <?php } ?>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('titulo') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_lembrete') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_concluida', 'Status') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tarefas as $tarefa) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $tarefa->id ?>">
                                    </td>

                                    <td><?= h($tarefa->dt_inicio) ?></td>
                                    <td><?= $tarefa->has('tarefatipo') ? $this->Html->link($tarefa->tarefatipo->descricao, ['controller' => 'Tarefatipos', 'action' => 'view', $tarefa->tarefatipo->id]) : '' ?></td>
                                    <td><?= $tarefa->has('tarefaprioridade') ? $this->Html->link($tarefa->tarefaprioridade->descricao, ['controller' => 'Tarefaprioridades', 'action' => 'view', $tarefa->tarefaprioridade->id]) : '' ?></td>
                                    <?php if ($admin) { ?>
                                        <td><?= $tarefa->has('areaservico') ? $this->Html->link($tarefa->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tarefa->areaservico->id]) : '' ?></td>
                                        <td><?= $tarefa->has('tiposervico') ? $this->Html->link($tarefa->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $tarefa->tiposervico->id]) : '' ?></td>
                                    <?php } ?>
                                    <td><?= h($tarefa->titulo) ?></td>
                                    <td><?= h($tarefa->dt_lembrete) ?></td>
                                    <td><?= $tarefa->dt_concluida?'Concluída':'Pendente' ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $tarefa->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tarefa->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $tarefa->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $tarefa->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
