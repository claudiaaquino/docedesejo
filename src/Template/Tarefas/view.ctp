<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Informações da Tarefa</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($admin || ($loggeduser == $tarefa->user_id)) { ?>
                        <li><?= $this->Html->link(__(' Editar Tarefa'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefa->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?>  </li>
                        <li><?= $this->Html->link(__(' Anexar Documentos'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-paperclip"]) ?>  </li>
                    <?php } ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="project_detail">


                        <p class="title"><?= __('Título') ?></p>
                        <p>  <?= $tarefa->titulo ? $tarefa->titulo : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Tipo de Tarefa') ?></p>
                        <p><?= $tarefa->has('tarefatipo') ? $this->Html->link($tarefa->tarefatipo->descricao, ['controller' => 'Tarefatipos', 'action' => 'view', $tarefa->tarefatipo->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Prioridade') ?></p>
                        <p><?= $tarefa->has('tarefaprioridade') ? $this->Html->link($tarefa->tarefaprioridade->descricao, ['controller' => 'Tarefaprioridades', 'action' => 'view', $tarefa->tarefaprioridade->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p>  <?= $tarefa->descricao ? $tarefa->descricao : 'Não Informado'; ?></p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Dia da Tarefa') ?></p>
                        <p><?= $tarefa->dt_inicio ? $tarefa->dt_inicio : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Data Final da Tarefa') ?></p>
                        <p><?= $tarefa->dt_final ? $tarefa->dt_final : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Dt Lembrete no sistema') ?></p>
                        <p><?= $tarefa->dt_lembrete ? $tarefa->dt_lembrete : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $tarefa->dt_cadastro ? $tarefa->dt_cadastro : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $tarefa->last_update ? $tarefa->last_update : 'Não foi modificada'; ?></p>

                        <?php if ($tarefa->dt_concluida) { ?>
                            <p class="title"><?= __('Dt Conclusão') ?></p>
                            <p><?= $tarefa->dt_concluida ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($tarefa->empresa) { ?>
            <div class="x_panel">         
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Informações Específicas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="project_detail">

                            <p class="title"><?= __('Empresa que foi atribuida a tarefa') ?></p>
                            <p><?= $tarefa->has('empresa') ? $this->Html->link($tarefa->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $tarefa->empresa->id]) : 'Não Informado' ?></p>

                            <?php if ($tarefa->areaservico) { ?>
                                <p class="title"><?= __('Setor da Empresa') ?></p>
                                <p><?= $tarefa->has('areaservico') ? $this->Html->link($tarefa->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tarefa->areaservico->id]) : 'Não Informado' ?></p>
                            <?php } ?>

                            <?php if ($tarefa->tiposervico) { ?>
                                <p class="title"><?= __('Serviço refente à Tarefa') ?></p>
                                <p><?= $tarefa->has('tiposervico') ? $this->Html->link($tarefa->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $tarefa->tiposervico->id]) : 'Não Informado' ?></p>
                            <?php } ?>                     

                            <?php if ($admin && $tarefa->tarefausuarios) { ?>
                                <p class="title"><?= __('Usuário que cadastrou a tarefa') ?></p>
                                <p><?= $tarefa->has('user') ? $this->Html->link($tarefa->user->nome, ['controller' => 'Users', 'action' => 'view', $tarefa->user->id]) : 'Não Informado' ?></p>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="project_detail">

                            <?php if ($tarefa->admin_empresa) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> <?= __('Essa Tarefa foi atribuida SOMENTE para os Administradores da Empresa') ?></p><br>
                            <?php } ?>                     


                            <?php if ($tarefa->admin_setor) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> Essa Tarefa foi atribuida SOMENTE para os <div class="green">Administradores do Setor <?= $tarefa->areaservico->descricao ?> da Empresa <?= $tarefa->empresa->razao ?></div></p><br>
                            <?php } ?>    

                            <?php if ($tarefa->automatica) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> <?= __('Essa Tarefa foi gerada automaticamente pelo sistema') ?></p><br>
                            <?php } ?>

                            <?php if ($tarefa->permite_deletar) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> Essa Tarefa PODE SER DELETADA pelas pessoas atribuidas à ela.</p><br>
                            <?php } else { ?>
                                <p class="title"><i class="fa fa-tag red"></i> Essa Tarefa NÃO PODE SER DELETADA pelas pessoas atribuidas à ela.<br> Somente pelo admin da Tríade ou pelo próprio criador dessa tarefa</p><br>
                            <?php } ?>

                            <?php if ($tarefa->permite_editar) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> Essa Tarefa PODE SER EDITADA pelas pessoas atribuidas à ela.</p><br>
                            <?php } else { ?>
                                <p class="title"><i class="fa fa-tag red"></i> Essa Tarefa NÃO PODE SER EDITADA pelas pessoas atribuidas à ela.<br> Somente pelo admin da Tríade ou pelo próprio criador dessa tarefa</p><br>
                            <?php } ?>


                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php if (!empty($tarefa->tarefadocumentos)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th scope="col"  class="column-title"><?= __('Documento') ?></th>
                            <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                            <th scope="col"  class="column-title"><?= __('Tamanho do Arquivo') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($tarefa->tarefadocumentos as $tarefadocumentos):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td><?= h($tarefadocumentos->documento->tipodocumento->descricao) ?></td>
                                <td><?= h($tarefadocumentos->documento->descricao) ?></td>
                                <td><?= ($tarefadocumentos->documento->filesize / 1024) < 1024 ? floor($tarefadocumentos->documento->filesize / 1024) . ' KB' : floor(($tarefadocumentos->documento->filesize / 1024) / 1024) . ' MB'; ?></td>

                                <td  class=" last">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('Download'), "/docs/" . $tarefadocumentos->documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefadocumentos', 'action' => 'delete', $tarefadocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefadocumentos->id)]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (!empty($tarefa->tarefausuarios)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Usuários Responsáveis por essa Tarefa </h2>
            <ul class="nav navbar-right panel_toolbox">
                <?php if ($tarefa->permite_editar) { ?>
                    <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefausuarios', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                <?php } ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th scope="col"  class="column-title">Nome do Responsável</th>
                            <th scope="col"  class="column-title">Dt Concluída</th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($tarefa->tarefausuarios as $tarefausuarios):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td><?= h($tarefausuarios->user->nome) ?></td>
                                <td><?= $tarefausuarios->dt_concluida ? $tarefausuarios->dt_concluida : 'NÃO CONCLUÍDA' ?></td>
                                <?php if ($tarefa->permite_deletar || $tarefa->user_id == $tarefausuarios->user->id) { ?>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefausuarios', 'action' => 'delete', $tarefausuarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefausuarios->id)]) ?>
                                        </div>
                                    </td>
                                <?php } ?>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>
</div>


