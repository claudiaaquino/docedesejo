<?= $this->Html->css('/vendors/fullcalendar/dist/fullcalendar.min.css') ?>
<?= $this->Html->css('/vendors/fullcalendar/dist/fullcalendar.print.css', ['media' => 'print']) ?>
<?= $this->Html->css('agenda.css') ?>
<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>

<script type="text/javascript">
    var calendar;
</script>

<div class="row">
    <div class="" role="main">
        <div class="x_panel">
            <div class="x_title">
                <h2>Agenda</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </li>
                    <li>
                        <a class="close-link">
                            <i class="fa fa-close"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div id='calendar'></div>
            </div>
        </div>
    </div>

    <!-- calendar modal New -->
    <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Nova Tarefa</h4>
                </div>
                <div class="modal-body">
                    <div class="btn-group">
                        <a class="btn btn-principal"  title="Principal"><i class="fa fa-book"></i></a>
                    </div>
                    
                    <div class="btn-group">
                        <a class="btn btn-anexo"  title="Anexos"><i class="fa fa-paperclip"></i>&nbsp;<b class="caret"></b></a>
                    </div>
                    <div class="clearfix ln_solid"></div>
                    <form id="antoform" class="form-horizontal calender formNew" action="/tarefas/add" data-form="formNew" role="form" enctype="multipart/form-data" method="POST">
                        <div id="principal">
                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="tarefatipo_id">Tipo <span class="required">*</span> <?= $this->Html->link('', ['controller' => 'Tarefatipos', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        </label>
                                <div class="col-md-4 col-sm-3 col-xs-8">
                                    <?= $this->Form->input('tarefatipo_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $tarefatipos, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div>
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="tarefaprioridade_id">Prioridade</label>
                                <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;" >
                                    <?= $this->Form->input('tarefaprioridade_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $tarefaprioridades, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="titulo">Título <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3 col-xs-8">
                                    <?= $this->Form->input('titulo', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                </div>                  
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="descricao">Hora da Tarefa</label>
                                <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;">
                                    <?= $this->Form->input('hr_tarefa', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'data-inputmask' => "'mask': '99:99'"]); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="dt_final">Dt. Final</label>
                                <div class="col-md-4 col-sm-3 col-xs-8">
                                    <?= $this->Form->input('dt_final', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>

                                </div> 
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="dt_lembrete">Dt. Lembrete</label>
                                <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 10px;">
                                    <?= $this->Form->input('dt_lembrete', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                                </div> 
                            </div> 
                            <div class="form-group">          
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="descricao">Descrição</label>
                                <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                                    <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                </div>
                            </div>

                        </div>
                        <div id="anexos"  class="col-md-12 col-sm-12 col-xs-12">
                            <div id="attachment-fields">

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="setor">Setor  
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-8">
                                        <?= $this->Form->input('setor_id', [ 'options' => $areaservicos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'selecione uma opção']); ?>
                                    </div>
                                    <div class="grupodocumento-id">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-4" for="grupo">Grupo 
                                        </label>
                                        <div class="col-md-4 col-sm-4 col-xs-8">
                                            <?= $this->Form->input('grupodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                        </div>
                                    </div>
                                </div>                
                                <div class="form-group document-details">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="tipo">Documento 
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-8">
                                        <?= $this->Form->input('tipodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                    
                                </div>                      
                                <div class="form-group document-details">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="descricao">Observação
                                    </label>
                                    <div class="col-md-10 col-sm-10 col-xs-8">
                                        <?= $this->Form->input('descricao-anexo', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                    </div>
                                </div>  
                                <div id="fileField" class="col-md-12 col-sm-12 col-xs-12">
                                    <div id="pathFile"  class="col-md-10 col-sm-10 col-xs-8">
                                        <?= $this->Form->file('file[]', ['label' => false, 'key' => 0, 'class' => 'file']) ?>      
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <button class="addFile btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Adicionar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-attach-view"  title="Anexos"><i class="fa fa-paperclip"></i>&nbsp; <span id="action-all-attach">Ver</span> anexos &nbsp;<b class="caret"></b></a>
                            </div>
                            <div class="form-group files"  style="display: none;">
                                <div id="queue">                        
                                </div>
                            </div> 
                        </div>
                        <?= $this->Form->input('dt_inicio', ['type' => 'hidden'], ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose btn-cancel" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnSave">Salvar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de Edit -->
    <div id="CalenderModalView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Detalhes da Tarefa" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Detalhes da Tarefa</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 col-sm-12 col-xs-12" id="tituloInfo">
                        <label class="label-input col-md-3 col-sm-3 col-xs-12">Título:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="tituloField">
                        </div> 
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12" id="tipotarefaInfo"  style="padding-top: 15px;">
                        <label class="label-input col-md-3 col-sm-3 col-xs-12">Tipo de Tarefa:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="tipotarefaField">
                        </div> 
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12"  id="prioridadeInfo" style="padding-top: 15px;">
                        <label class="label-input col-md-3 col-sm-3 col-xs-12">Prioridade:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="prioridadeField">
                        </div> 
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12"  id="dtinicioInfo" style="padding-top: 15px;">
                        <label class="label-input col-md-3 col-sm-3 col-xs-12">Data da Tarefa:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="dtinicioField">
                        </div> 
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12"  id="dtfinalInfo" style="padding-top: 15px;">
                        <label class="label-input col-md-3 col-sm-3 col-xs-12">Data Final:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="dtfinalField">
                        </div> 
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12"  id="descricaoInfo" style="padding-top: 15px;">
                        <label class="label-input col-md-3 col-sm-3 col-xs-12">Descrição:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="descricaoField">
                        </div> 
                    </div> 
                    <!--Listar todos os campos aqui--> 
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-danger btnRemove margin-zero">Remover</button>-->
                    <button type="button" class="btn btn-s btnView margin-zero" data-dismiss="modal">Ver Tudo</button>
                    <button type="button" class="btn btn-primary btnEdit margin-zero">Editar</button>
                    <button type="button" class="btn btn-default antoclose2 margin-zero" data-dismiss="modal">Fechar</button>

                </div>
            </div>
        </div>
    </div>
    <!-- /Modal de edit-->
    <?= $this->Form->input('urlroot', ["type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
</div>
<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_view" data-toggle="modal" data-target="#CalenderModalView"></div>


<?= $this->Html->script('/vendors/moment/min/moment.min.js'); ?>
<?= $this->Html->script('/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>
<?= $this->Html->script('/vendors/fullcalendar/dist/fullcalendar.min.js'); ?>
<?= $this->Html->script('/vendors/fullcalendar/dist/lang/pt-br.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<?= $this->Html->script('/js/date.js'); ?>
<?= $this->Html->script('/js/ajax/agenda.js'); ?>
<script>
    var keys_anexos = 0;
    $(document).ready(function () {
        $("#anexos").hide();
        $('#destinatarios').hide();
        $('#gruposervico-id').hide();
        $(":input").inputmask();
        $("#user-ids").select2({multiple: true, tokenSeparators: [',', ' '], tags: true, allowClear: true});
        $("select").select2({minimumResultsForSearch: 6});

        /*-------------------------EVENTOS---------------------------------*/
        $('.btn-anexo').click(function (e) {
            $('#destinatarios').hide();
            if ($('#anexos').css('display') == 'block') {
                $('#principal').slideDown();
            } else {
                if ($('#setor-id').val() == '' || $('#setor-id').val() == null) {
                    $('.document-details').hide();
                    $('.grupodocumento-id').hide();
                } else {
                    if ($('#grupodocumento-id').val() == '' || $('#grupodocumento-id').val() == null) {
                        $('.grupodocumento-id').show();
                        $('.document-details').hide();
                    } else {
                        $('.document-details').show();
                        $('.grupodocumento-id').show();
                    }
                }
                $('#principal').slideUp();
            }
            $('#anexos').slideToggle();

        });
        $('.btn-destinatarios').click(function (e) {
            $('#anexos').hide();
            if ($('#destinatarios').css('display') == 'block') {
                $('#principal').slideDown();
            } else {
                $('#principal').slideUp();
            }
            $('#destinatarios').slideToggle();
        });
        $('.btn-principal').click(function (e) {
            $('#anexos').slideUp();
            $('#destinatarios').slideUp();
            $('#principal').slideDown();
        });
        $('#fc_create').click(function (e) {
            $('#anexos').hide();
            $('#destinatarios').hide();
            $('#principal').slideDown();
        });
        $('.btn-cancel').click(function (e) {
            clearModalNew();
        });
        $('.addFile').click(function (e) {
            e.preventDefault();
            if (!$('#setor-id').val() || !$('#grupodocumento-id').val() || !$('#tipodocumento-id').val()) {
                alert('Os campos Setor/Grupo/Tipo de documentos devem ser preenchidos para poder adicionar o arquivo.');
            } else {
                addAttachment();
            }
        });
        $('.btn-attach-view').click(function (e) {
            $('.files').slideToggle();
            if ($('#action-all-attach').html() == 'Ver') {
                $('#action-all-attach').html('Ocultar');
            } else {
                $('#action-all-attach').html('Ver');
            }
        });
        $('select').on('select2:open', function (e) {
            $('.select2-search input').prop('focus', false);
        });
    });
    function clearModalNew() {
        var fields = ['hr-tarefa', 'tarefatipo-id', 'setor-id', 'grupodocumento-id', 'tipodocumento-id', 'tiposervico-id', 'empresa-id', 'funcionario-id', 'descricao', 'descricao-anexo', 'areaservico-id', 'user-ids', 'titulo', 'tarefaprioridade-id', 'dt-final', 'dt-lembrete'];
        var radios = ['permite_deletar', 'permite_editar', 'admin_empresa', 'admin_setor'];

        var selects = ['grupodocumento-id', 'tipodocumento-id', 'tiposervico-id', 'user-ids'];
        var selects2 = ['tarefaprioridade-id', 'tarefatipo-id', 'setor-id', 'grupodocumento-id', 'tipodocumento-id'];
<?php if ($admin) { ?>
            selects.push('empresa-id');
            selects.push('areaservico-id');
            selects.push('funcionario-id');
            selects.push('user-ids');
            selects2.push('empresa-id');
            selects2.push('areaservico-id');
            selects2.push('funcionario-id');
            selects2.push('user-ids');
<?php } ?>
        clearFieldsInfo(fields);
        clearCheckedRadios(radios);
        clearSelectOptions(selects);
        refreshSelect2(selects2);
        detachAllFiles();
    }
    function addAttachment() {
        new PNotify({
            text: 'Arquivo adicionado à lista de documentos',
            type: 'success',
            styling: 'bootstrap3',
            animate_speed: 'fast'
        });

        getPropertiesFile();
        additemListFiles();
        hideLatestAttachmentField();
        newAttachmentField();
    }
    function getPropertiesFile() {
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='tipo_documentos[" + keys_anexos + "]' value='" + $('#tipodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='grupo_documentos[" + keys_anexos + "]' value='" + $('#grupodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='area_servicos[" + keys_anexos + "]' value='" + $('#setor-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='funcionario[" + keys_anexos + "]' value='" + $('#funcionario-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='descricoes[" + keys_anexos + "]' value='" + $('#descricao-anexo').val() + "'>");
    }
    function additemListFiles() {
        $('#queue').append('<div class="itemqueue col-md-4 col-sm-4 col-xs-12 well well-sm"  style="padding: 0;" item="' + keys_anexos + '">\
                            <div id="descFile" class="col-md-9 col-sm-9 col-xs-9">' +
                $('#setor-id option:selected').text() + '/ ' + $('#grupodocumento-id option:selected').text() + '/ ' + $('#tipodocumento-id option:selected').text()
                + '</div>\
                <div class="removeFile col-md-3 col-sm-3 col-xs-3">\
                    <button class="remove btn btn-sm btn-danger" type="button" onclick="removeItemListFiles(this,' + keys_anexos + ');"><i class="fa fa-trash"></i></button>\
                </div></div>');
    }
    function hideLatestAttachmentField() {
        $('.file').hide();
    }
    function showLatestAttachmentField() {
        $('.file').show();
    }
    function newAttachmentField() {
        keys_anexos++;
        $('#pathFile').append("<input type='file' class='file' key='" + keys_anexos + "' name='file[]'>");
    }
    function removeItemListFiles(item, key) {
        $(item).parent().parent().remove();
        $(':input[key=' + key + ']').remove();
    }
    function detachAllFiles() {
        //remove a lista de arquivos
        $('.itemqueue').remove();
        //remove todos os campos vinculados aos arquivos
        for (var i = keys_anexos; i > 0; i--) {
            $(':input[key=' + i + ']').remove();
        }
        //limpa o campo do select do arquivo principal
        $(":input[key='0']").val('');
        //exibe para poder selecionar o arquivo
        showLatestAttachmentField();

    }


</script>