
<div class="page-title">
    <div class="title_left">
        <h3><?= __('Cadastrar Tarefa') ?></h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Insira as informações para <?= __('cadastrar Tarefa') ?>. <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tarefa, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                                
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_id">empresa_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('empresa_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $empresas]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="areaservico_id">areaservico_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tarefatipo_id">tarefatipo_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('tarefatipo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefatipos]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiposervico_id">tiposervico_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tiposervicos, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tarefaprioridade_id">tarefaprioridade_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('tarefaprioridade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefaprioridades, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">titulo <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('titulo', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">descricao <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_inicio">dt_inicio <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('dt_inicio', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_final">dt_final <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('dt_final', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_lembrete">dt_lembrete <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('dt_lembrete', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_concluida">dt_concluida <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('dt_concluida', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_adiada">dt_adiada <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('dt_adiada', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="automatica">automatica <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('automatica', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_empresa">admin_empresa <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('admin_empresa', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_setor">admin_setor <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('admin_setor', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="permite_deletar">permite_deletar <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('permite_deletar', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">user_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('user_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $users, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                           
                                    
                                           
                    
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>