<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Subtipodocumento'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipodocumentos'), ['controller' => 'Tipodocumentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipodocumento'), ['controller' => 'Tipodocumentos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subtipodocumentos index large-9 medium-8 columns content">
    <h3><?= __('Subtipodocumentos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipodocumento_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descricao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subtipodocumentos as $subtipodocumento): ?>
            <tr>
                <td><?= $this->Number->format($subtipodocumento->id) ?></td>
                <td><?= $subtipodocumento->has('tipodocumento') ? $this->Html->link($subtipodocumento->tipodocumento->descricao, ['controller' => 'Tipodocumentos', 'action' => 'view', $subtipodocumento->tipodocumento->id]) : '' ?></td>
                <td><?= h($subtipodocumento->descricao) ?></td>
                <td><?= h($subtipodocumento->status) ?></td>
                <td><?= h($subtipodocumento->dt_cadastro) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $subtipodocumento->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $subtipodocumento->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $subtipodocumento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subtipodocumento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
