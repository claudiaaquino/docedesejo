<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= __('Submenus') ?> - <?= h($submenu->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Submenu') ?></p>
                        <p><?= $submenu->nome ? $submenu->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $submenu->descricao ? $submenu->descricao : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Controller') ?></p>
                        <p><?= $submenu->controller ? $submenu->controller : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Action') ?></p>
                        <p><?= $submenu->action ? $submenu->action : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $submenu->last_update ? $submenu->last_update : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $submenu->status ? __('Ativo') : __('Desativado'); ?></p>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                                <?= $this->Html->link("Editar", ['action' => 'edit', $submenu->id], ['class' => "btn btn-primary"]) ?>
                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $submenu->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $submenu->id)]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($submenu->menusubmenus)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Menus Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Menu') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Vínculo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($submenu->menusubmenus as $menusubmenus):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $this->Html->link($menusubmenus->menu->descricao, ['controller' => 'Menus', 'action' => 'view', $menusubmenus->menu_id]) ?></td>
                                        <td><?= h($menusubmenus->dt_cadastro) ?></td>
                                        <td><?= $menusubmenus->status ? 'Ativo' : 'Desativado' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Menu'), ['controller' => 'Menus', 'action' => 'view', $menusubmenus->menu_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvíncular desse Menu'), ['controller' => 'Menusubmenus', 'action' => 'delete', $menusubmenus->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $menusubmenus->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


