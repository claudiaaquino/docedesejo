<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= $movimentacaobancaria->movimentacaotipo->descricao . ' em ' . $movimentacaobancaria->dt_cadastro ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($admin) { ?>
                            <p class="title"><?= __('Empresa') ?></p>
                            <p><?= $movimentacaobancaria->has('empresa') ? $this->Html->link($movimentacaobancaria->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $movimentacaobancaria->empresa->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Num de Nota Fiscal') ?></p>
                        <p><?= $movimentacaobancaria->num_notafiscal ? $movimentacaobancaria->num_notafiscal : 'Não Informado'; ?></p>

                        <?php if ($movimentacaobancaria->num_recibo) { ?>
                            <p class="title"><?= __('Num do Recibo') ?></p>
                            <p><?= $movimentacaobancaria->num_recibo ? $movimentacaobancaria->num_recibo : 'Não Informado'; ?></p>
                        <?php } ?>


                        <p class="title"><?= __('Tipo de Movimentação') ?></p>
                        <p><?= $movimentacaobancaria->has('movimentacaotipo') ? $this->Html->link($movimentacaobancaria->movimentacaotipo->descricao, ['controller' => 'Movimentacaotipos', 'action' => 'view', $movimentacaobancaria->movimentacaotipo->id]) : 'Não Informado' ?></p>

                        <?php if ($movimentacaobancaria->has('tiporeceita')) { ?>
                            <p class="title"><?= __('Forma de Receita') ?></p>
                            <p><?= $movimentacaobancaria->has('tiporeceita') ? $this->Html->link($movimentacaobancaria->tiporeceita->descricao, ['controller' => 'Tiporeceitas', 'action' => 'view', $movimentacaobancaria->tiporeceita->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <?php if ($movimentacaobancaria->has('tipodespesa')) { ?>
                            <p class="title"><?= __('Tipo de Despesa') ?></p>
                            <p><?= $movimentacaobancaria->has('tipodespesa') ? $this->Html->link($movimentacaobancaria->tipodespesa->descricao, ['controller' => 'Tipodespesas', 'action' => 'view', $movimentacaobancaria->tipodespesa->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <?php if ($movimentacaobancaria->has('fornecedore')) { ?>
                            <p class="title"><?= __('Fornecedor') ?></p>
                            <p><?= $movimentacaobancaria->has('fornecedore') ? $this->Html->link($movimentacaobancaria->fornecedore->nome, ['controller' => 'Fornecedores', 'action' => 'view', $movimentacaobancaria->fornecedore->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <?php if ($movimentacaobancaria->has('cliente')) { ?>
                            <p class="title"><?= __('Cliente') ?></p>
                            <p><?= $movimentacaobancaria->has('cliente') ? $this->Html->link($movimentacaobancaria->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $movimentacaobancaria->cliente->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Forma de Pagamento') ?></p>
                        <p><?= $movimentacaobancaria->has('formaspagamento') ? $this->Html->link($movimentacaobancaria->formaspagamento->descricao, ['controller' => 'Formaspagamentos', 'action' => 'view', $movimentacaobancaria->formaspagamento->id]) . ' ' . ( $movimentacaobancaria->avista == 1 ? __('À vista') : __('Parcelado')) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Conta Bancária') ?></p>
                        <p><?= $movimentacaobancaria->has('contabancaria') ? $this->Html->link($movimentacaobancaria->contabancaria->banco->nome . ( $movimentacaobancaria->contabancaria->conta ? ' (' . $movimentacaobancaria->contabancaria->conta . '-' . $movimentacaobancaria->contabancaria->co_digito . ')' : ''), ['controller' => 'Contabancarias', 'action' => 'view', $movimentacaobancaria->contabancaria->id]) : 'Não Informado' ?></p>



                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Valor Total da Movimentação') ?></p>
                        <p><?= $movimentacaobancaria->valor ? $this->Number->format($movimentacaobancaria->valor) : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Saldo da conta Posterior à movimentação') ?></p>
                        <p><?= $movimentacaobancaria->saldo_posterior ? $this->Number->format($movimentacaobancaria->saldo_posterior) : 'Não Informado'; ?></p>

                        <?php if ($movimentacaobancaria->num_parcelas > 0) { ?>

                            <p class="title"><?= __('Num de Parcelas') ?></p>
                            <p><?= $movimentacaobancaria->num_parcelas ? $this->Number->format($movimentacaobancaria->num_parcelas) : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Valor das Parcelas') ?></p>
                            <p><?= $movimentacaobancaria->valorparcela ? $this->Number->format($movimentacaobancaria->valorparcela) : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Dia do Mês Programado para Pagamento das Parcelas') ?></p>
                            <p><?= $movimentacaobancaria->diaprogramado ? $this->Number->format($movimentacaobancaria->diaprogramado) : 'Não Informado'; ?></p>

                        <?php } ?>

                        <?php if ($movimentacaobancaria->dt_pagamentoavista) { ?>
                            <p class="title"><?= __('Dt Pagamento à Vista') ?></p>
                            <p><?= $movimentacaobancaria->dt_pagamentoavista ? $movimentacaobancaria->dt_pagamentoavista : 'Não Informado'; ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Status do Pagamento') ?></p>
                        <p><?= $movimentacaobancaria->pago ? __('PAGO') : __('PENDENTE'); ?></p>


                        <p class="title"><?= __('Dt Lançamento no Sistema') ?></p>
                        <p><?= $movimentacaobancaria->dt_cadastro ? $movimentacaobancaria->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Última Atualização') ?></p>
                        <p><?= $movimentacaobancaria->last_update ? $movimentacaobancaria->last_update : 'Não Informado'; ?></p>



                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?php if ($movimentacaobancaria->has('documento')) { ?>
                            <?= $this->Html->link("Baixar Documento", "/docs/" . $movimentacaobancaria->documento->file, array('target' => '_blank', 'class' => "btn btn-sm btn-primary")); ?>

                        <?php } ?>
                        <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($movimentacaobancaria->movimentacoesfuturas)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Parcelas Programadas Referente à esse Pagamento</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= $this->Flash->render() ?>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Dt Programada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor da Parcela') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Pago') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ult. Atualização') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($movimentacaobancaria->movimentacoesfuturas as $movimentacoesfuturas):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($movimentacoesfuturas->id) ?>">
                                        </td>
                                        <td><?= h($movimentacoesfuturas->dt_programada) ?></td>
                                        <td>R$ <?= h($movimentacoesfuturas->valorparcela) ?></td>
                                        <td><?= $movimentacoesfuturas->pago ? 'PAGO' : 'PENDENTE' ?></td>
                                        <td><?= h($movimentacoesfuturas->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Movimentacoesfuturas', 'action' => 'edit', $movimentacoesfuturas->id], [ 'class' => "btn btn-info btn-xs"]) ?>                                               
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


