<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Movimentação de Caixa</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro') ?></th>                               
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('movimentacaotipo_id', 'Tipo') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('fornecedor_id', 'Fornecedor') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('cliente_id', 'Cliente') ?></th>
                                <th scope="col" class="column-title"><?= 'Forma de Pagamento' ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('valor', 'Valor Mov.') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('saldo_posterior', 'Saldo Posterior') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('pago', 'Status Pag') ?></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($movimentacaobancarias as $movimentacaobancaria) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $movimentacaobancaria->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $movimentacaobancaria->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $movimentacaobancaria->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $movimentacaobancaria->id)]) ?>
                                        </div>
                                    </td>

                                    <td><?= h($movimentacaobancaria->dt_cadastro) ?></td>
                                    <td><?= $movimentacaobancaria->has('movimentacaotipo') ? $this->Html->link($movimentacaobancaria->movimentacaotipo->descricao, ['controller' => 'Movimentacaotipos', 'action' => 'view', $movimentacaobancaria->movimentacaotipo->id]) : '' ?></td>
                                    <td><?= $movimentacaobancaria->has('fornecedore') ? $this->Html->link($movimentacaobancaria->fornecedore->nome, ['controller' => 'Fornecedores', 'action' => 'view', $movimentacaobancaria->fornecedore->id]) : '--' ?></td>
                                    <td><?= $movimentacaobancaria->has('cliente') ? $this->Html->link($movimentacaobancaria->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $movimentacaobancaria->cliente->id]) : '--' ?></td>
                                    <td><?= $movimentacaobancaria->has('formaspagamento') ? $movimentacaobancaria->formaspagamento->descricao . ( $movimentacaobancaria->avista ? ' à vista' : ' parcelado') : '' ?></td>
                                    <td>R$ <?= $this->Number->format($movimentacaobancaria->valor) ?></td>
                                    <td>R$ <?= $this->Number->format($movimentacaobancaria->saldo_posterior) ?></td>
                                    <td><?= $movimentacaobancaria->pago ? 'PAGO' : 'PENDENTE' ?></td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
