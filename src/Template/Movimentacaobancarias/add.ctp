<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Movimentação de Caixa') ?> <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($movimentacaobancaria, ['enctype' => 'multipart/form-data', "class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="movimentacaotipo_id">Tipo de Movimentação <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('movimentacaotipo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $movimentacaotipos, 'empty' => 'selecione o tipo de movimentação', "required" => "required"]); ?>
                    </div> 
                </div> 

                <div class="form-group fornecedor">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fornecedor_id">Fornecedor <span class="required">*</span>
                        <?= $this->Html->link('', ['controller' => 'Fornecedores', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('fornecedor_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $fornecedores, 'empty' => 'selecione um fornecedor']); ?>

                    </div> 
                </div>                         
                <div class="form-group fornecedor">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fornecedor_id">Forma de Despesa <span class="required">*</span>
                        <?= $this->Html->link('', ['controller' => 'Tipodespesas', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tipodespesa_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tipodespesas, 'empty' => 'selecione um fornecedor']); ?>

                    </div> 
                </div>                         

                <div class="form-group cliente">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cliente_id">Cliente <span class="required">*</span>
                        <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('cliente_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $clientes, 'empty' => 'selecione um cliente']); ?>
                    </div> 
                </div> 
                <div class="form-group cliente">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cliente_id">Forma de Receita <span class="required">*</span>
                        <?= $this->Html->link('', ['controller' => 'Tiporeceitas', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tiporeceita_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tiporeceitas, 'empty' => 'selecione um cliente']); ?>
                    </div> 
                </div> 
                <div class="ln_solid"></div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="formaspagamento_id">Forma de Pagamento <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('formaspagamento_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $formaspagamentos, 'empty' => 'selecione a forma de pagamento', "required" => "required"]); ?>
                    </div> 
                </div> 

                <div class="form-group contabancaria">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contabancaria_id">Conta Bancária <span class="required">*</span>
                        <?= $this->Html->link('', ['controller' => 'Contabancarias', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('contabancaria_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $contabancarias, 'empty' => 'selecione a conta bancária', "required" => "required"]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-1">
                    </div> 
                    <div class="col-md-4 col-sm-6 col-xs-11">
                        <?=
                        $this->Form->radio('avista', [
                            ['value' => '1', 'text' => 'À vista', "class" => "form-control col-md-7 col-xs-12"],
                            ['value' => '0', 'text' => 'Parcelado', "class" => "form-control col-md-7 col-xs-12"]
                                ]
                        );
                        ?>
                    </div> 
                </div>      


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valor">Valor Total da Movimentação (R$) <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('valor', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>    

                <div class="form-group parcelas">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valor">Nº de Parcelas
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('num_parcelas', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>     
                <div class="form-group parcelas">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valor">Valor das Parcelas (R$) 
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('valorparcela', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>     

                <div class="form-group parcelas">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valor">Dia do mês para contabilização
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('diaprogramado', ["type" => 'number', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>          


                <div class="form-group parcelas">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="primeiraparcelapaga">A 1ª parcela já foi paga?
                    </label>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                        <?= $this->Form->input('primeiraparcelapaga', ["type" => 'checkbox', "class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>       
                <div class="form-group valortotal">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pago">A valor total ja foi pago?
                    </label>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                        <?= $this->Form->input('pago', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>       
                <div class="ln_solid"></div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="num_notafiscal">Num. da Nota Fiscal
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('num_notafiscal', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="num_recibo">Num do Recibo 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('num_recibo', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>      
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="arquivopath">Documento da Nota Fiscal/Recibo
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->file('file', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]) ?>
                    </div>
                </div> 
                <div class="ln_solid"></div> 
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success btn-submit">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>

<script>
    function calculaValorParcela() {
        if ($('#valor').val() != '' && $('#num-parcelas').val() != '') {
            var valor = $('#valor').val() / $('#num-parcelas').val();
            $('#valorparcela').val(valor);
        }
    }
    function atualizaSelects() {
//        $("#fornecedor-id[data-select2-tag='true']").prop("selected", false);
//        $("#cliente-id[data-select2-tag='true']").prop("selected", false);
//        $("#tipodespesa-id[data-select2-tag='true']").prop("selected", false);
//        $("#tiporeceita-id[data-select2-tag='true']").prop("selected", false);
    }
    $(document).ready(function () {
        $("select").select2();
        $("#fornecedor-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo'});
        $("#tipodespesa-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo'});
        $("#cliente-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo'});
        $("#tiporeceita-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo'});
        $('.parcelas').hide();
        $('.cliente').hide();
        $('.fornecedor').hide();

        $('#movimentacaotipo-id').change(function () {
            if ($('#movimentacaotipo-id').val() == 1) {//despesa
                $('.cliente').hide();
                $('.fornecedor').show();

                $('#cliente-id').val('');
                $('#tiporeceita-id').val('');
            } else if ($('#movimentacaotipo-id').val() == 2) {//receita
                $('.cliente').show();
                $('.fornecedor').hide();

                $('#fornecedor-id').val('');
                $('#tipodespesa-id').val('');
            } else {
                $('.cliente').hide();
                $('.fornecedor').hide();

                $('#fornecedor-id').val('');
                $('#tipodespesa-id').val('');

                $('#cliente-id').val('');
                $('#tiporeceita-id').val('');
            }
        });


        $('#valorparcela').focusin(function () {
            calculaValorParcela();
        });
        $('#valor').focusout(function () {
            calculaValorParcela();
        });
        $('#num_parcelas').focusout(function () {
            calculaValorParcela();
        });


        $('input[name="avista"]:radio').change(function () {
            if ($('input[name="avista"]:radio:checked').val() == '0') {//parcelado
                $('.parcelas').show();
                $('.valortotal').hide();
                $("#pago").prop("checked", false);
            } else {
                $('#num-parcelas').val('');
                $('#valorparcela').val('');
                $('#diaprogramado').val('');
                $('#primeiraparcelapaga').val('');
                $('.parcelas').hide();
                $('.valortotal').show();
            }
        });

        $('.btn-submit').click(function () {
            atualizaSelects();
            return true;
        });

    });
</script>