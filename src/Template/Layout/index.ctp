<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Doce Desejo</title>

        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">


        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/vendors/iCheck/skins/flat/green.css') ?>        
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) //necessário para carregas os outros js dentro dos content's ?>
        <?= $this->Html->script('/js/ajax/index.js', array('inline' => false)); ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?> 


        <style type="text/css">
            body a:hover {
                color: #194f8c;
            }
        </style>

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <!--                        <div class="navbar nav_title" style="border: 0;">
                                                    <span class="site_title">
                        
                                                        DOCE DESEJO
                                                    </span>
                                                </div>-->

                        <div class="clearfix"></div>
                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Menu Geral</h3>
                                <?php if ($loggedIn) { ?>
                                    <ul class="nav side-menu">
                                        <?php foreach ($menus as $menu) { ?>
                                            <li><a><i class="fa fa-<?= $menu->iconname; ?>"></i> <?= $menu->descricao; ?> <span class="fa fa-chevron-down"></span></a>
                                                <!--http://fontawesome.io/icons/-->
                                                <ul class="nav child_menu">
                                                    <?php
                                                    foreach ($menu->menusubmenus as $submenu) {
                                                        if ($submenu->submenu->controller) {
                                                            ?>
                                                            <li><?= $this->Html->link(__($submenu->submenu->nome), ['controller' => ucfirst($submenu->submenu->controller), 'action' => $submenu->submenu->action]); ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- /sidebar menu -->

                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <?= $loggedfirstname ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li> <?= $this->Html->link('Sair', ['controller' => 'users', 'action' => 'logout']) ?></li>
                                    </ul>
                                </li>
                                <li id="box-financeiro" role="presentation" class="dropdown">
                                    <a href="/movimentacaobancarias/add/">
                                        <i class="fa fa-money"></i>
                                    </a>
                                </li>
                                <li id="box-tarefas" role="presentation" class="dropdown"></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    <?= $this->fetch('content') ?>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Doce Desejo 
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div> 

        <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>

        <?= $this->Html->script('/vendors/fastclick/lib/fastclick.js') ?>
        <?= $this->Html->script('/vendors/iCheck/icheck.min.js') ?>

        <?= $this->Html->script('custom.min.js') ?>

        <?= $this->fetch('script') ?>
    </body>
</html>