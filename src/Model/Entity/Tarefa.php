<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tarefa Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $areaservico_id
 * @property int $tarefatipo_id
 * @property int $tiposervico_id
 * @property int $tarefaprioridade_id
 * @property string $titulo
 * @property string $descricao
 * @property string $dt_inicio
 * @property string $dt_final
 * @property string $dt_lembrete
 * @property \Cake\I18n\Time $dt_concluida
 * @property \Cake\I18n\Time $dt_adiada
 * @property bool $automatica
 * @property bool $admin_empresa
 * @property bool $admin_setor
 * @property bool $permite_deletar
 * @property bool $permite_editar
 * @property int $empresa_cadastrou
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 *
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Areaservico $areaservico
 * @property \App\Model\Entity\Tarefatipo $tarefatipo
 * @property \App\Model\Entity\Tiposervico $tiposervico
 * @property \App\Model\Entity\Tarefaprioridade $tarefaprioridade
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tarefadocumento[] $tarefadocumentos
 * @property \App\Model\Entity\Tarefausuario[] $tarefausuarios
 */
class Tarefa extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
