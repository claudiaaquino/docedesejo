<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Areaservico Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property string $descricao
 * @property bool $todos
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Grupodocumento[] $grupodocumentos
 * @property \App\Model\Entity\Gruposervico[] $gruposervicos
 * @property \App\Model\Entity\Mensagen[] $mensagens
 * @property \App\Model\Entity\Tarefa[] $tarefas
 * @property \App\Model\Entity\Usersareaservico[] $usersareaservicos
 */
class Areaservico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
