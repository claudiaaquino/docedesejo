<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tiposervico Entity
 *
 * @property int $id
 * @property int $gruposervico_id
 * @property string $nome
 * @property string $descricao
 * @property int $user_id
 * @property \Cake\I18n\Time $last_update
 * @property \Cake\I18n\Time $dt_cadastro
 * @property bool $status
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Gruposervico $gruposervico
 * @property \App\Model\Entity\Previsaoorcamento[] $previsaoorcamentos
 */
class Tiposervico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
