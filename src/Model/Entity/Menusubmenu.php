<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Menusubmenu Entity
 *
 * @property int $id
 * @property int $menu_id
 * @property int $submenu_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property bool $status
 *
 * @property \App\Model\Entity\Menu $menu
 * @property \App\Model\Entity\Submenu $submenu
 */
class Menusubmenu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
