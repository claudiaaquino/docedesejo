<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Movimentacaobancaria Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $movimentacaotipo_id
 * @property string $fornecedor_id
 * @property string $cliente_id
 * @property int $contabancaria_id
 * @property int $formaspagamento_id
 * @property string $tiporeceita_id
 * @property string $tipodespesa_id
 * @property int $documento_id
 * @property float $saldo_anterior
 * @property float $saldo_posterior
 * @property string $num_notafiscal
 * @property string $num_recibo
 * @property float $valor
 * @property bool $avista
 * @property bool $pago
 * @property \Cake\I18n\Time $dt_pagamentoavista
 * @property int $num_parcelas
 * @property float $valorparcela
 * @property int $diaprogramado
 * @property bool $primeiraparcelapaga
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Movimentacaotipo $movimentacaotipo
 * @property \App\Model\Entity\Fornecedor $fornecedor
 * @property \App\Model\Entity\Cliente $cliente
 * @property \App\Model\Entity\Contabancaria $contabancaria
 * @property \App\Model\Entity\Formaspagamento $formaspagamento
 * @property \App\Model\Entity\Tiporeceita $tiporeceita
 * @property \App\Model\Entity\Tipodespesa $tipodespesa
 * @property \App\Model\Entity\Documento $documento
 * @property \App\Model\Entity\Movimentacoesfutura[] $movimentacoesfuturas
 */
class Movimentacaobancaria extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
