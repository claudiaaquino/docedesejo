<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contabancaria Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $cliente_id
 * @property int $fornecedor_id
 * @property int $user_id
 * @property int $banco_id
 * @property int $operacao
 * @property float $valorcaixa
 * @property int $agencia
 * @property int $ag_digito
 * @property string $conta
 * @property string $co_digito
 * @property string $dt_vencimento
 * @property string $cpf
 * @property string $nome
 * @property string $endereco
 * @property string $end_numero
 * @property string $end_complemento
 * @property string $end_bairro
 * @property string $end_cep
 * @property int $estado_id
 * @property int $cidade_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Banco $banco
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Cidade $cidade
 * @property \App\Model\Entity\Boleto[] $boletos
 */
class Contabancaria extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
