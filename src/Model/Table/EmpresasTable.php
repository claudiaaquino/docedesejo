<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Empresas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Cidades
 * @property \Cake\ORM\Association\HasMany $Users
 * @property \Cake\ORM\Association\HasMany $Documentos
 * @property \Cake\ORM\Association\HasMany $Areaservicos
 *
 * @method \App\Model\Entity\Empresa get($primaryKey, $options = [])
 * @method \App\Model\Entity\Empresa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Empresa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Empresa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Empresa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Empresa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Empresa findOrCreate($search, callable $callback = null)
 */
class EmpresasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('empresas');
        $this->displayField('razao');
        $this->primaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->hasMany('Documentos', [
            'foreignKey' => 'empresa_id'
        ]);
      
        $this->hasMany('Areaservicos', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'empresa_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');


//        $validator->requirePresence('razao', 'create')
//                ->notEmpty('razao');
//        $validator->requirePresence('cnpj', 'create')
//                ->notEmpty('cnpj');
        $validator->allowEmpty('cnpj');

        $validator
                ->email('email')->allowEmpty('email');
//                ->requirePresence('email', 'create')
//                ->notEmpty('email', 'O email para contato deve ser informado');

        $validator
                ->integer('num_funcionarios')
                ->allowEmpty('num_funcionarios');
        $validator
                ->integer('num_socios')
                ->allowEmpty('num_socios');

        $validator
                ->numeric('faturamento_mensal')
                ->allowEmpty('faturamento_mensal');

        $validator
                ->numeric('faturamento_anual')
                ->allowEmpty('faturamento_anual');

        $validator
                ->numeric('capitalsocial')
                ->allowEmpty('capitalsocial');

        $validator
                ->date('dt_solicitacao')
                ->allowEmpty('dt_solicitacao');


//        $validator
//                ->allowEmpty('last_fields_updated');
//
//        $validator
//                ->dateTime('last_update')
//                ->allowEmpty('last_update');
//
//        $validator
//                ->integer('status')
//                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['CNPJ'], 'Esse CPNJ já está cadastrado para outra Empresa.'));
//        $rules->add($rules->existsIn(['cnae_id'], 'Cnaes'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));

        return $rules;
    }

}
