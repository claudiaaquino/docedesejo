<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Formaspagamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Movimentacaobancarias
 *
 * @method \App\Model\Entity\Formaspagamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Formaspagamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Formaspagamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Formaspagamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Formaspagamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Formaspagamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Formaspagamento findOrCreate($search, callable $callback = null)
 */
class FormaspagamentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('formaspagamentos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Movimentacaobancarias', [
            'foreignKey' => 'formaspagamento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
