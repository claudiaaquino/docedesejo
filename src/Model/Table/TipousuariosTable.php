<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipousuarios Model
 *
 * @property \Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Tipousuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipousuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipousuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipousuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipousuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipousuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipousuario findOrCreate($search, callable $callback = null)
 */
class TipousuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipousuarios');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Users', [
            'foreignKey' => 'tipousuario_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
