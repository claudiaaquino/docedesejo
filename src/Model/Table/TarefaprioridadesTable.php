<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tarefaprioridades Model
 *
 * @property \Cake\ORM\Association\HasMany $Tarefas
 *
 * @method \App\Model\Entity\Tarefaprioridade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tarefaprioridade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tarefaprioridade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tarefaprioridade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tarefaprioridade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefaprioridade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefaprioridade findOrCreate($search, callable $callback = null)
 */
class TarefaprioridadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tarefaprioridades');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Tarefas', [
            'foreignKey' => 'tarefaprioridade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->integer('ordem')
            ->requirePresence('ordem', 'create')
            ->notEmpty('ordem');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }
}
