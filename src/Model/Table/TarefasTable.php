<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tarefas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Areaservicos
 * @property \Cake\ORM\Association\BelongsTo $Tarefatipos
 * @property \Cake\ORM\Association\BelongsTo $Tiposervicos
 * @property \Cake\ORM\Association\BelongsTo $Tarefaprioridades
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Tarefadocumentos
 * @property \Cake\ORM\Association\HasMany $Tarefausuarios
 *
 * @method \App\Model\Entity\Tarefa get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tarefa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tarefa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tarefa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tarefa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefa findOrCreate($search, callable $callback = null)
 */
class TarefasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('tarefas');
        $this->displayField('titulo');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Areaservicos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->belongsTo('Tarefatipos', [
            'foreignKey' => 'tarefatipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tiposervicos', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->belongsTo('Tarefaprioridades', [
            'foreignKey' => 'tarefaprioridade_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Tarefadocumentos', [
            'foreignKey' => 'tarefa_id'
        ]);
        $this->hasMany('Tarefausuarios', [
            'foreignKey' => 'tarefa_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('titulo', 'create')
                ->notEmpty('titulo');

        $validator
                ->allowEmpty('descricao');

//            ->dateTime('dt_inicio')
        $validator
                ->allowEmpty('dt_inicio');

//            ->dateTime('dt_final')
        $validator
                ->allowEmpty('dt_final');

//            ->dateTime('dt_lembrete')
        $validator
                ->allowEmpty('dt_lembrete');

        $validator
                ->dateTime('dt_concluida')
                ->allowEmpty('dt_concluida');

        $validator
                ->dateTime('dt_adiada')
                ->allowEmpty('dt_adiada');

        $validator
                ->boolean('automatica')
                ->allowEmpty('automatica');

        $validator
                ->boolean('admin_empresa')
                ->allowEmpty('admin_empresa');

        $validator
                ->boolean('admin_setor')
                ->allowEmpty('admin_setor');

        $validator
                ->boolean('permite_deletar')
                ->allowEmpty('permite_deletar');

        $validator
                ->dateTime('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['areaservico_id'], 'Areaservicos'));
        $rules->add($rules->existsIn(['tarefatipo_id'], 'Tarefatipos'));
        $rules->add($rules->existsIn(['tiposervico_id'], 'Tiposervicos'));
        $rules->add($rules->existsIn(['tarefaprioridade_id'], 'Tarefaprioridades'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

}
