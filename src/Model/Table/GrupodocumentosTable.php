<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Grupodocumentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Areaservicos
 * @property \Cake\ORM\Association\HasMany $Tipodocumentos
 *
 * @method \App\Model\Entity\Grupodocumento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Grupodocumento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Grupodocumento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Grupodocumento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Grupodocumento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Grupodocumento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Grupodocumento findOrCreate($search, callable $callback = null)
 */
class GrupodocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('grupodocumentos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Areaservicos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->hasMany('Tipodocumentos', [
            'foreignKey' => 'grupodocumento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['areaservico_id'], 'Areaservicos'));

        return $rules;
    }
}
