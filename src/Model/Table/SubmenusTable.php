<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Submenus Model
 *
 * @property \Cake\ORM\Association\HasMany $Menusubmenus
 *
 * @method \App\Model\Entity\Submenu get($primaryKey, $options = [])
 * @method \App\Model\Entity\Submenu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Submenu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Submenu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Submenu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Submenu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Submenu findOrCreate($search, callable $callback = null)
 */
class SubmenusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('submenus');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Menusubmenus', [
            'foreignKey' => 'submenu_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->allowEmpty('controller');

        $validator
            ->allowEmpty('action');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        return $validator;
    }
}
