<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contabancarias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $Fornecedores
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Bancos
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Cidades
 * @property \Cake\ORM\Association\HasMany $Movimentacaobancarias
 *
 * @method \App\Model\Entity\Contabancaria get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contabancaria newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contabancaria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contabancaria|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contabancaria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contabancaria[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contabancaria findOrCreate($search, callable $callback = null)
 */
class ContabancariasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('contabancarias');
        $this->displayField('conta');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsTo('Fornecedores', [
            'foreignKey' => 'fornecedor_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Bancos', [
            'foreignKey' => 'banco_id'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->hasMany('Movimentacaobancarias', [
            'foreignKey' => 'contabancaria_id'
        ]);
    }

    public function findListDetails(\Cake\ORM\Query $query, array $options) {

        $query->find('list')->innerJoinWith('Bancos')
                ->where(["Contabancarias.status" => 1])
                ->order('Bancos.id');
        
        $query->select(['Contabancarias.id', 'Contabancarias__conta' => "(case when Contabancarias.conta = '' then Bancos.nome else 
  (CONCAT((Bancos.nome), ' - Conta  (', (Contabancarias.conta), '-', (case when Contabancarias.co_digito = '' then ' ' else Contabancarias.co_digito end), ')')) end)  "]);

        return $query;
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->integer('operacao')
                ->allowEmpty('operacao');

        $validator
                ->integer('agencia')
                ->allowEmpty('agencia');

        $validator
//                ->integer('ag_digito')
                ->allowEmpty('ag_digito');

        $validator
//                ->integer('conta')
                ->allowEmpty('conta');

        $validator
                ->integer('co_digito')
                ->allowEmpty('co_digito');

        $validator
                ->allowEmpty('dt_vencimento');

        $validator
                ->allowEmpty('cpf');

        $validator
                ->allowEmpty('nome');

        $validator
                ->allowEmpty('endereco');

        $validator
                ->allowEmpty('end_numero');

        $validator
                ->allowEmpty('end_complemento');

        $validator
                ->allowEmpty('end_bairro');

        $validator
                ->allowEmpty('end_cep');

        $validator
                ->date('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->boolean('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['fornecedor_id'], 'Fornecedores'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['banco_id'], 'Bancos'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));

        return $rules;
    }

}
