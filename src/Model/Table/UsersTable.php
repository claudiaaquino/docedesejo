<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tipousuarios
 * @property \Cake\ORM\Association\HasMany $Documentos
 * @property \Cake\ORM\Association\HasMany $Usermodulos
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\HasMany $Usersareaservicos
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Tipousuarios', [
            'foreignKey' => 'tipousuario_id'
        ]);
        $this->hasMany('Usermodulos', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->BelongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->hasMany('Documentos', [
            'foreignKey' => 'user_leu'
        ]);
        $this->hasMany('Documentos', [
            'foreignKey' => 'user_enviou'
        ]);

        $this->hasMany('Usersareaservicos', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    public function findAuth(\Cake\ORM\Query $query, array $options) {
        $query->select()
                ->contain(['Empresas', 'Tipousuarios', 'Usersareaservicos'])
                ->where(['Users.status' => 1]);

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('username', 'create')
                ->notEmpty('username');
        $validator
                ->requirePresence('password', 'create')
                ->notEmpty('password');

//        $validator
//            ->dateTime('last_login')
//            ->allowEmpty('last_login');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['username'], "Esse usuário já existe, você deve escolher outro."));
        $rules->add($rules->isUnique(['email'], "Esse email possui cadastro no sistema"));
        $rules->add($rules->existsIn(['tipousuario_id'], 'Tipousuarios'));

        return $rules;
    }

}
