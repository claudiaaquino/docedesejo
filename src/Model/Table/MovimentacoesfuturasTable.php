<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Movimentacoesfuturas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Movimentacaobancarias
 *
 * @method \App\Model\Entity\Movimentacoesfutura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Movimentacoesfutura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Movimentacoesfutura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacoesfutura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Movimentacoesfutura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacoesfutura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacoesfutura findOrCreate($search, callable $callback = null)
 */
class MovimentacoesfuturasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('movimentacoesfuturas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Movimentacaobancarias', [
            'foreignKey' => 'movimentacaobancaria_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('dt_programada');

        $validator
            ->numeric('valorparcela')
            ->allowEmpty('valorparcela');

        $validator
            ->boolean('pago')
            ->allowEmpty('pago');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['movimentacaobancaria_id'], 'Movimentacaobancarias'));

        return $rules;
    }
}
