<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Movimentacaobancarias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Movimentacaotipos
 * @property \Cake\ORM\Association\BelongsTo $Fornecedores
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $Contabancarias
 * @property \Cake\ORM\Association\BelongsTo $Formaspagamentos
 * @property \Cake\ORM\Association\BelongsTo $Tiporeceitas
 * @property \Cake\ORM\Association\BelongsTo $Tipodespesas
 * @property \Cake\ORM\Association\BelongsTo $Documentos
 * @property \Cake\ORM\Association\HasMany $Movimentacoesfuturas
 *
 * @method \App\Model\Entity\Movimentacaobancaria get($primaryKey, $options = [])
 * @method \App\Model\Entity\Movimentacaobancaria newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Movimentacaobancaria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacaobancaria|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Movimentacaobancaria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacaobancaria[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacaobancaria findOrCreate($search, callable $callback = null)
 */
class MovimentacaobancariasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('movimentacaobancarias');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Movimentacaotipos', [
            'foreignKey' => 'movimentacaotipo_id'
        ]);
        $this->belongsTo('Fornecedores', [
            'foreignKey' => 'fornecedor_id'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsTo('Contabancarias', [
            'foreignKey' => 'contabancaria_id'
        ]);
        $this->belongsTo('Formaspagamentos', [
            'foreignKey' => 'formaspagamento_id'
        ]);
        $this->belongsTo('Tiporeceitas', [
            'foreignKey' => 'tiporeceita_id'
        ]);
        $this->belongsTo('Tipodespesas', [
            'foreignKey' => 'tipodespesa_id'
        ]);
        $this->belongsTo('Documentos', [
            'foreignKey' => 'documento_id'
        ]);
        $this->hasMany('Movimentacoesfuturas', [
            'foreignKey' => 'movimentacaobancaria_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    

 public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->numeric('saldo_anterior')
                ->allowEmpty('saldo_anterior');

        $validator
                ->numeric('saldo_posterior')
                ->allowEmpty('saldo_posterior');

        $validator
                ->allowEmpty('num_notafiscal');

        $validator
                ->allowEmpty('num_recibo');

        $validator
                ->numeric('valor')
                ->nonNegativeInteger('valor')
                ->notEmpty('valor');

        $validator
                ->boolean('avista')
                ->allowEmpty('avista');

        $validator
                ->boolean('pago')
                ->allowEmpty('pago');

        $validator
                ->date('dt_pagamentoavista')
                ->allowEmpty('dt_pagamentoavista');

        $validator
                ->integer('num_parcelas')
                ->nonNegativeInteger('num_parcelas')
                ->allowEmpty('num_parcelas');

        $validator
                ->numeric('valorparcela')
                ->nonNegativeInteger('valorparcela')
                ->allowEmpty('valorparcela');

        $validator
                ->integer('diaprogramado')
                ->allowEmpty('diaprogramado');

        $validator
            ->boolean('primeiraparcelapaga')
            ->allowEmpty('primeiraparcelapaga');

        $validator
                ->dateTime('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->boolean('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['movimentacaotipo_id'], 'Movimentacaotipos'));
        $rules->add($rules->existsIn(['fornecedor_id'], 'Fornecedores'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['contabancaria_id'], 'Contabancarias'));
        $rules->add($rules->existsIn(['formaspagamento_id'], 'Formaspagamentos'));
        $rules->add($rules->existsIn(['tiporeceita_id'], 'Tiporeceitas'));
        $rules->add($rules->existsIn(['tipodespesa_id'], 'Tipodespesas'));
        $rules->add($rules->existsIn(['documento_id'], 'Documentos'));

        return $rules;
    }
}
