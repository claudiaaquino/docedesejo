<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tarefadocumentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tarefas
 * @property \Cake\ORM\Association\BelongsTo $Documentos
 *
 * @method \App\Model\Entity\Tarefadocumento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tarefadocumento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tarefadocumento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tarefadocumento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tarefadocumento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefadocumento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefadocumento findOrCreate($search, callable $callback = null)
 */
class TarefadocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tarefadocumentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Tarefas', [
            'foreignKey' => 'tarefa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Documentos', [
            'foreignKey' => 'documento_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('obs')
            ->allowEmpty('obs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tarefa_id'], 'Tarefas'));
        $rules->add($rules->existsIn(['documento_id'], 'Documentos'));

        return $rules;
    }
}
