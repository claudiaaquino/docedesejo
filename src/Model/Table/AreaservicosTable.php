<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Areaservicos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\HasMany $Grupodocumentos
 * @property \Cake\ORM\Association\HasMany $Gruposervicos
 * @property \Cake\ORM\Association\HasMany $Mensagens
 * @property \Cake\ORM\Association\HasMany $Tarefas
 * @property \Cake\ORM\Association\HasMany $Usersareaservicos
 *
 * @method \App\Model\Entity\Areaservico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Areaservico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Areaservico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Areaservico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Areaservico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Areaservico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Areaservico findOrCreate($search, callable $callback = null)
 */
class AreaservicosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('areaservicos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Grupodocumentos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->hasMany('Gruposervicos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->hasMany('Mensagens', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->hasMany('Tarefas', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->hasMany('Usersareaservicos', [
            'foreignKey' => 'areaservico_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->boolean('todos')
            ->allowEmpty('todos');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));

        return $rules;
    }
}
