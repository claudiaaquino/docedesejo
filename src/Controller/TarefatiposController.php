<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefatipos Controller
 *
 * @property \App\Model\Table\TarefatiposTable $Tarefatipos
 */
class TarefatiposController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $tarefatipos = $this->paginate($this->Tarefatipos->find()
                        ->where(['Tarefatipos.empresa_id' => $this->Auth->user('empresa_id'), 'Tarefatipos.todos_empresa' => '1'])
                        ->orWhere(['Tarefatipos.todos_sistema' => '1'])
                        ->orWhere(['Tarefatipos.user_id' => $this->Auth->user('id')]));

        $this->set(compact('tarefatipos'));
        $this->set('_serialize', ['tarefatipos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefatipo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tarefatipo = $this->Tarefatipos->get($id, [
            'contain' => ['Tarefas.Empresas', 'Tarefas.Areaservicos', 'Tarefas.Tiposervicos']
        ]);

        if (($tarefatipo->empresa_id != $this->Auth->user('empresa_id') && !$tarefatipo->todos_sistema) || ($tarefatipo->user_id != $this->Auth->user('id') && !$tarefatipo->todos_sistema) || (!$tarefatipo->empresa_id && !$tarefatipo->user_id && !$tarefatipo->todos_sistema && !$tarefatipo->todos_empresa)
        ) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect(['action' => 'index']);
        }


        $this->set('tarefatipo', $tarefatipo);
        $this->set('_serialize', ['tarefatipo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tarefatipo = $this->Tarefatipos->newEntity();
        if ($this->request->is('post')) {
            $tarefatipo = $this->Tarefatipos->patchEntity($tarefatipo, $this->request->data);
            $tarefatipo->dt_cadastro = date('Y-m-d H:i:s');

            if (($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) && ($tarefatipo->todos_sistema || $tarefatipo->todos_empresa)) {
                $tarefatipo->empresa_id = $this->Auth->user('empresa_id');
            } else {
                $tarefatipo->user_id = $this->Auth->user('id');
            }
            if ($this->Tarefatipos->save($tarefatipo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));
                if ($tarefatipo->backlink && $tarefatipo->backlink != '/') {
                    return $this->redirect($tarefatipo->backlink);
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tarefatipo'));
        $this->set('_serialize', ['tarefatipo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefatipo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tarefatipo = $this->Tarefatipos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefatipo = $this->Tarefatipos->patchEntity($tarefatipo, $this->request->data);

            if (($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) && (!$tarefatipo->todos_sistema && !$tarefatipo->todos_empresa)) {
                $tarefatipo->empresa_id = null;
                $tarefatipo->user_id = $this->Auth->user('id');
            } else if (($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) && ($tarefatipo->todos_sistema || $tarefatipo->todos_empresa)) {
                $tarefatipo->empresa_id = $this->Auth->user('empresa_id');
            }
            if ($this->Tarefatipos->save($tarefatipo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));
                if ($tarefatipo->backlink && $tarefatipo->backlink != '/') {
                    return $this->redirect($tarefatipo->backlink);
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tarefatipo'));
        $this->set('_serialize', ['tarefatipo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefatipo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tarefatipo = $this->Tarefatipos->get($id);
        if ($this->Tarefatipos->delete($tarefatipo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
