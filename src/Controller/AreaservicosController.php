<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Areaservicos Controller
 *
 * @property \App\Model\Table\AreaservicosTable $Areaservicos
 */
class AreaservicosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        if ($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) {
            $areaservicos = $this->paginate($this->Areaservicos->find()->where(['Areaservicos.empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['Areaservicos.todos' => '1']));
        }

        $this->set(compact('areaservicos'));
        $this->set('_serialize', ['areaservicos']);
    }

    /**
     * View method
     *
     * @param string|null $id Areaservico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $areaservico = $this->Areaservicos->get($id, [
            'contain' => ['Empresas', 'Grupodocumentos', 'Gruposervicos', 'Mensagens', 'Tarefas.Tarefatipos', 'Tarefas.Tarefaprioridades', 'Tarefas.Tiposervicos', 'Usersareaservicos']]
        );

        if ($areaservico->empresa_id && ($areaservico->empresa_id != $this->Auth->user('empresa_id') && !$areaservico->todos)) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect(['action' => 'index']);
        }

        $this->set('areaservico', $areaservico);
        $this->set('_serialize', ['areaservico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $areaservico = $this->Areaservicos->newEntity();
        if ($this->request->is('post')) {
            $areaservico = $this->Areaservicos->patchEntity($areaservico, $this->request->data);
            $areaservico->dt_cadastro = date('Y-m-d H:i:s');
            $areaservico->user_id = $this->Auth->user('id');
            $areaservico->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Areaservicos->save($areaservico)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('areaservico'));
        $this->set('_serialize', ['areaservico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Areaservico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $areaservico = $this->Areaservicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $areaservico = $this->Areaservicos->patchEntity($areaservico, $this->request->data);

            if ($areaservico->empresa_id != $this->Auth->user('empresa_id') || ($areaservico->empresa_id == $this->Auth->user('empresa_id') && !$this->Auth->user('admin_empresa')) || !$this->Auth->user('admin')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect(['action' => 'index']);
            }

            if ($this->Areaservicos->save($areaservico)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('areaservico'));
        $this->set('_serialize', ['areaservico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Areaservico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $areaservico = $this->Areaservicos->get($id);
        if ($areaservico->empresa_id == $this->Auth->user('empresa_id') || $this->Auth->user('admin')) {
            if ($this->Areaservicos->delete($areaservico)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

}
