<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Movimentacoesfuturas Controller
 *
 * @property \App\Model\Table\MovimentacoesfuturasTable $Movimentacoesfuturas
 */
class MovimentacoesfuturasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Movimentacaobancarias']
        ];
        $movimentacoesfuturas = $this->paginate($this->Movimentacoesfuturas);

        $this->set(compact('movimentacoesfuturas'));
        $this->set('_serialize', ['movimentacoesfuturas']);
    }

    /**
     * View method
     *
     * @param string|null $id Movimentacoesfutura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $movimentacoesfutura = $this->Movimentacoesfuturas->get($id, [
            'contain' => ['Movimentacaobancarias']
        ]);

        $this->set('movimentacoesfutura', $movimentacoesfutura);
        $this->set('_serialize', ['movimentacoesfutura']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $movimentacoesfutura = $this->Movimentacoesfuturas->newEntity();
        if ($this->request->is('post')) {
            $movimentacoesfutura = $this->Movimentacoesfuturas->patchEntity($movimentacoesfutura, $this->request->data);
            $movimentacoesfutura->dt_cadastro = date('Y-m-d H:i:s');
            $movimentacoesfutura->user_id = $this->Auth->user('id');
            $movimentacoesfutura->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Movimentacoesfuturas->save($movimentacoesfutura)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacoesfutura->movimentacaobancaria_id]);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $movimentacaobancarias = $this->Movimentacoesfuturas->Movimentacaobancarias->find('list', ['limit' => 200]);
        $this->set(compact('movimentacoesfutura', 'movimentacaobancarias'));
        $this->set('_serialize', ['movimentacoesfutura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Movimentacoesfutura id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $movimentacoesfutura = $this->Movimentacoesfuturas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movimentacoesfutura = $this->Movimentacoesfuturas->patchEntity($movimentacoesfutura, $this->request->data);


            if ($movimentacoesfutura->pago && $movimentacoesfutura->dirty('pago')) {// antes estava como pago, e agora está como não pago, então tem que atualizar a conta p voltar com o saldo anterior ao pagamento
                $this->efetuarpagamento($movimentacoesfutura);
            } else if (!$movimentacoesfutura->pago && $movimentacoesfutura->dirty('pago')) {
                $this->desfazerpagamento($movimentacoesfutura);
            }


            if ($this->Movimentacoesfuturas->save($movimentacoesfutura)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacoesfutura->movimentacaobancaria_id]);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $this->set(compact('movimentacoesfutura'));
        $this->set('_serialize', ['movimentacoesfutura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Movimentacoesfutura id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $movimentacoesfutura = $this->Movimentacoesfuturas->get($id);
        if ($this->Movimentacoesfuturas->delete($movimentacoesfutura)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

    public function efetuarpagamento($movimentacaofutura, $parcela = true) {
        $this->loadModel('Movimentacaobancarias');
        $contabancariaTable = TableRegistry::get('Contabancarias');

        $movimentacaobancaria = $this->Movimentacaobancarias->get($movimentacaofutura->movimentacaobancaria_id);
        $calcular = $this->Movimentacaobancarias->Movimentacaotipos->find()->select(['evento'])->where(['Movimentacaotipos.id' => $movimentacaobancaria->movimentacaotipo_id])->first();
        $contabancaria = $contabancariaTable->get($movimentacaobancaria->contabancaria_id);

        $valor = $movimentacaofutura->valorparcela;

        if ($calcular->evento == 'sub') {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa - $valor;
        } else {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa + $valor;
        }

        $contaspagas = $this->Movimentacoesfuturas->find()->where(['movimentacaobancaria_id' => $movimentacaobancaria->id, 'pago' => '1'])->count();
        ++$contaspagas;
        if ($contaspagas == $movimentacaobancaria->num_parcelas) {
            $movimentacaobancaria->pago = 1;
        }

        $movimentacaobancaria->saldo_posterior = $contabancaria->valorcaixa;

        if ($contabancariaTable->save($contabancaria) && $this->Movimentacaobancarias->save($movimentacaobancaria)) {
            $this->Flash->success(__('Saldo da Conta Bancária Atualizado.'));
        } else {
            $this->Flash->error(__('Houve um erro ao atualiza o saldo da conta, contate o administrador.'));
        }

        return $contabancaria->valorcaixa;
    }

    public function desfazerpagamento($movimentacaofutura, $parcela = true) {
        $this->loadModel('Movimentacaobancarias');
        $contabancariaTable = TableRegistry::get('Contabancarias');

        $movimentacaobancaria = $this->Movimentacaobancarias->get($movimentacaofutura->movimentacaobancaria_id);
        $calcular = $this->Movimentacaobancarias->Movimentacaotipos->find()->select(['evento'])->where(['Movimentacaotipos.id' => $movimentacaobancaria->movimentacaotipo_id])->first();
        $contabancaria = $contabancariaTable->get($movimentacaobancaria->contabancaria_id);

        $valor = $movimentacaofutura->valorparcela;

        if ($calcular->evento == 'sub') {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa + $valor;
        } else {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa - $valor;
        }

        $contaspagas = $this->Movimentacoesfuturas->find()->where(['movimentacaobancaria_id' => $movimentacaobancaria->id, 'pago' => '1'])->count();
        --$contaspagas;
        if ($contaspagas < $movimentacaobancaria->num_parcelas) {
            $movimentacaobancaria->pago = 0;
        }
        $movimentacaobancaria->saldo_posterior = $contabancaria->valorcaixa;

        if ($contabancariaTable->save($contabancaria) && $this->Movimentacaobancarias->save($movimentacaobancaria)) {
            $this->Flash->success(__('A movimentação financeira anterior foi desfeita'));
        } else {
            $this->Flash->error(__('Houve um erro ao atualiza o saldo da conta, contate o administrador.'));
        }

        return $contabancaria->valorcaixa;
    }

}
