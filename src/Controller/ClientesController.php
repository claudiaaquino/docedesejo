<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        if ($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) {
            $clientes = $this->paginate($this->Clientes->find()->where(['Clientes.empresa_id' => $this->Auth->user('empresa_id')]));
        }

        $this->set(compact('clientes'));
        $this->set('_serialize', ['clientes']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Empresas', 'Estados', 'Cidades', 'Contabancarias.Bancos',
                'Movimentacaobancarias' => ['sort' => ['Movimentacaobancarias.dt_cadastro' => 'DESC']], 'Movimentacaobancarias.Formaspagamentos', 
                'Movimentacaobancarias.Contabancarias.Bancos']
        ]);

        if ($cliente->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cliente = $this->Clientes->newEntity();
        if ($this->request->is('post')) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            $cliente->dt_cadastro = date('Y-m-d H:i:s');
            $cliente->user_id = $this->Auth->user('id');
            $cliente->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $estados = $this->Clientes->Estados->find('list');
        $this->set(compact('cliente', 'estados'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cliente = $this->Clientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);


            if ($cliente->empresa_id != $this->Auth->user('empresa_id') || ($cliente->empresa_id == $this->Auth->user('empresa_id') && !$this->Auth->user('admin_empresa')) || !$this->Auth->user('admin')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect(['action' => 'index']);
            }


            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $estados = $this->Clientes->Estados->find('list');
        $this->set(compact('cliente', 'estados'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);

        if ($cliente->empresa_id == $this->Auth->user('empresa_id')) {
            if ($this->Clientes->delete($cliente)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

}
