<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Documentos Controller
 *
 * @property \App\Model\Table\DocumentosTable $Documentos
 */
class DocumentosController extends AppController {

    public function beforeFilter(Event $event) {
        if ($this->request->params['action'] == 'addajax') {
            $this->request->session()->id($this->request['pass'][0]);
            $this->request->session()->start();
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Tipodocumentos.Grupodocumentos.Areaservicos',
                'Empresas']
        ];

        $query = $this->Documentos->find()->distinct();
        if ($this->Auth->user('empresa_id') == $this->_ID_EMPRESA_ADMIN && $this->Auth->user('admin_setores')) {
            //se qualquer outro usuário da triade && admin_setor -> vê todos os documentos de todas as empresas relacionados ao setor dele
            $query = $query->where(function ($exp, $q) {
                return $exp->in('areaservico_id', $this->Auth->user('admin_setores'));
            });
        } else if ($this->Auth->user('admin_empresa') && $this->Auth->user('empresa_id') != $this->_ID_EMPRESA_ADMIN) {
            //se usuario qualquer && admin_empresa -> vê todos os documentos referentes à essa empresa 
            $query = $query->where(['Documentos.empresa_id' => $this->Auth->user('empresa_id')]);
        } else if ($this->Auth->user('admin_setores') && $this->Auth->user('empresa_id') != $this->_ID_EMPRESA_ADMIN) {
            //se usuario qualquer && admin_setor -> vê todos os documentos referentes àpenas a um setor essa empresa  (e se o usuário tiver mais setores?)
            $query = $query->where(['Documentos.empresa_id' => $this->Auth->user('empresa_id')])
                    ->andWhere(function ($exp, $q) {
                return $exp->in('Areaservicos.id', $this->Auth->user('admin_setores'));
            });
        }

        //todos os usuários, inclusive o usuario que não tem empresa, vê todos que user_enviou = sessão, e user_destinatario = sessão
        if (!$this->Auth->user('admin') && (!$this->Auth->user('admin_empresa') && !$this->Auth->user('empresa_id') == $this->_ID_EMPRESA_ADMIN)) {
            $query = $query->orWhere(['user_destinatario' => $this->Auth->user('id')])->orWhere(['user_enviou' => $this->Auth->user('id')]);
        }

        if (is_numeric($this->request->data('areaservico_id'))) {
            $query->andWhere(['Areaservicos.id' => $this->request->data('areaservico_id')]);
        }
        if (is_numeric($this->request->data('grupodocumento_id'))) {
            $query->andWhere(['Grupodocumentos.id' => $this->request->data('grupodocumento_id')]);
        }
        if (is_numeric($this->request->data('tipodocumento_id'))) {
            $query->andWhere(['Tipodocumentos.id' => $this->request->data('tipodocumento_id')]);
        }

        if ($this->request->data('dt_inicio')) {
            $query->andWhere(['Documentos.dt_enviado >= ' => $this->convertDateBRtoEN($this->request->data('dt_inicio'))]);
        }
        if ($this->request->data('dt_final')) {
            $query->andWhere(['Documentos.dt_enviado <= ' => $this->convertDateBRtoEN($this->request->data('dt_final'))]);
        }


        $documentos = $this->paginate($query->orderDesc('dt_enviado'));

        /* -------------***DADOS USADOS NO FILTRO PARA FILTRAR OS DADOS DA TABELA******* */
        $this->loadModel('Areaservicos');
        $areaservicos = $this->Areaservicos->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 '])->orderDesc('descricao');

        $this->set(compact('documentos', 'areaservicos'));
        $this->set('_serialize', ['documentos']);
    }

    /**
     * Loadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function loadajax() {
        $where = array();
        if ($this->request->query('empresa_id')) {
            $where['Documentos.empresa_id'] = $this->request->query('empresa_id');
        }
        if ($this->request->query('tipodocumento_id')) {
            $where['Documentos.tipodocumento_id'] = $this->request->query('tipodocumento_id');
        }
        if ($this->request->query('grupodocumento_id')) {
            $where['Tipodocumentos.grupodocumento_id'] = $this->request->query('grupodocumento_id');
        }
        if ($this->request->query('areaservico_id')) {
            $where['Grupodocumentos.areaservico_id'] = $this->request->query('areaservico_id');
        }

        $documentos = $this->Documentos->find('all')->contain(['Tipodocumentos.Grupodocumentos.Areaservicos', 'Empresas'])->where($where);

        $tpview = 'table';
        $this->set(compact('documentos', 'tpview'));
        $this->set('_serialize', ['documentos', 'tpview']);
        $this->render('ajax');
    }

    /**
     * View method
     *
     * @param string|null $id Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $documento = $this->Documentos->get($id, [
            'contain' => ['Tipodocumentos.Grupodocumentos', 'Empresas']
        ]);
//        $user_leu = $this->Documentos->Users->find()->select(['id', 'nome'])->where(['id' => $documento->user_leu])->first();
        $user_enviou = $this->Documentos->Users->find()->select(['id', 'nome'])->where(['id' => $documento->user_enviou])->first();

        $this->set(compact('documento', 'user_enviou'));
        $this->set('_serialize', ['documento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $documento = $this->Documentos->newEntity();
        if ($this->request->is('post')) {
            $documento = $this->Documentos->patchEntity($documento, $this->request->data);

            $documentos = array();
            if (!empty($documento->files)) {
                $documentos = $this->saveDocuments($documento);
            }
            if ($this->Documentos->saveMany($documentos)) {
                $this->Flash->success(__('Enviado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O registro de documento não pôde ser salvo. Por favor, tente novamente.'));
            }
        }

        $empresas = $this->Documentos->Empresas->find('list')->where(['id' => $this->_ID_EMPRESA_ADMIN])->orWhere(['id' => $this->Auth->user('empresa_id')]);

        $areaservicos = $this->Documentos->Empresas->Areaservicos->find('list', ['order' => 'Areaservicos.descricao DESC'])->where(['todos = 1 ']);
        $this->set(compact('documento', 'areaservicos', 'empresas'));
        $this->set('_serialize', ['documento']);
    }

    /**
     * SaveDocument method
     *
     * @return Documentos[] $documentos
     */
    public function saveDocuments($documento) {
        $documentos = array();
        foreach ($documento->files as $key => $file) {
            if ($file['size'] > 0) {
                $newdocumento = $this->Documentos->newEntity();
                $newdocumento->empresa_id = $this->Auth->user('empresa_id');
                $newdocumento->user_enviou = $this->Auth->user('id');
                $newdocumento->filesize = $file['size'];
                $newdocumento->tipodocumento_id = $documento->tipo_documentos[$key];
                $newdocumento->descricao = $documento->descricoes[$key];
                $newdocumento->file = $this->saveAttachment($file);
                $newdocumento->dt_enviado = date('Y-m-d H:i:s');
                $documentos[] = $newdocumento;
            }
        }
        return $documentos;
    }

    /**
     * Addajax method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addajax() {
        if ($this->request->is('post') || $this->request->is('put') || $this->request->is('get')) {
            $documento = $this->Documentos->patchEntity($this->Documentos->newEntity(), $this->request->data);
            $documento->user_enviou = $this->Auth->user('id');
            $documento->empresa_id = $this->request->data('empresa_id');


            if (!empty($this->request->data['Filename'])) {
                $file = $this->request->data['Filedata']; //put the data into a var for easy use

                $setNewFileName = time() . "_" . rand(000000, 999999);
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/' . $setNewFileName . '.' . $ext);


                $documento->file = $setNewFileName . '.' . $ext;
                $documento->dt_enviado = date('Y-m-d H:i:s');
                if ($this->Documentos->save($documento)) {
                    $retorno = $documento->file;
                } else {
                    $retorno = 'Deu erro ao salvar';
                }
            } else {
                $retorno = 'não recebeu o arquivo';
            }
        }

        $this->set('tpview', 'add');
        $this->set('retorno', $retorno);
        $this->viewBuilder()->layout('ajax');
        $this->render('ajax');
    }

    /**
     * Delete method
     *
     * @param string|null $id Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $documento = $this->Documentos->get($id);
        if ($this->Documentos->delete($documento)) {
            $this->Flash->success(__('O registro de documento foi deletado.'));
        } else {
            $this->Flash->error(__('O registro de documento não pôde ser deletado. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
