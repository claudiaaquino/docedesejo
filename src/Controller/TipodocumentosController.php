<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipodocumentos Controller
 *
 * @property \App\Model\Table\TipodocumentosTable $Tipodocumentos
 */
class TipodocumentosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => 'Grupodocumentos'
        ];
        
        $tipodocumentos = $this->paginate($this->Tipodocumentos);
        
        $this->set(compact('tipodocumentos'));
        $this->set('_serialize', ['tipodocumentos']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax($grupodocumento_id = null) {// retorna os grupos, de acordo com o setor passado por parametro, e codifica para o tipo json 
        $this->viewBuilder()->layout('ajax');
        $retorno = $this->Tipodocumentos->find('list')->where(['grupodocumento_id' => $grupodocumento_id])->order(['descricao' => 'ASC']);

        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
        $this->render('ajax');
    }

    /**
     * View method
     *
     * @param string|null $id Tipodocumento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tipodocumento = $this->Tipodocumentos->get($id, [
            'contain' => ['Grupodocumentos', 'Documentos.Empresas']
        ]);

        $this->set('tipodocumento', $tipodocumento);
        $this->set('_serialize', ['tipodocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tipodocumento = $this->Tipodocumentos->newEntity();
        if ($this->request->is('post')) {
            $tipodocumento = $this->Tipodocumentos->patchEntity($tipodocumento, $this->request->data);
            if ($this->Tipodocumentos->save($tipodocumento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar tipo de documento. Por favor, tente novamente.'));
            }
        }
        
        $grupodocumentos = $this->Tipodocumentos->Grupodocumentos->find('list', ['limit' => 200]);
        $this->set(compact('tipodocumento', 'grupodocumentos'));
        $this->set('_serialize', ['tipodocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipodocumento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tipodocumento = $this->Tipodocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipodocumento = $this->Tipodocumentos->patchEntity($tipodocumento, $this->request->data);
            if ($this->Tipodocumentos->save($tipodocumento)) {
                $this->Flash->success(__('Tipo de documento atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar tipo de documento. Por favor, tente novamente.'));
            }
        }
        
        $grupodocumentos = $this->Tipodocumentos->Grupodocumentos->find('list', ['limit' => 200]);
        $this->set(compact('tipodocumento', 'grupodocumentos'));
        $this->set('_serialize', ['tipodocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipodocumento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tipodocumento = $this->Tipodocumentos->get($id);
        if ($this->Tipodocumentos->delete($tipodocumento)) {
            $this->Flash->success(__('Tipo de documento removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover tipo de documento. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
