<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Usermodulos Controller
 *
 * @property \App\Model\Table\UsermodulosTable $Usermodulos
 */
class UsermodulosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'Modulos']
        ];

        $usermodulos = $this->paginate($this->Usermodulos);
        
        if (!$this->Auth->user('admin')) {
            $this->Flash->error(__('Acesso Restrito somente à Administradores'));
            $this->viewBuilder()->layout('acessoindevido');
        }

        $this->set(compact('usermodulos'));
        $this->set('_serialize', ['usermodulos']);
    }

    /**
     * View method
     *
     * @param string|null $id Usermodulo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $usermodulo = $this->Usermodulos->get($id, [
            'contain' => ['Users', 'Modulos']
        ]);

        $this->set('usermodulo', $usermodulo);
        $this->set('_serialize', ['usermodulo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $usermodulo = $this->Usermodulos->newEntity();
        if ($this->request->is('post')) {
            $usermodulo = $this->Usermodulos->patchEntity($usermodulo, $this->request->data);
            if ($this->Usermodulos->save($usermodulo)) {
                $this->Flash->success(__('O registro de usermodulo foi salvo.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O registro de usermodulo não pôde ser salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->Usermodulos->Users->find('list', ['limit' => 200]);
        $modulos = $this->Usermodulos->Modulos->find('list', ['limit' => 200]);
        $this->set(compact('usermodulo', 'users', 'modulos'));
        $this->set('_serialize', ['usermodulo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Usermodulo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $usermodulo = $this->Usermodulos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usermodulo = $this->Usermodulos->patchEntity($usermodulo, $this->request->data);
            if ($this->Usermodulos->save($usermodulo)) {
                $this->Flash->success(__('O registro de usermodulo foi salvo.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O registro de usermodulo não pôde ser salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->Usermodulos->Users->find('list', ['limit' => 200]);
        $modulos = $this->Usermodulos->Modulos->find('list', ['limit' => 200]);
        $this->set(compact('usermodulo', 'users', 'modulos'));
        $this->set('_serialize', ['usermodulo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Usermodulo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $usermodulo = $this->Usermodulos->get($id);
        if ($this->Usermodulos->delete($usermodulo)) {
            $this->Flash->success(__('O registro de usermodulo foi deletado.'));
        } else {
            $this->Flash->error(__('O registro de usermodulo não pôde ser deletado. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
