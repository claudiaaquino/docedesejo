<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefausuarios Controller
 *
 * @property \App\Model\Table\TarefausuariosTable $Tarefausuarios
 */
class TarefausuariosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Tarefas', 'Users']
        ];
        $tarefausuarios = $this->paginate($this->Tarefausuarios);

        $this->set(compact('tarefausuarios'));
        $this->set('_serialize', ['tarefausuarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefausuario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tarefausuario = $this->Tarefausuarios->get($id, [
            'contain' => ['Tarefas', 'Users']
        ]);

        $this->set('tarefausuario', $tarefausuario);
        $this->set('_serialize', ['tarefausuario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($tarefa_id = null) {
        $tarefausuario = $this->Tarefausuarios->newEntity();
        if ($this->request->is('post')) {
            $tarefausuario = $this->Tarefausuarios->patchEntity($tarefausuario, $this->request->data);

            $users = array();
            if ($tarefausuario->user_ids) {
                foreach ($tarefausuario->user_ids as $user_id) {
                    $user_id = is_numeric($user_id) ? $user_id : $user_id->id;
                    $user = $this->Tarefausuarios->newEntity();
                    $user->user_id = $user_id;
                    $user->tarefa_id = $tarefausuario->tarefa_id;
                    $users[] = $user;
                }
            }

            if ($this->Tarefausuarios->saveMany($users)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));
                if ($tarefausuario->backlink) {
                    return $this->redirect($tarefausuario->backlink);
                } else {
                    return $this->redirect(['controller' => 'Tarefas', 'action' => 'view', $tarefausuario->tarefa_id]);
                }
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        if ($this->Auth->user('admin')) {
            $empresas = $this->Tarefausuarios->Users->Empresas->find('list');
            $users = $this->Tarefausuarios->Users->find('list');
        } else if ($this->Auth->user('admin_empresa')) {
            $users = $this->Tarefausuarios->Users->find('list')->where(['Users.empresa_id' => $this->Auth->user('empresa_id')]);
        }
        $areaservicos = $this->Tarefausuarios->Tarefas->Areaservicos->find('list')->where(['Areaservicos.empresa_id' => $this->Auth->user('empresa_id')]);
        $tarefausuario->tarefa_id = $tarefa_id;

        $this->set(compact('tarefausuario', 'tarefas', 'users', 'empresas', 'areaservicos'));
        $this->set('_serialize', ['tarefausuario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefausuario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tarefausuario = $this->Tarefausuarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefausuario = $this->Tarefausuarios->patchEntity($tarefausuario, $this->request->data);
            if ($this->Tarefausuarios->save($tarefausuario)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $tarefas = $this->Tarefausuarios->Tarefas->find('list', ['limit' => 200]);
        $users = $this->Tarefausuarios->Users->find('list', ['limit' => 200]);
        $this->set(compact('tarefausuario', 'tarefas', 'users'));
        $this->set('_serialize', ['tarefausuario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefausuario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tarefausuario = $this->Tarefausuarios->get($id);
        if ($this->Tarefausuarios->delete($tarefausuario)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }
        return $this->redirect($this->request->referer());
    }

}
