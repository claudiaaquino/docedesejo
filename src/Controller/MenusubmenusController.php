<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Menusubmenus Controller
 *
 * @property \App\Model\Table\MenusubmenusTable $Menusubmenus
 */
class MenusubmenusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Menus', 'Submenus']
        ];
        $menusubmenus = $this->paginate($this->Menusubmenus);

        $this->set(compact('menusubmenus'));
        $this->set('_serialize', ['menusubmenus']);
    }

    /**
     * View method
     *
     * @param string|null $id Menusubmenu id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menusubmenu = $this->Menusubmenus->get($id, [
            'contain' => ['Menus', 'Submenus']
        ]);

        $this->set('menusubmenu', $menusubmenu);
        $this->set('_serialize', ['menusubmenu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menusubmenu = $this->Menusubmenus->newEntity();
        if ($this->request->is('post')) {
            $menusubmenu = $this->Menusubmenus->patchEntity($menusubmenu, $this->request->data);
            $menusubmenu->dt_cadastro =  date('Y-m-d H:i:s');
            $menusubmenu->user_id =  $this->Auth->user('id');
            $menusubmenu->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Menusubmenus->save($menusubmenu)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $menus = $this->Menusubmenus->Menus->find('list', ['limit' => 200]);
        $submenus = $this->Menusubmenus->Submenus->find('list', ['limit' => 200]);
        $this->set(compact('menusubmenu', 'menus', 'submenus'));
        $this->set('_serialize', ['menusubmenu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Menusubmenu id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menusubmenu = $this->Menusubmenus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menusubmenu = $this->Menusubmenus->patchEntity($menusubmenu, $this->request->data);
            if ($this->Menusubmenus->save($menusubmenu)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $menus = $this->Menusubmenus->Menus->find('list', ['limit' => 200]);
        $submenus = $this->Menusubmenus->Submenus->find('list', ['limit' => 200]);
        $this->set(compact('menusubmenu', 'menus', 'submenus'));
        $this->set('_serialize', ['menusubmenu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Menusubmenu id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menusubmenu = $this->Menusubmenus->get($id);
        if ($this->Menusubmenus->delete($menusubmenu)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
