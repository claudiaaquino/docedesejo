<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefadocumentos Controller
 *
 * @property \App\Model\Table\TarefadocumentosTable $Tarefadocumentos
 */
class TarefadocumentosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Tarefas', 'Documentos']
        ];
        $tarefadocumentos = $this->paginate($this->Tarefadocumentos);

        $this->set(compact('tarefadocumentos'));
        $this->set('_serialize', ['tarefadocumentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefadocumento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tarefadocumento = $this->Tarefadocumentos->get($id, [
            'contain' => ['Tarefas', 'Documentos']
        ]);

        $this->set('tarefadocumento', $tarefadocumento);
        $this->set('_serialize', ['tarefadocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($tarefa_id = null) {
        $tarefadocumento = $this->Tarefadocumentos->newEntity();
        if ($this->request->is('post')) {
            $tarefadocumento = $this->Tarefadocumentos->patchEntity($tarefadocumento, $this->request->data);
//            debug($tarefadocumento);
//            die;
            $tarefadocumentos = array();
            if (!empty($tarefadocumento->file)) {
                $tarefadocumentos = $this->saveDocuments($tarefadocumento);
            }

            if ($this->Tarefadocumentos->saveMany($tarefadocumentos)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));
                if ($tarefadocumento->backlink) {
                    return $this->redirect($tarefadocumento->backlink);
                } else {
                    return $this->redirect(['controller' => 'Tarefas', 'action' => 'view', $tarefadocumento->tarefa_id]);
                }
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $areaservicos = $this->Tarefadocumentos->Tarefas->Areaservicos->find('list')->where(['Areaservicos.todos = 1']);
        $tarefadocumento->tarefa_id = $tarefa_id;

        $this->set(compact('tarefadocumento', 'areaservicos'));
        $this->set('_serialize', ['tarefadocumento']);
    }

    /**
     * SaveDocument method
     *
     * @return Tarefadocumentos[] $tarefadocumentos
     */
    public function saveDocuments($tarefadocumento) {
        $tarefadocumentos = array();
        foreach ($tarefadocumento->file as $key => $file) {
            if ($file['size'] > 0) {
                $documento = $this->Tarefadocumentos->Documentos->newEntity();
                $documento->empresa_id = $this->Auth->user('empresa_id');
                $documento->user_enviou = $this->Auth->user('id');
                $documento->filesize = $file['size'];
                $documento->tipodocumento_id = $tarefadocumento->tipo_documentos[$key];
                $documento->descricao = $tarefadocumento->descricoes[$key];
                $documento->file = $this->saveAttachment($file);
                $documento->dt_enviado = date('Y-m-d H:i:s');
                if ($this->Tarefadocumentos->Documentos->save($documento)) {
                    $newtarefadocumento = $this->Tarefadocumentos->newEntity();
                    $newtarefadocumento->documento_id = $documento->id;
                    $newtarefadocumento->tarefa_id = $tarefadocumento->tarefa_id;
                    $tarefadocumentos[] = $newtarefadocumento;
                }
            }
        }
        return $tarefadocumentos;
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefadocumento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tarefadocumento = $this->Tarefadocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefadocumento = $this->Tarefadocumentos->patchEntity($tarefadocumento, $this->request->data);
            if ($this->Tarefadocumentos->save($tarefadocumento)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $tarefas = $this->Tarefadocumentos->Tarefas->find('list', ['limit' => 200]);
        $documentos = $this->Tarefadocumentos->Documentos->find('list', ['limit' => 200]);
        $this->set(compact('tarefadocumento', 'tarefas', 'documentos'));
        $this->set('_serialize', ['tarefadocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefadocumento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tarefadocumento = $this->Tarefadocumentos->get($id);
        if ($this->Tarefadocumentos->delete($tarefadocumento)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
