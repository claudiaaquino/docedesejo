<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefas Controller
 *
 * @property \App\Model\Table\TarefasTable $Tarefas
 */
class TarefasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $this->paginate = [
            'contain' => ['Users', 'Tarefatipos', 'Tarefaprioridades', 'Areaservicos', 'Tiposervicos']
        ];
        $tarefas = $this->paginate($this->Tarefas
                        ->find()
                        ->innerJoinWith('Tarefausuarios')
                        ->orderAsc('Tarefas.dt_inicio')
                        ->where(['Tarefausuarios.user_id' => $this->Auth->user('id')])
        );

        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas']);
    }

    /**
     * Upcomingtasks method
     *
     * @return \Cake\Network\Response|null
     */
    public function upcomingtasksajax() {
        $tarefas = $this->Tarefas->find()
                ->innerJoinWith('Tarefausuarios')
                ->contain(['Tarefaprioridades', 'Tarefatipos'])
                ->where(['Tarefausuarios.user_id' => $this->Auth->user('id')])
                ->andWhere(['Tarefausuarios.dt_concluida is null'])
                ->orderAsc('Tarefas.dt_inicio');

        $count_upcoming = $tarefas->count();
        $this->set(compact('tarefas', 'count_upcoming'));
        $this->set('_serialize', ['tarefas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Empresas', 'Areaservicos', 'Users', 'Tarefatipos', 'Tiposervicos', 'Tarefaprioridades', 'Tarefadocumentos.Documentos.Tipodocumentos', 'Tarefausuarios.Users']
        ]);

        $this->set('tarefa', $tarefa);
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $tarefa = $this->Tarefas->newEntity();
        if ($this->request->is('post')) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);
            $tarefa->dt_lembrete = $this->convertDateBRtoEN($tarefa->dt_lembrete);
            $tarefa->hr_tarefa = $tarefa->hr_tarefa ? $tarefa->hr_tarefa : '';
            $tarefa->dt_inicio = $this->convertDateBRtoEN($tarefa->dt_inicio) . ' ' . $tarefa->hr_tarefa;
            $tarefa->dt_final = $this->convertDateBRtoEN($tarefa->dt_final);
            $tarefa->empresa_cadastrou = $this->Auth->user('empresa_id');
            /* --------------------------------------------------------------------------------------------- */
            /* --------------------------------------------------------------------------------------------- */
            $users = array();
            if ($tarefa->user_ids) {
                //fazer aqui para capturar emails digitados também.
            } else if ($tarefa->admin_empresa) {
                $tarefa->user_ids = $this->Tarefas->Users->find()->select(['id'])->where(['empresa_id' => $tarefa->empresa_id, 'admin_empresa = 1 ']);
            } else if ($tarefa->admin_setor) {
                $tarefa->user_ids = $this->Tarefas->Users->innerJoinWith('Usersareaservicos')->find()->select(['id'])->where(['Users.empresa_id' => $tarefa->empresa_id, 'Usersareaservicos.areaservico_id' => $tarefa->areaservico_id, 'Usersareaservicos.admin_setor = 1 ']);
            } else if ($tarefa->empresa_id && (!$tarefa->areaservico_id || $tarefa->areaservico_id == 'null')) {
                $tarefa->user_ids = $this->Tarefas->Users->find()->select(['id'])->where(['empresa_id' => $tarefa->empresa_id]);
            } else if ($tarefa->empresa_id && ($tarefa->areaservico_id && $tarefa->areaservico_id != 'null')) {
                $tarefa->user_ids = $this->Tarefas->Users->find()->innerJoinWith('Usersareaservicos')->select(['Users.id'])
                        ->where(['Usersareaservicos.empresa_id' => $tarefa->empresa_id, 'Usersareaservicos.areaservico_id' => $tarefa->areaservico_id]);
            }

            if ($tarefa->user_ids) {
                foreach ($tarefa->user_ids as $user_id) {
                    $user_id = is_numeric($user_id) ? $user_id : $user_id->id;
                    $user = $this->Tarefas->Tarefausuarios->newEntity();
                    $user->user_id = $user_id;
                    $users[] = $user;
                }
            } else {
                $user = $this->Tarefas->Tarefausuarios->newEntity();
                $user->user_id = $this->Auth->user('id');
                $users[] = $user;
            }
            $tarefa->tarefausuarios = $users;


            $tarefadocumentos = array();
            if (!empty($tarefa->file)) {
                $tarefadocumentos = $this->saveDocuments($tarefa);
            }
            $tarefa->tarefadocumentos = $tarefadocumentos;


            /* --------------------------------------------------------------------------------------------- */
            /* --------------------------------------------------------------------------------------------- */

            $tarefa->dt_cadastro = date('Y-m-d h:i:s');
            $tarefa->user_id = $this->Auth->user('id');
//            debug($tarefa);
//            die;
            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'agenda']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }


        $tarefa->dt_lembrete = $this->convertDateENtoBR($tarefa->dt_lembrete);
        $dt_tarefa = explode(' ', $tarefa->dt_inicio);
        $tarefa->dt_inicio = $this->convertDateENtoBR($dt_tarefa[0]);
        $tarefa->hr_tarefa = $dt_tarefa[1];
        $tarefa->dt_final = $this->convertDateENtoBR($tarefa->dt_final);


        $users = $this->Tarefas->Users->find('list');
        $tarefatipos = $this->Tarefas->Tarefatipos->find('list');
        $tarefaprioridades = $this->Tarefas->Tarefaprioridades->find('list');
        $areaservicos = $this->Tarefas->Areaservicos->find('list');
        $this->set(compact('tarefa', 'users', 'tarefatipos', 'tarefaprioridades', 'areaservicos', 'tiposervicos'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * SaveDocument method
     *
     * @return Mensagensdocumentos[] $mensagemdocumentos
     */
    public function saveDocuments($tarefa) {
        $tarefadocumentos = array();
        $this->loadModel('Documentos');
        foreach ($tarefa->file as $key => $file) {
            if ($file['size'] > 0) {
                $documento = $this->Documentos->newEntity();
                $documento->empresa_id = $this->Auth->user('empresa_id');
                $documento->user_enviou = $this->Auth->user('id');
                $documento->filesize = $file['size'];
                $documento->tipodocumento_id = $tarefa->tipo_documentos[$key];
                $documento->descricao = $tarefa->descricoes[$key];
                $documento->file = $this->saveAttachment($file);
                $documento->dt_enviado = date('Y-m-d H:i:s');
                if ($this->Documentos->save($documento)) {
                    $tarefadocumento = $this->Tarefas->Tarefadocumentos->newEntity();
                    $tarefadocumento->documento_id = $documento->id;
                    $tarefadocumentos[] = $tarefadocumento;
                }
            }
        }
        return $tarefadocumentos;
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Tarefadocumentos.Documentos.Tipodocumentos', 'Tarefausuarios.Users']
        ]);
        if ($tarefa->user_id != $this->Auth->user('id') && !$tarefa->permite_editar) {
            $this->Flash->error(__('Você não tem permissão para deletar essa tarefa.'));
            return $this->redirect(['action' => 'view', $id]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);
            $tarefa->dt_lembrete = $this->convertDateBRtoEN($tarefa->dt_lembrete);
            $tarefa->hr_tarefa = $tarefa->hr_tarefa ? $tarefa->hr_tarefa : '';
            $tarefa->dt_inicio = $this->convertDateBRtoEN($tarefa->dt_inicio) . ' ' . $tarefa->hr_tarefa;
            $tarefa->dt_final = $this->convertDateBRtoEN($tarefa->dt_final);

            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'agenda']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $tarefa->dt_lembrete = $this->convertDateENtoBR($tarefa->dt_lembrete);
        $dt_tarefa = explode(' ', $tarefa->dt_inicio);
        $tarefa->dt_inicio = $this->convertDateENtoBR($dt_tarefa[0]);
        $tarefa->hr_tarefa = $dt_tarefa[1];
        $tarefa->dt_final = $this->convertDateENtoBR($tarefa->dt_final);


        $tarefatipos = $this->Tarefas->Tarefatipos->find('list');
        $tarefaprioridades = $this->Tarefas->Tarefaprioridades->find('list');
        $areaservicos = '';
        if ($tarefa->empresa_id) {
            $areaservicos = $this->Tarefas->Areaservicos->find('list')->where(['empresa_id' => $tarefa->empresa_id])->orWhere(['todos = 1']);
        }
        $gruposervicos = '';
        if ($tarefa->areaservico_id) {
            $gruposervicos = $this->Tarefas->Areaservicos->Gruposervicos->find('list')->where(['areaservico_id' => $tarefa->areaservico_id]);
        }
        $tiposervicos = '';
        if ($tarefa->tiposervico) {
            $tiposervicos = $this->Tarefas->Tiposervicos->find('list')->where(['gruposervico_id' => $tarefa->tiposervico->gruposervico_id]);
        }

        $this->set(compact('tarefa',  'tarefatipos', 'tarefaprioridades', 'areaservicos', 'tiposervicos', 'gruposervicos'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {

        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);
        if ($this->Tarefas->delete($tarefa)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     *  Função para carregar a view agenda e seus dados.
     */
    public function agenda() {

        $tarefa = $this->Tarefas->newEntity();
        $tarefatipos = $this->Tarefas->Tarefatipos->find('list')
                ->where(['Tarefatipos.empresa_id' => $this->Auth->user('empresa_id'), 'Tarefatipos.todos_empresa' => '1'])
                ->orWhere(['Tarefatipos.todos_sistema' => '1'])
                ->orWhere(['Tarefatipos.user_id' => $this->Auth->user('id')]);
        $tarefaprioridades = $this->Tarefas->Tarefaprioridades->find('list');
        
        $areaservicos = $this->Tarefas->Areaservicos->find('list')->where(['todos' => 1])->orWhere(['empresa_id' => $this->Auth->user('empresa_id')]);
        $users = $this->Tarefas->Users->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);

        $this->set(compact('tarefa', 'tarefatipos', 'tarefaprioridades', 'areaservicos', 'users'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Função para carregar as tarefas via ajax.
     */
    public function load() {
        $user_id = $this->request->query("user_id");
        $empresa_id = $this->request->query("empresa_id");
        $areaservico_id = $this->request->query("areaservico_id");
        $tiposervico_id = $this->request->query("tiposervico_id");

        //lembrar de colocar as verificações de permissão aqui
        if ($user_id) {
            $tarefas = $this->Tarefas->find()->innerJoinWith('Tarefausuarios')->contain(['Tarefatipos', 'Tarefaprioridades'])->where(['Tarefausuarios.user_id' => $user_id]);
        } else {
            $tarefas = $this->Tarefas->find()->innerJoinWith('Tarefausuarios')->contain(['Tarefatipos', 'Tarefaprioridades'])->where(['Tarefausuarios.user_id' => $this->Auth->user('id')]);
        }

        if ($this->request->query("mes") && $this->request->query("ano")) {
            $tarefas->where(['YEAR(dt_inicio)' => $this->request->query("ano"), 'MONTH(dt_inicio)' => $this->request->query("mes")]);
        }

        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas']);
    }

    /**
     * Função para remover a tarefa via ajax. Os parametros são passados por json.
     */
    public function deleteajax() {

        $id = $this->request->data["id"];
        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);

        if ($this->Tarefas->delete($tarefa)) {
            $success = true;
        } else {
            $success = false;
        }
    }

}
