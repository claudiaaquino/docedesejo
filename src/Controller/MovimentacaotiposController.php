<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Movimentacaotipos Controller
 *
 * @property \App\Model\Table\MovimentacaotiposTable $Movimentacaotipos
 */
class MovimentacaotiposController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $movimentacaotipos = $this->paginate($this->Movimentacaotipos);

        $this->set(compact('movimentacaotipos'));
        $this->set('_serialize', ['movimentacaotipos']);
    }

    /**
     * View method
     *
     * @param string|null $id Movimentacaotipo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $movimentacaotipo = $this->Movimentacaotipos->get($id, [
            'contain' => [ 'Movimentacaobancarias' =>
                ['sort' => ['Movimentacaobancarias.dt_cadastro' => 'DESC']],
                'Movimentacaobancarias.Formaspagamentos',
                'Movimentacaobancarias.Contabancarias.Bancos'],
            
        ]);

        $this->set('movimentacaotipo', $movimentacaotipo);
        $this->set('_serialize', ['movimentacaotipo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $movimentacaotipo = $this->Movimentacaotipos->newEntity();
        if ($this->request->is('post')) {
            $movimentacaotipo = $this->Movimentacaotipos->patchEntity($movimentacaotipo, $this->request->data);
            $movimentacaotipo->dt_cadastro = date('Y-m-d H:i:s');
            $movimentacaotipo->user_id = $this->Auth->user('id');
            if ($this->Movimentacaotipos->save($movimentacaotipo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('movimentacaotipo'));
        $this->set('_serialize', ['movimentacaotipo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Movimentacaotipo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $movimentacaotipo = $this->Movimentacaotipos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movimentacaotipo = $this->Movimentacaotipos->patchEntity($movimentacaotipo, $this->request->data);
            if ($this->Movimentacaotipos->save($movimentacaotipo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('movimentacaotipo'));
        $this->set('_serialize', ['movimentacaotipo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Movimentacaotipo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $movimentacaotipo = $this->Movimentacaotipos->get($id);
        if ($this->Movimentacaotipos->delete($movimentacaotipo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
