<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Empresas Controller
 *
 * @property \App\Model\Table\EmpresasTable $Empresas
 */
class EmpresasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function index() {
        $empresas = $this->paginate($this->Empresas->find()->where(['solicitacao' => '0']));

        $this->set(compact('empresas'));
        $this->set('_serialize', ['empresas']);
    }

    /**
     * View method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        if ($id == null || (!$this->Auth->user('admin') && $id != $this->Auth->user('empresa_id') )) {
            $id = $this->Auth->user('empresa_id');
        }

        $empresa = $this->Empresas->get($id, [
            'contain' => ['Estados', 'Cidades',
                'Documentos.Tipodocumentos.Grupodocumentos',
                'Users']
        ]);

        $this->set('empresa', $empresa);
        $this->set('_serialize', ['empresa']);
    }

    /**
     * Setoresajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function setoresajax($empresa_id) {
        $retorno = $this->Empresas->Areaservicos->find('list')->where(['Areaservicos.empresa_id' => $empresa_id])->orWhere(['Areaservicos.todos' => 1]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Usuariosajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function usuariosajax() {
        if (($this->request->query('empresa_id') != 'null' && $this->request->query('empresa_id') != 'undefined' && $this->request->query('empresa_id') != '') && ($this->request->query('areaservico_id') == 'null' || $this->request->query('areaservico_id') == 'undefined' || $this->request->query('areaservico_id') == '')) {

            $retorno = $this->Empresas->Users->find('list')->where(['Users.empresa_id' => $this->request->query('empresa_id')]);
        } else if ($this->request->query('user_id') && ($this->request->query('empresa_id') == 'null' || $this->request->query('empresa_id') == 'undefined' || $this->request->query('empresa_id') == '') && ($this->request->query('areaservico_id') == 'null' || $this->request->query('areaservico_id') == 'undefined' || $this->request->query('areaservico_id') == '')) {

            $retorno = $this->Empresas->Users->find('list')->where(['Users.id' => $this->request->query('user_id')]);
        } else if (($this->request->query('empresa_id') != 'null' && $this->request->query('empresa_id') != 'undefined' && $this->request->query('empresa_id') != '') && ($this->request->query('areaservico_id') != 'null' && $this->request->query('areaservico_id') != 'undefined' && $this->request->query('areaservico_id') != '')) {

            $retorno = $this->Empresas->Users->find('list')->innerJoinWith('Usersareaservicos')->contain(['Usersareaservicos'])
                    ->select(['Users.id', 'Users.nome'])
                    ->where(['Usersareaservicos.empresa_id' => $this->request->query('empresa_id'), 'Usersareaservicos.areaservico_id' => $this->request->query('areaservico_id')]);
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

}
