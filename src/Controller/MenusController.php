<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Menus Controller
 *
 * @property \App\Model\Table\MenusTable $Menus
 */
class MenusController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Areaservicos']
        ];
        $listmenus = $this->paginate($this->Menus);

        $this->set(compact('listmenus'));
        $this->set('_serialize', ['listmenus']);
    }

    /**
     * View method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $menu = $this->Menus->get($id, [
            'contain' => ['Areaservicos', 'Menusubmenus.Submenus', 'Modulosmenus.Modulos']
        ]);

        $this->set('menu', $menu);
        $this->set('_serialize', ['menu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
            $menu = $this->Menus->patchEntity($menu, $this->request->data);
            $menu->dt_cadastro = date('Y-m-d H:i:s');

            $menusubmenus = array();
            if ($menu->submenu_ids) {
                foreach ($menu->submenu_ids as $submenu) {
                    if (is_numeric($submenu)) {// se o usuário selecionou um submenu já existente
                        $menusubmenu = $this->Menus->Menusubmenus->newEntity();
                        $menusubmenu->submenu_id = $submenu;
                        $menusubmenus[] = $menusubmenu;
                    } else if (is_string($submenu)) {// se o usuário digitou um submenu para ser criado
                        $newsubmenu = $this->Menus->Menusubmenus->Submenus->newEntity();
                        $newsubmenu->nome = $submenu;
                        $newsubmenu->descricao = $submenu;
                        $newsubmenu->action = 'index';
                        $newsubmenu->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Menus->Menusubmenus->Submenus->save($newsubmenu)) {
                            $menusubmenu = $this->Menus->Menusubmenus->newEntity();
                            $menusubmenu->submenu_id = $newsubmenu->id;
                            $menusubmenus[] = $menusubmenu;
                        }
                    }
                }
            }
            $menu->menusubmenus = $menusubmenus;

            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $areaservicos = $this->Menus->Areaservicos->find('list');
        $listsubmenus = $this->Menus->Menusubmenus->Submenus->find('list');
        $this->set(compact('menu', 'areaservicos', 'listsubmenus'));
        $this->set('_serialize', ['menu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $menu = $this->Menus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->Menus->patchEntity($menu, $this->request->data);

            $cadastrados = $this->Menus->Menusubmenus->find()->where(['menu_id' => $id])->extract('submenu_id')->filter()->toArray();

            $menusubmenus = array();
            if ($menu->submenu_ids) {
                foreach ($menu->submenu_ids as $submenu) {
                    if (!in_array($submenu, $cadastrados)) {
                        if (is_numeric($submenu)) {// se o usuário selecionou um submenu já existente
                            $menusubmenu = $this->Menus->Menusubmenus->newEntity();
                            $menusubmenu->submenu_id = $submenu;
                            $menusubmenus[] = $menusubmenu;
                        } else if (is_string($submenu)) {// se o usuário digitou um submenu para ser criado
                            $newsubmenu = $this->Menus->Menusubmenus->Submenus->newEntity();
                            $newsubmenu->nome = $submenu;
                            $newsubmenu->descricao = $submenu;
                            $newsubmenu->action = 'index';
                            $newsubmenu->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Menus->Menusubmenus->Submenus->save($newsubmenu)) {
                                $menusubmenu = $this->Menus->Menusubmenus->newEntity();
                                $menusubmenu->submenu_id = $newsubmenu->id;
                                $menusubmenus[] = $menusubmenu;
                            }
                        }
                    }
                }
            }
            $menu->menusubmenus = $menusubmenus;
            $this->Menus->Menusubmenus->deleteAll(['menu_id' => $id, 'submenu_id NOT IN' => $menu->submenu_ids]);

            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'view', $id]);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->loadModel('Submenus');
        $selectedsubmenus = array_keys($this->Submenus->find('list')->innerJoinWith('Menusubmenus')->where(['Menusubmenus.menu_id' => $id])->toArray());
        $listsubmenus = $this->Menus->Menusubmenus->Submenus->find('list');
        $areaservicos = $this->Menus->Areaservicos->find('list');
        $this->set(compact('menu', 'areaservicos', 'selectedsubmenus', 'listsubmenus'));
        $this->set('_serialize', ['menu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($this->Menus->delete($menu)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
