<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    public $_NOVOUSUARIO = 5;
    public $_MODULONOVOUSUARIO = 6;
    public $_MODULOGERAL = 1;
    public $_MODULOEMPRESA = 8;

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Tipousuarios', 'Usermodulos']
        ];

        if ($this->Auth->user('admin')) {
            $users = $this->paginate($this->Users);
        } else if ($this->Auth->user('admin_empresa')) {
            $users = $this->paginate($this->Users->find()->where(['empresa_id' => $this->Auth->user('empresa_id')]));
        } else {
            $users = null;
            $this->Flash->error(__('Você não tem permissão de visualizar registros que não pertencem à sua Empresa'));
        }


        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        if ($id == null) {
            $id = $this->Auth->user('id');
        }

        $user = $this->Users->get($id, [
            'contain' => ['Empresas', 'Tipousuarios', 'Usermodulos.Modulos', 'Documentos', 'Usersareaservicos.Areaservicos']
        ]);

        if (!$this->Auth->user('admin')) {
            if ($user->empresa_id != $this->Auth->user('empresa_id') || ($user->empresa_id == $this->Auth->user('empresa_id')) && !$this->Auth->user('admin_empresa')) {
                $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa, ou que você não seja administrador dela.'));
                $this->viewBuilder()->layout('acessoindevido');
            }
        }

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function printpdf($id = null) {
        $this->viewBuilder()->layout('pdf');
        if ($id == null) {
            $id = $this->Auth->user('id');
        }

        $user = $this->Users->get($id, [
            'contain' => ['Tipousuarios', 'Usermodulos.Modulos', 'Documentos']
        ]);

        $this->set('user', $user);
        $this->set('filename', '1477874420_138031.pdf');
        $this->set('_serialize', ['user']);
        $this->render('view');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->tipousuario_id = $user->tipousuario_id ? $user->tipousuario_id : $this->_NOVOUSUARIO; // Se não tem essa informação, é pq o proprio usuário se cadastrou pelo e sistema

            /**             * *********MÒDULOS que todos os usuarios tem acesso logo que se cadastram, 
             * lembrar de fazer isso posteriormente por triggers********** */
            /* $usermodulo1 = $this->Users->Usermodulos->newEntity();
              $usermodulo1->modulo_id = $this->_MODULONOVOUSUARIO;

              $usermodulo2 = $this->Users->Usermodulos->newEntity();
              $usermodulo2->modulo_id = $this->_MODULOGERAL;

              $user->usermodulos = array($usermodulo1, $usermodulo2);
             */

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Cadastrado com sucesso. Você já pode acessar o sistema com seus dados.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('Não foi possível cadastrar. Verifique se os campos foram corretamente preenchidos.'));
            }
        }
        $tipousuarios = $this->Users->Tipousuarios->find('list', ['limit' => 200]);
        $this->set(compact('user', 'tipousuarios'));
        $this->set('user', $user);
    }

    public function adduser() {

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->tipousuario_id = $user->tipousuario_id ? $user->tipousuario_id : $this->_NOVOUSUARIO; // Se não tem essa informação, é pq o proprio usuário se cadastrou pelo e sistema

            $user->empresa_id = $user->empresa_id ? $user->empresa_id : $this->Auth->user('empresa_id');

            $usermodulos = array();
            if ($user->modulo_ids) {
                foreach ($user->modulo_ids as $modulo) {
                    if (is_numeric($modulo)) {// se o usuário selecionou um modulo já existente
                        $modulousuario = $this->Users->Usermodulos->newEntity();
                        $modulousuario->modulo_id = $modulo;
                        $usermodulos[] = $modulousuario;
                    } else if (is_string($modulo)) {// se o usuário digitou um modulo para ser criado
                        $newmodulo = $this->Users->Usermodulos->Modulos->newEntity();
                        $newmodulo->nome = $modulo;
                        $newmodulo->descricao = $modulo;
                        $newmodulo->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Users->Usermodulos->Modulos->save($newmodulo)) {
                            $modulousuario = $this->Users->Usermodulos->newEntity();
                            $modulousuario->modulo_id = $newmodulo->id;
                            $usermodulos[] = $modulousuario;
                        }
                    }
                }
            }

            $user->usermodulos = $usermodulos;



            $setores = array();
            if ($user->setor_ids) {
                foreach ($user->setor_ids as $setor) {
                    if (is_numeric($setor)) {// se o usuário selecionou um modulo já existente
                        $setorusuario = $this->Users->Usersareaservicos->newEntity();
                        $setorusuario->empresa_id = $user->empresa_id;
                        $setorusuario->areaservico_id = $setor;
                        $setorusuario->admin_setor = $user->admin_setor;
                        $setorusuario->dt_cadastro = date('Y-m-d H:i:s');
                        $setores[] = $setorusuario;
                    } else if (is_string($setor)) {// se o usuário digitou um modulo para ser criado
                        $newsetor = $this->Users->Usersareaservicos->Areaservicos->newEntity();
                        $newsetor->empresa_id = $user->empresa_id;
                        $newsetor->descricao = $setor;
                        $newsetor->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Users->Usersareaservicos->Areaservicos->save($newsetor)) {
                            $setorusuario = $this->Users->Usersareaservicos->newEntity();
                            $setorusuario->empresa_id = $user->empresa_id;
                            $setorusuario->areaservico_id = $newsetor->id;
                            $setorusuario->admin_setor = $user->admin_setor;
                            $setorusuario->dt_cadastro = date('Y-m-d H:i:s');
                            $setores[] = $setorusuario;
                        }
                    }
                }
            }

            $user->usersareaservicos = $setores;

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Cadastrado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Não foi possível cadastrar. Verifique se os campos foram corretamente preenchidos.'));
            }
        }
        $empresas = $this->Users->Empresas->find('list');
        $tipousuarios = $this->Users->Tipousuarios->find('list', ['limit' => 200]);
        $modulos = $this->Users->Usermodulos->Modulos->find('list');
        $areaservicos = $this->Users->Usersareaservicos->Areaservicos->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 ']);
        $this->set(compact('user', 'tipousuarios', 'empresas', 'modulos', 'areaservicos'));
        $this->set('_serialize', ['user']);
    }

    public function login() {

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->registraLogin($this->Auth->user('id'));

                if ($this->Auth->user('tipousuario_id') == $this->_ADMIN) {
                    $this->request->session()->write('Auth.User.admin', true);
                }

                $this->request->session()->write('Auth.User.hasSetor', false);
                if ($this->Auth->user('usersareaservicos')) {
                    $this->storeSessionSetores($this->Auth->user('usersareaservicos'));
                    $this->request->session()->write('Auth.User.hasSetor', true);
                }

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Usuário e/ou Senha inválido(s)'));
        }
        $user = $this->Users->newEntity();
        $this->set('user', $user);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);

            if (!$this->Auth->user('admin')) {
                if ($user->empresa_id != $this->Auth->user('empresa_id') || ($user->empresa_id == $this->Auth->user('empresa_id') && !$this->Auth->user('admin_empresa'))) {
                    $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                    return $this->redirect(['action' => 'index']);
                }
            }

            $moduloscadastrados = $this->Users->Usermodulos->find()->where(['user_id' => $id])->extract('modulo_id')->filter()->toArray();
            $setorescadastrados = $this->Users->Usersareaservicos->find()->where(['user_id' => $id])->extract('areaservico_id')->filter()->toArray();

            $usermodulos = array();
            if ($user->modulo_ids) {
                foreach ($user->modulo_ids as $modulo) {
                    if (!in_array($modulo, $moduloscadastrados)) {
                        if (is_numeric($modulo)) {// se o usuário selecionou um modulo já existente
                            $modulousuario = $this->Users->Usermodulos->newEntity();
                            $modulousuario->modulo_id = $modulo;
                            $usermodulos[] = $modulousuario;
                        } else if (is_string($modulo)) {// se o usuário digitou um modulo para ser criado
                            $newmodulo = $this->Users->Usermodulos->Modulos->newEntity();
                            $newmodulo->nome = $modulo;
                            $newmodulo->descricao = $modulo;
                            $newmodulo->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Users->Usermodulos->Modulos->save($newmodulo)) {
                                $modulousuario = $this->Users->Usermodulos->newEntity();
                                $modulousuario->modulo_id = $newmodulo->id;
                                $usermodulos[] = $modulousuario;
                            }
                        }
                    }
                }
                $this->Users->Usermodulos->deleteAll(['user_id' => $id, 'modulo_id NOT IN' => $user->modulo_ids]);
            } else {
                $this->Users->Usermodulos->deleteAll(['user_id' => $id]);
            }

            $user->usermodulos = $usermodulos;


            $setores = array();
            if ($user->setor_ids) {
                foreach ($user->setor_ids as $setor) {
                    if (!in_array($setor, $setorescadastrados)) {
                        if (is_numeric($setor)) {// se o usuário selecionou um modulo já existente
                            $setorusuario = $this->Users->Usersareaservicos->newEntity();
                            $setorusuario->empresa_id = $user->empresa_id;
                            $setorusuario->areaservico_id = $setor;
                            $setorusuario->admin_setor = $user->admin_setor;
                            $setorusuario->dt_cadastro = date('Y-m-d H:i:s');
                            $setores[] = $setorusuario;
                        } else if (is_string($setor)) {// se o usuário digitou um modulo para ser criado
                            $newsetor = $this->Users->Usersareaservicos->Areaservicos->newEntity();
                            $newsetor->empresa_id = $user->empresa_id;
                            $newsetor->descricao = $setor;
                            $newsetor->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Users->Usersareaservicos->Areaservicos->save($newsetor)) {
                                $setorusuario = $this->Users->Usersareaservicos->newEntity();
                                $setorusuario->empresa_id = $user->empresa_id;
                                $setorusuario->areaservico_id = $newsetor->id;
                                $setorusuario->admin_setor = $user->admin_setor;
                                $setorusuario->dt_cadastro = date('Y-m-d H:i:s');
                                $setores[] = $setorusuario;
                            }
                        }
                    }
                }
                $this->Users->Usersareaservicos->deleteAll(['user_id' => $id, 'areaservico_id NOT IN' => $user->setor_ids]);
            } else {
                $this->Users->Usersareaservicos->deleteAll(['user_id' => $id]);
            }

            $user->usersareaservicos = $setores;



            if ($this->Users->save($user)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('Modulos');
        $selectedmodulos = array_keys($this->Modulos->find('list')->innerJoinWith('Usermodulos')->where(['Usermodulos.user_id' => $id])->toArray());
        $this->loadModel('Areaservicos');
        $selectedsetores = array_keys($this->Areaservicos->find('list')->innerJoinWith('Usersareaservicos')->where(['Usersareaservicos.user_id' => $id])->toArray());
        $empresas = $this->Users->Empresas->find('list');
        $tipousuarios = $this->Users->Tipousuarios->find('list', ['limit' => 200]);
        $modulos = $this->Users->Usermodulos->Modulos->find('list');
        $areaservicos = $this->Users->Usersareaservicos->Areaservicos->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 ']);
        $this->set(compact('user', 'tipousuarios', 'empresas', 'modulos', 'areaservicos', 'selectedmodulos', 'selectedsetores'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if (($user->empresa_id == $this->Auth->user('empresa_id') && $this->Auth->user('admin_empresa')) || $this->Auth->user('admin')) {
            if ($this->Users->delete($user)) {
                $this->Flash->success(__('Deletado com sucesso'));
            } else {
                $this->Flash->error(__('Não foi possível deletar, tente novamente.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'login', 'logout', 'view']);
    }

    public function registraLogin($userid) {
        $user = $this->Users->get($userid);
        $user->last_login = date('Y-m-d H:i:s');
        $this->Users->save($user);
    }

    public function storeSessionSetores($setores) {
        $admin_setores = array();
        $todos_setores = array();
        $this->request->session()->write('Auth.User.admin_setores', false);
        $this->request->session()->write('Auth.User.todos_setores', false);
        foreach ($setores as $setor) {
            if ($setor['admin_setor']) {
                $admin_setores[] = $setor['areaservico_id'];
            }
            $todos_setores[] = $setor['areaservico_id'];
        }
        $this->request->session()->write('Auth.User.admin_setores', $admin_setores);
        $this->request->session()->write('Auth.User.todos_setores', $todos_setores);
    }

//    public function registraModulos($modulos) {
//    }
}
