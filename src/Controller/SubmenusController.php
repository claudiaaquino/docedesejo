<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Submenus Controller
 *
 * @property \App\Model\Table\SubmenusTable $Submenus
 */
class SubmenusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $submenus = $this->paginate($this->Submenus);

        $this->set(compact('submenus'));
        $this->set('_serialize', ['submenus']);
    }

    /**
     * View method
     *
     * @param string|null $id Submenu id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $submenu = $this->Submenus->get($id, [
            'contain' => ['Menusubmenus.Menus']
        ]);

        $this->set('submenu', $submenu);
        $this->set('_serialize', ['submenu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $submenu = $this->Submenus->newEntity();
        if ($this->request->is('post')) {
            $submenu = $this->Submenus->patchEntity($submenu, $this->request->data);
            $submenu->dt_cadastro =  date('Y-m-d H:i:s');
            $submenu->user_id =  $this->Auth->user('id');
            $submenu->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Submenus->save($submenu)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('submenu'));
        $this->set('_serialize', ['submenu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Submenu id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $submenu = $this->Submenus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $submenu = $this->Submenus->patchEntity($submenu, $this->request->data);
            if ($this->Submenus->save($submenu)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('submenu'));
        $this->set('_serialize', ['submenu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Submenu id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $submenu = $this->Submenus->get($id);
        if ($this->Submenus->delete($submenu)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
