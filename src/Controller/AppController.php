<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $paginate = [
        'limit' => 10
    ];
    public $_ADMIN = 1;
    public $_EMPRESA = 2;
    public $_CLIENTE = 3;
    public $_ID_EMPRESA_ADMIN = 1;
 
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'App',
                'action' => 'redirectfirstView'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'finder' => 'auth'
                ]
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            [
                'authorize' => 'Controller',
            ],
            'authError' => '',
//            'authError' => 'Você não tem permissão para acessar esse conteúdo.',
            'storage' => 'Session'
        ]);
    }

    public function convertDateBRtoEN($date) {
        return $date ? implode("-", array_reverse(explode("/", $date))) : null;
    }

    public function convertDateENtoBR($date) {
        return $date ? implode("/", array_reverse(explode("-", $date))) : '';
    }

    public function saveAttachment($file) {
        $setNewFileName = time() . "_" . rand(000000, 999999);
        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
        move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/' . $setNewFileName . '.' . $ext);
        return $setNewFileName . '.' . $ext;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if ($user_id = $this->request->session()->read('Auth.User.id')) {
            if ($this->request->is('ajax') || $this->viewBuilder()->layout() == 'ajax') {
                $this->viewBuilder()->layout('ajax');
            } else { // se vier de frame, deverá ser setado esse valor na própria action
                $this->viewBuilder()->layout('index');
                $menus = TableRegistry::get('Menus');
                $this->set("menus", $menus->find('menusOfUsers', ['user_id' => $user_id]));

                if (!$foto = $this->request->session()->read('Auth.User.foto')) {
                    $foto = 'default.png';
                }

                $this->set("foto", $foto);
                $name = $this->request->session()->read('Auth.User.nome');
                $this->set("loggednome", $name);

                $loggedfirstname = explode(' ', $name);
                $this->set("loggedfirstname", $loggedfirstname[0]);
            }

            $this->set("backlink", $this->referer());
            $this->set("admin", $this->request->session()->read('Auth.User.admin'));
            $this->set("loggeduser", $this->request->session()->read('Auth.User.id'));
            $this->set("loggedIn", true);
        } else {
            $this->set("loggedIn", false);
        }
    }

    public function redirectfirstView() {

        $tipousuario_id = $this->Auth->user('tipousuario_id');
        $this->request->session()->write('Auth.User.admin', false);

        if ($tipousuario_id == $this->_ADMIN) {//ADMIN-triade
            $this->request->session()->write('Auth.User.admin', true);
            return $this->redirect(['controller' => 'movimentacaobancarias', 'action' => 'add']);
        } else if ($tipousuario_id == $this->_CLIENTE) {//CLIENTES
            return $this->redirect(['controller' => 'Empresas', 'action' => 'view']);
        }
    }

}
