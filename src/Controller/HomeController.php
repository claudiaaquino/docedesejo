<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Sexos Controller
 *
 */
class HomeController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        if ($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) {
            return $this->redirect(['controller' => 'Movimentacaobancarias', 'action' => 'add']);
        } else {
            return $this->redirect(['controller' => 'Users', 'action' => 'view']);
        }
    }

}
