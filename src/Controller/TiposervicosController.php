<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tiposervicos Controller
 *
 * @property \App\Model\Table\TiposervicosTable $Tiposervicos
 */
class TiposervicosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Gruposervicos', 'Users']
        ];
        $tiposervicos = $this->paginate($this->Tiposervicos);

        $this->set(compact('tiposervicos'));
        $this->set('_serialize', ['tiposervicos']);
    }

    /**
     * ajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax($gruposervico_id = null) {
        $retorno = $this->Tiposervicos->find('list')->where(['gruposervico_id' => $gruposervico_id])->order(['descricao' => 'ASC']);
        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
    }

    /**
     * View method
     *
     * @param string|null $id Tiposervico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tiposervico = $this->Tiposervicos->get($id, [
            'contain' => ['Gruposervicos', 'Users', 'Tarefas']
        ]);

        $this->set('tiposervico', $tiposervico);
        $this->set('_serialize', ['tiposervico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tiposervico = $this->Tiposervicos->newEntity();
        if ($this->request->is('post')) {
            $tiposervico = $this->Tiposervicos->patchEntity($tiposervico, $this->request->data);
            $tiposervico->dt_cadastro = date('Y-m-d H:i:s');
            $tiposervico->user_id = $this->Auth->user('id');
            $tiposervico->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Tiposervicos->save($tiposervico)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $gruposervicos = $this->Tiposervicos->Gruposervicos->find('list', ['limit' => 200]);
        $users = $this->Tiposervicos->Users->find('list', ['limit' => 200]);
        $this->set(compact('tiposervico', 'gruposervicos', 'users'));
        $this->set('_serialize', ['tiposervico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tiposervico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tiposervico = $this->Tiposervicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tiposervico = $this->Tiposervicos->patchEntity($tiposervico, $this->request->data);
            if ($this->Tiposervicos->save($tiposervico)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $gruposervicos = $this->Tiposervicos->Gruposervicos->find('list', ['limit' => 200]);
        $users = $this->Tiposervicos->Users->find('list', ['limit' => 200]);
        $this->set(compact('tiposervico', 'gruposervicos', 'users'));
        $this->set('_serialize', ['tiposervico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tiposervico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tiposervico = $this->Tiposervicos->get($id);
        if ($this->Tiposervicos->delete($tiposervico)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
