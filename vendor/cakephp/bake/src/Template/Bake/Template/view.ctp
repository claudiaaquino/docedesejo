<%

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'];
$associationFields = collection($fields)
        ->map(function($field) use ($immediateAssociations) {
            foreach ($immediateAssociations as $alias => $details) {
                if ($field === $details['foreignKey']) {
                    return [$field => $details];
                }
            }
        })
        ->filter()
        ->reduce(function($fields, $value) {
    return $fields + $value;
}, []);

$groupedFields = collection($fields)
        ->filter(function($field) use ($schema) {
            return $schema->columnType($field) !== 'binary';
        })
        ->groupBy(function($field) use ($schema, $associationFields) {
            $type = $schema->columnType($field);
            if (isset($associationFields[$field])) {
                return 'string';
            }
            if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
                return 'number';
            }
            if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
                return 'date';
            }
            return in_array($type, ['text', 'boolean']) ? $type : 'string';
        })
        ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('<%= $pluralHumanName %>') ?> - <?= h($<%= $singularVar %>-><%= $displayField %>) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <% if ($groupedFields['string']) : %>
                            <% foreach ($groupedFields['string'] as $field) : %>
                                <%
                                if (isset($associationFields[$field])) :
                                    $details = $associationFields[$field];
                                    %>
                        <p class="title"><?= __('<%= Inflector::humanize($details['property']) %>') ?></p>
                                    <p><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : 'Não Informado' ?></p>

                                <% else : %>
                        <p class="title"><?= __('<%= Inflector::humanize($field) %>') ?></p>
                                    <p><?= $<%= $singularVar %>-><%= $field %> ? $<%= $singularVar %>-><%= $field %> : 'Não Informado'; ?></p>

                                <% endif; %>
                            <% endforeach; %>
                        <% endif; %>
                        <% if ($associations['HasOne']) : %>
                            <% foreach ($associations['HasOne'] as $alias => $details) : %>
                        <p class="title"><?= __('<%= Inflector::humanize(Inflector::singularize(Inflector::underscore($alias))) %>') ?></p>
                                <p><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : 'Não Informado' ?></p>

                            <% endforeach; %>
                        <% endif; %>
                        <% if ($groupedFields['number']) : %>
                            <% foreach ($groupedFields['number'] as $field) : %>
                        
                        <p class="title"><?= __('<%= Inflector::humanize($field) %>') ?></p>
                                <p><?= $<%= $singularVar %>-><%= $field %> ? $this->Number->format($<%= $singularVar %>-><%= $field %>) : 'Não Informado'; ?></p>

                            <% endforeach; %>
                        <% endif; %>
                        <% if ($groupedFields['date']) : %>
                            <% foreach ($groupedFields['date'] as $field) : %>
                        
                        <p class="title"><%= "<%= __('" . Inflector::humanize($field) . "') %>" %></p>
                                <p><?= $<%= $singularVar %>-><%= $field %> ? $<%= $singularVar %>-><%= $field %> : 'Não Informado'; ?></p>

                            <% endforeach; %>
                        <% endif; %>
                        <% if ($groupedFields['boolean']) : %>
                            <% foreach ($groupedFields['boolean'] as $field) : %>
                        
                        <p class="title"><?= __('<%= Inflector::humanize($field) %>') ?></p>
                                <p><?= $<%= $singularVar %>-><%= $field %> ? __('Ativo') : __('Desativado'); ?></p>

                            <% endforeach; %>
                        <% endif; %>
                        <% if ($groupedFields['text']) : %>
                            <% foreach ($groupedFields['text'] as $field) : %>
                        <p class="title"><?= __('<%= Inflector::humanize($field) %>') ?></p>
                                <p>  <?= $<%= $singularVar %>-><%= $field %> ? $this->Text->autoParagraph(h($<%= $singularVar %>-><%= $field %>)) : 'Não Informado'; ?></p>
                            <% endforeach; %>
                        <% endif; %>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', <%=$pk %>], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', <%=$pk %>], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', <%= $pk %>)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <%
        $relations = $associations['HasMany'] + $associations['BelongsToMany'];
        foreach ($relations as $alias => $details):
            $otherSingularVar = Inflector::variable($alias);
            $otherPluralHumanName = Inflector::humanize(Inflector::underscore($details['controller']));
            %>
            <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> <%= $otherPluralHumanName %> Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <% foreach ($details['fields'] as $field): %>
                                <th scope="col"  class="column-title"><?= __('<%= Inflector::humanize($field) %>') ?></th>
                                    <% endforeach; %>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($<%= $singularVar
                                %>-><%= $details['property'] %> as $<%= $otherSingularVar %>):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <% foreach ($details['fields'] as $field): %>
                                <td><?= h($<%= $otherSingularVar %>-><%= $field %>) ?></td>
                                    <% endforeach; %>
                                    <% $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>                  
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => '<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', <%= $otherPk %>)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <% endforeach; %>
    </div>
</div>


