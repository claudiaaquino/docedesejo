$(document).ready(function () {
    reloadTarefas();
    setInterval(reloadTarefas, 3600000);

});

function reloadTarefas() {
    $.ajax({
        url: $('#urlroot').val() + 'tarefas/upcomingtasksajax/',
        type: 'GET',
        success: function (data) {
            $('#box-tarefas').html(data);
        },
        error: function (a) {
            console.log(a);
        }
    });
}

function clearFieldsInfo(clearFields) {
    $.each(clearFields, function (index, value) {
        $('#' + value).val('');
    });
}

function clearCheckedRadios(clearFields) {
    $.each(clearFields, function (index, value) {
        $('#' + value).prop('checked', null);
        $('#' + value).parent().removeClass('checked');
    });
}

function clearSelectOptions(clearFields) {
    $.each(clearFields, function (index, value) {
        $('#' + value).html('');
    });
}

function refreshSelect2(clearFields) {
    $.each(clearFields, function (index, value) {
        if ($('#' + value).hasClass('select2')) {
            $('#' + value).select2("destroy");
            $('#' + value).select2({minimumResultsForSearch: 6});
        }
    });
}