$(document).ready(function () {
    loadTasks();
});


var save = function (calendar, started, end, allDay) {

    if (validateTaskFields()) {
        $('form').submit();
    } else {
        alert('Preencha todos os campos obrigatórios, por favor.');
        return false;
    }
};
var loadTasks = function () {

    var listTarefas = Array();
    var tarefas;

    $.ajax({
        url: $('#urlroot').val() + 'tarefas/load/',
        data: "",
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.tarefas) {
                var arrayTarefas = data.tarefas;
                arrayTarefas.forEach(function (tarefa) {
                    var dataInicio = fullDateToCalendarDate(tarefa.dt_inicio);
                    var dataFim = tarefa.dt_final ? fullDateToCalendarDate(tarefa.dt_final) : "";
                    var allday = true;
                    if (dataInicio.getHours() != '0') {
                        allday = false;
                    }
                    tarefas = {
                        id: tarefa.id,
                        title: tarefa.titulo,
                        start: dataInicio,
                        end: dataFim,
                        dtinicio: fullDateToshortBrDate(tarefa.dt_inicio),
                        dtfinal: fullDateToshortBrDate(tarefa.dt_final),
                        allDay: allday,
                        hr_tarefa: dataInicio.getHours(),
                        tipotarefa: tarefa.tarefatipo ? tarefa.tarefatipo.descricao : null,
                        descricao: tarefa.descricao,
                        prioridade: tarefa.tarefaprioridade ? tarefa.tarefaprioridade.descricao : null,
                        dt_final: tarefa.dt_final
                    };
                    listTarefas.push(tarefas);
                });
            }
            $('#calendar').fullCalendar('addEventSource', listTarefas);
        },
        error: function (a) {
            console.log(a);
        }
    });
};
var fullDateToCalendarDate = function (fulldate) {
    fulldate = fulldate.replace("T", "-");
    var arrayDate = fulldate.split("-");
    var startYear = arrayDate[0];
    var startMonth = arrayDate[1] - 1;
    var startDay = parseInt(arrayDate[2]);
    var arrayTime = arrayDate[3].split(":");
    var startHour = arrayTime[0];
    var startMinutes = arrayTime[1];
    return  new Date(startYear, startMonth, startDay, startHour, startMinutes);
};
var fullDateToshortBrDate = function (fulldate) {
    var date = fulldate.split("T");
    date = date[0].split("-");
    return  date[2] + '/' + date[1] + '/' + date[0];
};
var calendarDateToSimpleBrDate = function (date) {
    return  (date.getUTCDate() < 10 ? '0' + date.getUTCDate() : date.getUTCDate())
            + '/' + (date.getUTCMonth() < 9 ? '0' + (date.getUTCMonth() + 1) : (date.getUTCMonth() + 1))
            + '/' + date.getUTCFullYear();
};


var calendar = $('#calendar').fullCalendar({
    contentHeight: 'auto',
    lang: 'pt-br',
    timezone: 'America/Sao_Paulo',
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
    },
    selectable: true,
    selectHelper: true,
    select: function (start, end, allDay) {
        $('#fc_create').click();
        var currentdate = calendarDateToSimpleBrDate(start._d);
        console.log(currentdate);
        $('#dt-inicio').val(currentdate);
        $('#dt-final').val(currentdate);
        $('#dt-lembrete').val(currentdate);

        var utchours = start._d.getUTCHours();
        var utcminutes = start._d.getUTCMinutes();
        if (utchours != '0') {
            var hora = utchours < 10 ? '0' + utchours : utchours;
            var minutos = utcminutes < 10 ? '0' + utcminutes : utcminutes;
            $("#hr-tarefa").val(hora + ':' + minutos);
        }

        $('#btnSave').on('click', function () {
            save(calendar, start, end, allDay);
        });
    },
    eventClick: function (Tarefa, jsEvent, view) {
        $('#fc_view').click();
        $('#tituloField').html(Tarefa.title);
        var hora = '';
        if (Tarefa.hr_tarefa != '0') {
            hora = ' às ' + Tarefa.hr_tarefa + ' horas';
        }
        $('#dtinicioField').html(Tarefa.dtinicio + hora);
        $('#dtfinalField').html(Tarefa.dtfinal);
        $('#prioridadeField').html(Tarefa.prioridade);
        $('#descricaoField').html(Tarefa.descricao);
        $('#tipotarefaField').html(Tarefa.tipotarefa);
        console.log(Tarefa);

        $('#CalenderModalView').find(".btnEdit").on("click", function () {
            location.href = $('#urlroot').val() + "tarefas/edit/" + Tarefa.id;
        });

        $('#CalenderModalView').find(".btnView").on("click", function () {
            location.href = $('#urlroot').val() + "tarefas/view/" + Tarefa.id;
        });

        $('#CalenderModalView').find(".btnRemove").on('click', function () {
            if (confirm("Deseja mesmo remover essa tarefa?")) {
                remove(Tarefa, calendar);
            }
        });

        calendar.fullCalendar('unselect');

        /*
         //        calEvent.id
         //        calEvent.tipotarefa_id
         //        calEvent.prioridade
         //        calEvent.descricao
         
         if (calEvent.dt_final) {
         var dataFim = calEvent.dt_final;
         
         dataFim = dataFim.replace("T", "-");
         var arrayDataFim = dataFim.split("-");
         
         arrayDataFim.splice(3, 1);
         var date = arrayDataFim.reverse().join("/");
         $(".formUpdate").find("#dt-final").val(date);
         }
         
         if (calEvent.dt_lembrete) {
         var dataLembrete = calEvent.dt_lembrete;
         
         dataLembrete = dataLembrete.replace("T", "-");
         var arrayDataLembrete = dataLembrete.split("-");
         
         arrayDataLembrete.splice(3, 1);
         var date = arrayDataLembrete.reverse().join("/");
         $(".formUpdate").find("#dt-lembrete").val(date);
         }
         
         if (calEvent.permite_deletar === false) {
         $('#CalenderModalEdit').find(".btnRemove").hide();
         } else {
         $('#CalenderModalEdit').find(".btnRemove").show();
         }
         
         if (calEvent.file !== "") {
         var url = $('#urlroot').val();
         $(".formUpdate").find(".fileTarefas").show();
         $(".formUpdate").find(".urlFile").attr('href', url + "docs/" + calEvent.id).html(calEvent.file);
         } else {
         $(".formUpdate").find(".fileTarefas").hide();
         }
         
         categoryClass = $("#event_type").val();*/

    },
    allDayDefault: true,
    editable: false
});

var remove = function (event, calendar) {

    $.ajax({
        url: $('#urlroot').val() + 'tarefas/deleteajax/',
        data: {
            id: event.id
        },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                calendar.fullCalendar('removeEvents', event.id);
                $("#CalenderModalView").find(".antoclose2").trigger("click");
            } else {
                alert('Erro ao remover. Por favor, tente novamente.');
            }
        },
        error: function (a) {
            alert('Erro ao remover. Por favor, tente novamente.');
        }
    });
};

var validateTaskFields = function () {
    if (!$('#tarefatipo-id').val() || !$('#titulo').val()) {
        return false;
    }
    return true;
};

/*
 var save = function (calendar, started, end, allDay) {
 if ($(".formNew").find('#titulo').val() !== "") {
 
 $(".formNew").find('#dt-inicio').val(started._d);
 
 $.ajax({
 url: $('#urlroot').val() + 'tarefas/addAjax/',
 data: $(".formNew").serialize(),
 type: 'POST',
 dataType: 'json',
 
 success: function (data) {
 if (data.success) {
 var title = $(".formNew").find('#titulo').val();
 var tarefa = data.tarefa;
 
 
 var objDate = "";
 if ($(".formNew").find('#dt-final').val() !== "") {
 var dataFinal = $(".formNew").find('#dt-final').val();
 
 var arrData = dataFinal.split("/");
 var day = arrData[0];
 var month = arrData[1] - 1;
 var year = arrData[2];
 
 objDate = new Date(year, month, day);
 }
 
 var hasFile = false;
 var ext = "";
 
 if ($(".formNew").find("#doc_upload").val()) {
 uploadFile('formNew', data.tarefa.id);
 hasFile = true;
 var fileName = $(".formNew").find("#doc_upload").val();
 fileName = fileName.split(".");
 ext = fileName[fileName.length - 1];
 }
 
 if (title) {
 calendar.fullCalendar('renderEvent', {
 title: title,
 start: started,
 end: objDate,
 allDay: allDay,
 id: data.tarefa.id,
 tipotarefa: tarefa.tarefatipo_id,
 areaservico: tarefa.areaservico_id,
 descricao: tarefa.descricao,
 prioridade: tarefa.tarefaprioridade_id,
 dt_final: tarefa.dt_final,
 dt_lembrete: tarefa.dt_lembrete,
 permite_deletar: tarefa.permite_deletar,
 file: hasFile ? $(".formNew").find("#doc_nome").val() : "",
 ext: hasFile ? ext : ""
 }, true); //make the event stick
 }
 
 clear();
 calendar.fullCalendar('unselect');
 $('.antoclose').click();
 } else {
 alert('Erro ao salvar. Confira os campos e tente novamente.');
 }
 
 return false;
 },
 error: function (a) {
 alert('Preencha os campos obrigatórios e tente novamente.');
 }
 });
 } else {
 alert('Preencha os campos obrigatórios e tente novamente.');
 }
 };
 
 
 */