var tablesocios = null;
$(document).ready(function () {
    $('#empresa-id').change(function () {
        if ($("#funcionario-id").length && ($("#user-ids").length || $("#sollaendern").length)) {
            preencheFuncionarios($(this).val());
        }
        if ($("#areaservico-id").length) {
            preencheSetoresEmpresa($(this).val());
        }
        if ($("#user-id").length || $("#user-ids").length) {
            preencheUsuariosEmpresa($(this).val(), null);
        }
    });
    $('#areaservico-id').change(function () {
        if ($("#user-id").length || $("#user-ids").length) {
            preencheUsuariosEmpresa($('#empresa-id').val(), $(this).val());
        }

    });
});
function preencheFuncionarios(empresa) {
    $.ajax({
        url: $('#urlroot').val() + 'funcionarios/listajax/' + empresa,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='null'> Selecione (se necessário)</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#funcionario-id').html(html);

            if ($('#funcionario-id').hasClass('select2')) {
                $("#funcionario-id").select2("destroy");
                $("#funcionario-id").select2({placeholder: 'selecione (se necessário)', minimumResultsForSearch: 5});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function preencheSetoresEmpresa(empresa, areaservico_id) {
    $.ajax({
        url: $('#urlroot').val() + 'empresas/setoresajax/' + empresa,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='null'> Selecione (se necessário) </option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#areaservico-id').html(html);

            if (areaservico_id) {
                alert(areaservico_id);
                $('#areaservico-id').val(areaservico_id);
            }

            if ($('#areaservico-id').hasClass('select2')) {
                $("#areaservico-id").select2("destroy");
                $("#areaservico-id").select2({minimumResultsForSearch: 2});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function preencheUsuariosEmpresa(empresa, areaservico, selected_user) {

    $.ajax({
        url: $('#urlroot').val() + 'empresas/usuariosajax/',
        data: '&empresa_id=' + empresa + "&areaservico_id=" + areaservico + "&user_id=" + selected_user,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });

            var element = $("#user-ids").length ? "user-ids" : "user-id";
            $('#' + element).html(html);

            if (selected_user) {
                $('#' + element).val(selected_user);
            }

            if ($('#' + element).hasClass('select2')) {
                $('#' + element).select2("destroy");
                $('#' + element).select2();
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}

/*
 function loadTabSocios() {
 if (tablesocios === null) {// se é a primeira vez que clica na aba, então carrega o objeto, se não, ele apenas dá reload nos dados             
 tablesocios = $('#datatable-responsive').DataTable({
 ajax: {
 url: $('#urlroot').val() + 'empresas/sociosajax/' + $('#empresa-id').val(),
 dataSrc: 'socios'
 },
 columns: [
 {data: 'id'}, {data: 'nome'}, {data: 'cpf'}, {data: 'percentual_capitalsocial'}, {data: 'admin'},
 {defaultContent: "<span class='btn btn-primary btn-small fa fa-eye view'>  Visualizar</span><span class='btn btn-info btn-small fa fa-edit edit' onclick=\"redirectEditSocio($(this).parents('tr'));\">  Editar</span><span class='btn btn-danger btn-small fa fa-trash remove' onclick=\"deleteSocio($(this).parents('tr'));\">  Remover</span>"},
 {data: 'dt_nascimento'},
 {data: 'cargo.descricao', defaultContent: " "}, {data: 'email'}, {data: 'telefone_residencial'}, {data: 'telefone_celular'},
 {data: 'endereco'}, {data: 'end_numero'}, {data: 'end_complemento'}, {data: 'end_bairro'}, {data: 'end_cep'},
 {data: 'estado.estado_sigla', defaultContent: " "}, {data: 'cidade.cidade_nome', defaultContent: " "}, {data: 'estadocivil.descricao', defaultContent: " "},
 {data: 'comunhaoregime.descricao', defaultContent: " "}, {data: 'sexo.descricao', defaultContent: " "}, {data: 'ctps'}, {data: 'ctps_serie'}, {data: 'cnh'},
 {data: 'cnh_dt_habilitacao'}, {data: 'cnh_dt_vencimento'}, {data: 'rg'}, {data: 'rg_estado'}, {data: 'rg_expedidor'},
 {data: 'rg_dt_expedicao'}, {data: 'militar_numero'}, {data: 'militar_expedidor'}, {data: 'militar_serie'},
 {data: 'eleitor_numero'}, {data: 'eleitor_zona'}, {data: 'eleitor_secao'}, {data: 'eleitor_dt_emissao'},
 {data: 'pai_nome'}, {data: 'mae_nome'}, {data: 'dt_cadastro'}
 ],
 responsive: {
 details: {
 type: 'column',
 target: '.view'
 }
 },
 select: {
 'style': 'multi',
 'selector': 'td:not(.control)'
 },
 paging: false
 });
 } else {
 reloadSocios();
 }
 
 }
 function reloadSocios() {
 tablesocios.ajax.reload();
 }
 function redirectEditSocio(tr_table) {
 location.href = $('#urlroot').val() + 'empresasocios/edit/' + $('td:first', tr_table).text();
 }
 function deleteSocio(tr_table) {
 
 if (confirm("Tem certeza que deseja remover esse sócio da empresa?")) {
 $.ajax({
 url: $('#urlroot').val() + 'empresasocios/deleteajax/' + $('td:first', tr_table).text(),
 type: 'POST',
 dataType: 'json',
 success: function (data) {
 reloadSocios();
 },
 error: function (a) {
 //                alert('teste deu erro ao carregar os sócios, pede p cau olhar isso');
 console.log(a);
 }
 });
 }
 
 }
 */