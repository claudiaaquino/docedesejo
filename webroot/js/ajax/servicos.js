$(document).ready(function () {
    $('#areaservico-id').change(function () {
        if($('#gruposervico-id').length){
            preencheGrupoServicos($(this).val());
            $('#gruposervico-id').show();                 
        }
    });

    $('#gruposervico-id').change(function () {
        preencheTipoServicos($(this).val());
        if($('.document-details').length){
            $('.document-details').show();                 
        }
    });

});
function preencheGrupoServicos(area) {
    $.ajax({
        url: $('#urlroot').val() + 'gruposervicos/ajax/' + area,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='' selected='selected'> Selecione um serviço</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#gruposervico-id').html(html);

            if ($('#gruposervico-id').hasClass('select2')) {
                $("#gruposervico-id").select2("destroy");
                $("#gruposervico-id").select2({minimumResultsForSearch: 5});
            }
            if ($('#tiposervico-id').hasClass('select2')) {
                $('#tiposervico-id').html('');
                $("#tiposervico-id").select2("destroy");
                $("#tiposervico-id").select2({minimumResultsForSearch: 5});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function preencheTipoServicos(grupo) {
    $.ajax({
        url: $('#urlroot').val() + 'tiposervicos/ajax/' + grupo,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value=''> Selecione um tipo</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#tiposervico-id').html(html);

            if ($('#tiposervico-id').hasClass('select2')) {
                $("#tiposervico-id").select2("destroy");
                $("#tiposervico-id").select2({minimumResultsForSearch: 5});
            }
            

        },
        error: function (a) {
            console.log(a);
        }
    });
}
