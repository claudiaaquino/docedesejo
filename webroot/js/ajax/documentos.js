$(document).ready(function () {

    $('#setor-id').change(function () {
        preencheGrupoDocumentos($(this).val());
        if ($('.grupodocumento-id').length) {
            $('.grupodocumento-id').show();
        }
        if ($('.document-details').length) {
            $('.document-details').hide();
            $('.btn-addFile').hide();
        }
    });

    $('#areaservico-id').change(function () {
        if (!$("#setor-id").length) {// quando em um mesmo formulário tiver os setores em relação à empresa e à documentos tbm
            preencheGrupoDocumentos($(this).val());
        }
    });

    $('#grupodocumento-id').change(function () {
        preencheTipoDocumentos($(this).val());
        if ($('.document-details').length) {
            $('.document-details').show();
            $('.btn-addFile').show();
        }
    });

});
function preencheGrupoDocumentos(area) {
    $.ajax({
        url: $('#urlroot').val() + 'grupodocumentos/ajax/' + area,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='' selected='selected'> Selecione um grupo</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#grupodocumento-id').html(html);

            if ($('#grupodocumento-id').hasClass('select2')) {
                $("#grupodocumento-id").select2("destroy");
                $("#grupodocumento-id").select2({minimumResultsForSearch: 5});
            }
        },
        error: function (a) {
//            alert('teste deu erro ao carregar o grupo de documentos, pede p cau olhar isso');
            console.log(a);
        }
    });
}
function preencheTipoDocumentos(grupo) {
    $.ajax({
        url: $('#urlroot').val() + 'tipodocumentos/ajax/' + grupo,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value=''> Selecione um tipo</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#tipodocumento-id').html(html);

            if ($('#tipodocumento-id').hasClass('select2')) {
                $("#tipodocumento-id").select2("destroy");
                $("#tipodocumento-id").select2({minimumResultsForSearch: 5});
            }

        },
        error: function (a) {
//            alert('teste deu erro ao carregar o tipo de documentos, pede p cau olhar isso');
            console.log(a);
        }
    });
}

function sendDocument() {
    $('#file_upload').uploadify('upload', '*');
}


function loadDocTable() {
    var empresa = $('#empresa-id').val() ? 'empresa_id=' + $('#empresa-id').val() : '';
    var tipodocumento = $('#tipodocumento-id').val() ? '&tipodocumento_id=' + $('#tipodocumento-id').val() : '';
    var grupodocumento = $('#grupodocumento-id').val() ? '&grupodocumento_id=' + $('#grupodocumento-id').val() : '';
    var areaservico = $('#areaservico-id').val() ? '&areaservico_id=' + $('#areaservico-id').val() : '';

    $.ajax({
        url: $('#urlroot').val() + 'documentos/loadajax/',
        data: empresa + tipodocumento + grupodocumento + areaservico,
        type: 'GET',
        success: function (data) {
            $('#table-documentos').html(data);
        },
        error: function (a) {
//            alert('teste deu erro ao carregar a tabela de documentos.. pede p cau verificar isso');
            console.log(a);
        }
    });
}

/*
 
 function orderTable(sort) {
 var order = '';
 _sort = sort ? '&sort=' + sort : '';
 order += _sort;
 order += ($('#direction').val() && $('#direction').val() == 'desc') ? '&direction=asc' : '&direction=desc';
 order += $('#currentPage').val() ? '&page=' + $('#currentPage').val() : '';
 loadDocTable(order);
 }
 function paginatorTable(page) {
 var paginator = '';
 paginator += page ? '&page=' + page : '';
 loadDocTable(paginator + _sort);
 }
 function addDocument() {
 $.ajax({
 url: $('#urlroot').val() + 'documentos/addajax/',
 data: 'empresa_id=' + $('#empresa-id').val() + "&tipodocumento_id=" + $('#tipodocumento-id').val(),
 dataType: 'json',
 success: function (data) {
 var retorno = data.retorno;
 loadCnaesTable();
 },
 error: function (a) {
 alert('teste deu erro ao inserir novo cnae a tabela .. pede p cau verificar isso');
 }
 });
 }
 */