$(document).ready(function () {

    $('#cnaesection-id').change(function () {
        loadCnaeDivisionList($(this).val());
        $('#divisao-box').show();
        $('#grupo-box').hide();
        $('#classe-box').hide();
        $('.quest-box').hide();
    });
    $('#cnaedivision-id').change(function () {
        loadCnaeGroupList($(this).val());
        $('#grupo-box').show();
        $('#classe-box').hide();
    });
    $('#cnaegroup-id').change(function () {
        loadCnaeClasseList($(this).val());
        $('#classe-box').show();
    });
    $('#cnaeclasse-id').change(function () {
        $('.quest-box').show();
    });

});
function loadCnaesTable() {
    $.ajax({
        url: $('#urlroot').val() + 'empresacnaes/loadajax/',
        data: 'empresa_id=' + $('#empresa-id').val(),
        type: 'GET',
        success: function (data) {
            $('#table-cnaes').html(data);
        },
        error: function (a) {
            console.log(a);
        }
    });
}

function loadCnaeSectionList() {
    $.ajax({
        url: $('#urlroot').val() + 'cnaesections/listajax/',
        dataType: 'json',
        success: function (data) {
            var cnaes = data.retorno;
            var html = "<option value='null'> Selecione a SEÇÃO da atividade</option>";
            $.each(cnaes, function (i, info) {
                html += '<option value="' + cnaes[i].id + '" >' + cnaes[i].secao + '</option>';
            });
            $('#cnaesection-id').html(html);
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function loadCnaeDivisionList(section) {
    $.ajax({
        url: $('#urlroot').val() + 'cnaedivisions/listajax/' + section,
        dataType: 'json',
        success: function (data) {
            var cnaes = data.retorno;
            var html = "<option value='null'> Selecione a DIVISÃO da atividade</option>";
            $.each(cnaes, function (i, info) {
                html += '<option value="' + cnaes[i].id + '" >' + cnaes[i].divisao + '</option>';
            });
            $('#cnaedivision-id').html(html);
            if ($('#cnaedivision-id').hasClass('select2')) {
                $("#cnaedivision-id").select2("destroy");
                $("#cnaedivision-id").select2({minimumResultsForSearch: 5, placeholder: 'selecione uma opção'});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function loadCnaeGroupList(division) {
    $.ajax({
        url: $('#urlroot').val() + 'cnaegroups/listajax/' + division,
        dataType: 'json',
        success: function (data) {
            var cnaes = data.retorno;
            var html = "<option value='null'> Selecione o GRUPO da atividade</option>";
            $.each(cnaes, function (i, info) {
                html += '<option value="' + cnaes[i].id + '" >' + cnaes[i].grupo + '</option>';
            });
            $('#cnaegroup-id').html(html);
            if ($('#cnaegroup-id').hasClass('select2')) {
                $("#cnaegroup-id").select2("destroy");
                $("#cnaegroup-id").select2({minimumResultsForSearch: 5, placeholder: 'selecione uma opção'});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function loadCnaeClasseList(group) {
    $.ajax({
        url: $('#urlroot').val() + 'cnaeclasses/listajax/' + group,
        dataType: 'json',
        success: function (data) {
            var cnaes = data.retorno;
            var html = "<option value='null'> Selecione a CLASSE da atividade</option>";
            $.each(cnaes, function (i, info) {
                html += '<option value="' + cnaes[i].id + '" >' + cnaes[i].classe + '</option>';
            });
            $('#cnaeclasse-id').html(html);
            if ($('#cnaeclasse-id').hasClass('select2')) {
                $("#cnaeclasse-id").select2("destroy");
                $("#cnaeclasse-id").select2({minimumResultsForSearch: 5, placeholder: 'selecione uma opção'});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function loadCnaesList() {
    $.ajax({
        url: $('#urlroot').val() + 'cnaes/ajax/',
        dataType: 'json',
        success: function (data) {
            var cnaes = data.retorno;
            var html = "<option value='null'> Selecione um novo CNAE e clique em inserir para vincular à essa empresa</option>";
            $.each(cnaes, function (i, info) {
                html += '<option value="' + cnaes[i].id + '" >' + cnaes[i].classe + '</option>';
            });
            $('#cnae-id').html(html);
            if ($('#cnae-id').hasClass('select2')) {
                $("#cnae-id").select2("destroy");
                $("#cnae-id").select2({minimumResultsForSearch: 5, placeholder: 'selecione uma opção'});
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function addCnaeRelation() {
    if ($('#cnaeclasse-id').val() == '' || $('#cnaeclasse-id').val() == null || $('#cnaeclasse-id').val() == 'null') {
        alert('É necessário informar a Classe da Atividade');
    } else {
        $.ajax({
            url: $('#urlroot').val() + 'empresacnaes/addajax/',
            data: 'empresa_id=' + $('#empresa-id').val() + "&cnaeclasse_id=" + $('#cnaeclasse-id').val() + "&exercida_local=" + $('#exercida_local').is(':checked') + "&principal=" + $('#principal').is(':checked'),
            dataType: 'json',
            success: function (data) {
                loadCnaesTable();
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
}

function deleteCnaeRelation(id) {
    if (confirm("Tem certeza que deseja desvincular esse Cnae?")) {
        $.ajax({
            url: $('#urlroot').val() + 'empresacnaes/deleteajax/',
            data: 'id=' + id,
            dataType: 'json',
            success: function (data) {
                loadCnaesTable();
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
}
/**--------------------------FIM (Parte de CNAES)---------------------------------------------------------***/


/**--------------------------INICIO (Parte de Atividades Auxiliares)---------------------------------------------------------***/
function loadAtividadesList() {
    $.ajax({
        url: $('#urlroot').val() + 'atividadesauxiliares/ajax/',
        dataType: 'json',
        success: function (data) {
            var retorno = data.retorno
            var html = "<option value='null'> Selecione e click para inserir uma atividade</option>";
            $.each(retorno, function (i, info) {
                html += '<option value="' + i + '" >' + info + '</option>';
            });
            $('#atividadesauxiliar-id').html(html);
            if ($('#atividadesauxiliar-id').hasClass('select2')) {
                $("#atividadesauxiliar-id").select2("destroy");
                $("#atividadesauxiliar-id").select2({minimumResultsForSearch: 5, placeholder: 'selecione uma opção'});
            }

        },
        error: function (a) {
            console.log(a);
        }
    });
}
function addAtividadeAuxiliarRelation() {
    if ($('#atividadesauxiliar-id').val() == '' || $('#atividadesauxiliar-id').val() == null || $('#atividadesauxiliar-id').val() == 'null') {
        alert('É necessário informar a Atividade Auxiliar para poder inserir');
    } else {
        $.ajax({
            url: $('#urlroot').val() + 'empresaatividades/addajax/',
            data: 'empresa_id=' + $('#empresa-id').val() + "&atividadesauxiliar_id=" + $('#atividadesauxiliar-id').val(),
            dataType: 'json',
            success: function (data) {
                loadAtividadesTable();
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
}

function loadAtividadesTable() {
    $.ajax({
        url: $('#urlroot').val() + 'empresaatividades/loadajax/',
        data: 'empresa_id=' + $('#empresa-id').val(),
        type: 'GET',
        success: function (data) {
            $('#table-atividades').html(data);
        },
        error: function (a) {
            console.log(a);
        }
    });
}

function deleteAtividadeRelation(id) {
    if (confirm("Tem certeza que deseja excluir essa atividade?")) {
        $.ajax({
            url: $('#urlroot').val() + 'empresaatividades/deleteajax/',
            data: 'id=' + id,
            dataType: 'json',
            success: function (data) {
                loadCnaesTable();
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
}
