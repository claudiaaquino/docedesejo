$.date = function (dateObject, showHours) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    if (showHours) {
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
//        var curr_sec = d.getSeconds();
        date += ' ' + curr_hour + ":" + curr_min ;
    }

    return date;
};